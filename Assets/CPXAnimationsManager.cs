﻿using UnityEngine;
using System.Collections;
using System;

public class CPXAnimationsManager : MonoBehaviour {

    //public Transform _sphere;
    public int _cantSteps;
    private int _currentStep;
    public Transform[] _StepsPositions;
    public GameObject _sphere;
    public bool _worldAnimation;
    public Transform[] _objectsToMove;
    public Vector3[] _objectDestiny;
    private Vector3[] _objectsOrigins;

    public enum AnimationType { Oscilation, Animator, OscilateColor, Calibrate};
    public AnimationType _animationType;
    public Animator _animator;
    public float _animSpeed = 0.1f;


    public static CPXAnimationsManager Inst;
    public delegate void EndAnimationDelegateCPX();
    public static event EndAnimationDelegateCPX EndAnimationOn;
    public delegate void EndAllAnimationDelegateCPX();
    public static event EndAllAnimationDelegateCPX EndAllAnimationOn;

    private CActivateParticles _particleManager;
    private Vector3 _sphereScale;
    private Material _sphereMaterial;

    public bool _animationPlaying;
    private Coroutine _animCoroutine = null;

    [Header("Calibrate")]
    public SActionsObjectsPerStepCal[] _actionsOnEveryStep;
    public Animator[] _animatorsToTurnSpeedZero;
    public Transform[] _vertex;

    [System.Serializable]
    public class SActionsObjectsPerStepCal
    {
        public ECalibrateActions[] _actionsToApplyOnObjects;
        public GameObject[] _objects;
    }



    void Awake()
    {
        Inst = this;
    }

    void Start()
    {
        _currentStep = -1;
        switch (_animationType)
        {
            case AnimationType.Oscilation:
                InitializeCodeAnimations();
                break;
            case AnimationType.Animator:
                InitializeAnimatorAnimation();
                break;
            case AnimationType.OscilateColor:
                InitializeCodeAnimations();
                break;
            case AnimationType.Calibrate:
                InitializeCalibrateAnims();
                break;
        }
        _sphereMaterial = _sphere.GetComponentInChildren<Renderer>().material;

    }


    private void InitializeCalibrateAnims()
    {
        for (int i = 0; i < _animatorsToTurnSpeedZero.Length; i++)
            _animatorsToTurnSpeedZero[i].speed = 0;
        CSkillsMeasurement.Inst.SteadinessSetInstance(_vertex[0]);
        AnimCalibrate(0);
    }

    private void InitializeCodeAnimations()
    {
        _sphere.transform.position = _StepsPositions[0].position;
        _sphere.transform.rotation = _StepsPositions[0].rotation;
        _sphereScale = _sphere.transform.localScale;
        _sphere.transform.localScale = Vector3.zero;
        _particleManager = _sphere.GetComponentInChildren<CActivateParticles>();        
        _objectsOrigins = new Vector3[_objectsToMove.Length];
        for (int _i = 0; _i < _objectsToMove.Length; _i++)
        {
            _objectsOrigins[_i] = _objectsToMove[_i].localPosition;
        }        
    }

    public void InitializeAnimatorAnimation()
    {
        _animator.speed = 0.0f;
    }


    public void PlayAnimation()
    {       
        _sphereMaterial.color = Color.green;
        CAudioManager.Inst.PlaySFX("Found");
        StartCoroutine("SoundLoop");        
        switch (_animationType)
        {
            case AnimationType.Oscilation:                
                _animCoroutine = StartCoroutine(OscilateSizeAndDisapeare(() => { }, _sphere.transform, 0.8f * _sphereScale, 1.2f * _sphereScale, 1.0f, 3));
                break;
            case AnimationType.Animator:
                _animator.speed = _animSpeed;
                _animCoroutine = StartCoroutine(OscilateColor(_sphereMaterial, new Color32(0x05, 0x92, 0x1D, 0xFF), new Color32(0x1D, 0xFF, 0x00, 0xFF), 1.0f));
                break;
            case AnimationType.OscilateColor:
                _animCoroutine = StartCoroutine(OscilateColorWithEnding(() => { }, _sphere.transform,_sphereMaterial, new Color32(0x05, 0x92, 0x1D, 0xFF), new Color32(0x1D, 0xFF, 0x00, 0xFF), 1.0f,3));
                break;
            case AnimationType.Calibrate:
                AnimCalibrate(_currentStep);
                break;
        }
        _animationPlaying = true;

    }

    public void StopAnimation()
    {
        //Debug.Log("Stopping Animation Carajo...");        
        //CAudioManager.Inst.PlaySFX("Lost");
        StopCoroutine("SoundLoop");
        switch (_animationType)
        {
            case AnimationType.Oscilation:
                StopCoroutine(_animCoroutine);
                break;
            case AnimationType.Animator:
                _animator.speed = 0.0f;
                StopAllCoroutines();
                break;
            case AnimationType.OscilateColor:
                StopAllCoroutines();
                break;
        }
        _animationPlaying = false;
        _sphereMaterial.color = Color.red;
    }





    IEnumerator SoundLoop()
    {
        WaitForSeconds _soundDelay = new WaitForSeconds(0.3f);
        yield return _soundDelay;
        _soundDelay = new WaitForSeconds(1);
        while (true)
        {
            CAudioManager.Inst.PlaySFX("Loop");
            yield return _soundDelay;
        }
    }

    public void EndAnimationCall()
    {
        Debug.Log("End Animation..");
        CSkillsMeasurement.Inst.SteadinessCloseInstance();
        CAudioManager.Inst.PlaySFX("Lost");
        StopCoroutine("SoundLoop");
        if (EndAnimationOn != null)
        {
            EndAnimationOn();
        }
        if (_worldAnimation)
        {
            GetDownBlock(() => { NextStep(); }, _currentStep);
        }
        else
        {
            NextStep();
        }     
       
    }

    public void NextStep()
    {
        Debug.Log("Next Step...");
        _currentStep += 1;

        if (_currentStep == _cantSteps)
        {
            if (EndAllAnimationOn != null)
            {
                Debug.Log("End All Animation..");
                EndAllAnimationOn();                
            }
        }
        else
        {
            if(_animationType == AnimationType.Oscilation || _animationType == AnimationType.OscilateColor)
            {
                if (_worldAnimation)
                {
                    GetUpBlock(() => {
                        AppearShpere(() => { }, _currentStep);
                    }, _currentStep);
                }
                else
                {
                    AppearShpere(() => { }, _currentStep);
                }
            }
            else
            {
                CSkillsMeasurement.Inst.SteadinessSetInstance(_sphere.transform);
            }
            if(_animationType == AnimationType.Calibrate)
            {
                Debug.Log("Prendiendo step " + _currentStep + " de calibrate...");
                CSkillsMeasurement.Inst.SteadinessSetInstance(_vertex[_currentStep]);
            }
                    
        }

    }


    // Animaciones
    private IEnumerator OscilateSizeAndDisapeare(Action _callback, Transform _objeto, Vector3 _minScale, Vector3 _maxScale, float _period, float _numberOfPeriods)
    {

        int _n = 0;
        while (_n < _numberOfPeriods)
        {
            yield return StartCoroutine(CCodeAnimations.Change3DSize(() => { }, _objeto, _objeto.localScale, _maxScale, _period / 2.0f));
            yield return StartCoroutine(CCodeAnimations.Change3DSize(() => { }, _objeto, _objeto.localScale, _minScale, _period / 2.0f));
            _n += 1;
        }
        StartCoroutine(CCodeAnimations.Change3DSize(() => {
            _particleManager.PlayParticle(0);
            EndAnimationCall();
        }, _objeto, _objeto.localScale, Vector3.zero, _period / 10.0f));
        _callback();
        yield break;
    }

    private IEnumerator OscilateColor(Material _objeto, Color _minScale, Color _maxScale, float _period)
    {
        while (true)
        {
            yield return StartCoroutine(CCodeAnimations.ChangeColor(() => { }, _objeto, _objeto.color, _maxScale, _period / 2.0f));
            yield return StartCoroutine(CCodeAnimations.ChangeColor(() => { }, _objeto, _objeto.color, _minScale, _period / 2.0f));
            yield return null;
        }
    }

    private IEnumerator OscilateColorWithEnding(Action _callback, Transform _objetoT, Material _objeto, Color _minScale, Color _maxScale, float _period, float _numberOfPeriods)
    {
        int _n = 0;
        while (_n < _numberOfPeriods)
        {
            yield return StartCoroutine(CCodeAnimations.ChangeColor(() => { }, _objeto, _objeto.color, _maxScale, _period / 2.0f));
            yield return StartCoroutine(CCodeAnimations.ChangeColor(() => { }, _objeto, _objeto.color, _minScale, _period / 2.0f));
            _n += 1;
            yield return null;
        }
        StartCoroutine(CCodeAnimations.Change3DSize(() => {
            _particleManager.PlayParticle(0);
            EndAnimationCall();
        }, _objetoT, _objetoT.localScale, Vector3.zero, _period / 10.0f));
        _callback();
    }


    private void AnimCalibrate(int _step)
    {
        Debug.Log("Animating step " + _step + " de calibrate...");
        SActionsObjectsPerStepCal _ao = _actionsOnEveryStep[_step - 1];
        for (int i = 0; i < _ao._objects.Length; i++)
        {
            switch (_ao._actionsToApplyOnObjects[i])
            {
                case ECalibrateActions.ACTIVATE_VERTEX:
                    _ao._objects[i].GetComponent<Animator>().speed = 1;
                    break;
                case ECalibrateActions.TURN_ON_VERTEX:
                    _ao._objects[i].GetComponent<Animator>().SetTrigger("change_color");
                    break;
                case ECalibrateActions.SCALE_CILINDER:
                    _ao._objects[i].GetComponent<Animator>().speed = 1;
                    break;
                case ECalibrateActions.FACE_FADE_IN:
                    _ao._objects[i].GetComponent<Animator>().speed = 1;
                    break;
                case ECalibrateActions.ACTIVATE_GAME_OBJECT:
                    _ao._objects[i].SetActive(true);
                    break;
            }
        }
        EndAnimationCall();
    }



    private void AppearShpere(Action _callback, int _pos)
    {
        _sphere.transform.localScale = Vector3.zero;
        _sphere.transform.position = _StepsPositions[_pos].position;
        _sphere.transform.rotation = _StepsPositions[_pos].rotation;
        _sphere.transform.localScale = _sphereScale;
        CSkillsMeasurement.Inst.SteadinessSetInstance(_sphere.transform);
        StartCoroutine(CCodeAnimations.Change3DSize(() => {
            StartCoroutine(CCodeAnimations.Change3DSize(() => {
                _callback();
            }, _sphere.transform, _sphere.transform.localScale, _sphereScale, 0.25f));
        }, _sphere.transform, _sphere.transform.localScale, _sphereScale*1.2f, 0.75f));
    }

    private void GetDownBlock(Action _callback, int _blockD)
    {
        StartCoroutine(CCodeAnimations.Change3DPosition(() => { _callback(); }, _objectsToMove[_blockD], _objectsToMove[_blockD].localPosition, _objectsOrigins[_blockD], 1.0f));
    }
    private void GetUpBlock(Action _callback, int _block)
    {
        StartCoroutine(CCodeAnimations.Change3DPosition(() => { _callback(); }, _objectsToMove[_block], _objectsToMove[_block].localPosition, _objectDestiny[_block], 1.0f));
    }



}
