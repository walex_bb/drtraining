﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CSkillsMeasurement : MonoBehaviour {

    // Singleton
    public static CSkillsMeasurement Inst;

    // Generics
    private bool _isPlaying;


    // Agility
    [Header("Agility")]
    public bool _playAgility = true;
    public Text _timerText;
    public float _tMin, _tMax;
    public float _timer = 0;
    private string _hours, _minutes, _seconds;   
    private bool _activatedAgility = false;
    [Range(0.0f, 100.0f)]
    public float Agility = 100f;

    // Angular Navigation
    [Header("Angular Navigation")]
    public bool _playAngularNavigation = true;
    public Transform _instrument;
    public float _angleNavigation = 0;
    public float _angMin, _angMax;
    public float _angTol = 10;    
    private Vector3 _previousInstrumentAngle;
    private bool _activatedAngularNavigation = false;
    private float _difAngle;
    [Range(0.0f, 100.0f)]
    public float AngularNavigation = 100f;

    // Linear Navigation
    [Header("Linear Navigation")]
    public float _distNavigation = 0;
    public float _distTol = 10;
    public float _distMin, _distMax;   
    private Vector3 _previousInstrumentPos;
    private bool _activatedLinearNavigation = false;
    private float _difPos;
    [Range(0.0f, 100.0f)]
    public float LinearNavigation = 100f;

    // Steadiness
    [Header("Steadiness")]
    public Transform _cameraPosSteadiness;
    public float _distToTarget; // Distancia instantánea
    public float _distToTargetSupr; // Distancia suprema para un caso
    public float _steadinessPreMap;
    public float _distTargetMin, _distTargetMax; // Claibración para mapeo de 0 a 100
    private bool _activatedSteadiness = false;
    private bool _playSteadiness = false;
    public float _distLocked = 10f; // dist ok
    public float _distToTargetTol = 50f; // dist not ok
    private Vector3 _lockedPosition;
    public float[] _steadinessSupremos;
    private int _maxNumberOfSteps;
    private int _currentStep;
    public Transform _objetivo;
    public Color _steadinessIndicator;
    public Material _steadinessIndicatorMaterial;
    private bool _lookingForTarget = true;
    private bool _targetLocked = false;
    [Range(0.0f, 100.0f)]
    public float Steadiness = 100f;
    public delegate void DistanceCheckerDelegates();
    public static event DistanceCheckerDelegates DistanceTargetLocked;
    public static event DistanceCheckerDelegates DistanceTargetLost;
    
    // Tracking
    [Header("Tracking")]
    public bool _trackingMode;
    public Transform _objectToTrack;
    public int _trackingLostCount;
    [Range(0.0f, 100.0f)]
    public float Tracking = 100f;
    public int _trackingLosMax;

    // Centering
    [Header("Centering")]
    public float _angToTarget; // Distancia instantánea
    public float _angToTargetSupr; // Distancia suprema para un caso
    public float _centeringPreMap;
    public float _angTargetMin, _angTargetMax; // Claibración para mapeo de 0 a 100
    private bool _activatedCentering = false;
    public bool _playCentering = false;
    public float _angLocked = 0f; // dist ok
    public float _angToTargetTol = 50f; // dist not ok
    private Vector3 _lockedAngle;
    public float[] _centeringSupremos;
    public Color _centeringIndicator;
    public Material _centeringIndicatorMaterial;
    private bool _lookingForTargetFront = true;
    private bool _targetFrontLocked = false;
    [Range(0.0f, 100.0f)]
    public float Centering = 100f;
    public delegate void AngleCheckerDelegates();
    public static event AngleCheckerDelegates AngleTargetLocked;
    public static event AngleCheckerDelegates AngleTargetLost;

    



    // Horizon Control
    [Header("Horizon Control")]
    public float _angHC;
    public float _angHCsup;
    private bool _activatedHC = false;
    public float _angHCmin, _angHCmax;
    public float[] _HorizonSupremos;
    [Range(0.0f, 100.0f)]
    public float HorizonControl = 100f;
    public float _horizonPreMap;
    public bool _playHorizon = false;
    public Color _HorizonIndicator;
    public Material _HorizonIndicatorMaterial;
    public Transform _horizonIndicatorToRotate;

    public Button _initialPopupOkButton;
    public bool _isMarkerPreDetected = false;

    // Awake
    void Awake()
    {
        Inst = this;
    }

    void OnDestroy()
    {
        ARDelegates.PreNotificationAROn -= OnPreNotificationAROn;
        ARDelegates.PreNotificationAROff -= OnPreNotificationAROff;
        DelegatesSubscription(false);
    }


    // Use this for initialization
    void Start () {
        _initialPopupOkButton.onClick.AddListener(TouchOk);
        ARDelegates.PreNotificationAROn += OnPreNotificationAROn;
        ARDelegates.PreNotificationAROff += OnPreNotificationAROff;
    }

    void OnPreNotificationAROn()
    {
        _isMarkerPreDetected = true;
    }

    void OnPreNotificationAROff()
    {
        _isMarkerPreDetected = false;
    }

    public void TouchOk()
    {
        _initialPopupOkButton.onClick.RemoveListener(TouchOk);

        _isPlaying = true;
        DelegatesSubscription(true);
        _currentStep = 0;
        _maxNumberOfSteps = CXIndicadoresManager.Inst._totalSteps - 1;
        _steadinessSupremos = new float[_maxNumberOfSteps];
        _centeringSupremos = new float[_maxNumberOfSteps];
        _HorizonSupremos = new float[_maxNumberOfSteps];

        if (_isMarkerPreDetected) {
            ActivateAngularNavigation();
            ActivateLinearNavigation();
            ActivateSteadiness();
            ActivateAgility();
        }

    }

    void Update()
    {
        if (_isPlaying)
        {
            // Timer
            if (_activatedAgility && _playAgility)  ProcesarTimer();
            // Angular Navigation
            if (_activatedAngularNavigation && _playAngularNavigation) SumarAngulo();
            // Linear 
            if (_activatedLinearNavigation) SumarDistancia();
            // Steadiness
            if (_activatedSteadiness && _playSteadiness) ProcesarSteadiness();
            // Centering
			if (_activatedCentering && _playCentering) ProcesarCentering();
            // HC
            if (_activatedHC && _playHorizon) ProcesarHC();
            if (!_activatedHC && _playHorizon)  ChangeHCInidcator();
        }
       

    }


    // Comunes a todos ---------------------------------------------------------------------------------
    public void ChangePlayingState(bool pState)
    {
        _isPlaying = pState;
    }

    void DelegatesSubscription(bool _subscribing)
    {
        if (_subscribing)
        {
            ARDelegates.AROn += ActivateAngularNavigation;
            ARDelegates.AROff += PauseAngularNavigation;
            ARDelegates.AROn += ActivateLinearNavigation;
            ARDelegates.AROff += PauseLinearNavigation;
            ARDelegates.AROn += ActivateSteadiness;
            ARDelegates.AROff += PauseSteadiness;
            ARDelegates.AROn += ActivateAgility;
            ARDelegates.AROff += PauseAgility;
        }
        else
        {
            ARDelegates.AROn -= ActivateAngularNavigation;
            ARDelegates.AROff -= PauseAngularNavigation;
            ARDelegates.AROn -= ActivateLinearNavigation;
            ARDelegates.AROff -= PauseLinearNavigation;
            ARDelegates.AROn -= ActivateSteadiness;
            ARDelegates.AROff -= PauseSteadiness;
            ARDelegates.AROn -= ActivateAgility;
            ARDelegates.AROff -= PauseAgility;
        }
    }







    // Agility --------------------------------------------------------------------------------
    public void TimeConversion()
    {
        float _milliseconds = _timer * 1000;
        _seconds = ((int)(_milliseconds / 1000) % 60).ToString("00");
        _minutes = ((int)((_milliseconds / (1000 * 60)) % 60)).ToString("00");
        _hours = ((int)((_milliseconds / (1000 * 60 * 60)) % 24)).ToString("00");
    }
    public string Timer
    {
        get { return _hours + ":" + _minutes + ":" + _seconds; }
    }
    public void ActivateAgility()
    {
        _activatedAgility = true;
    }
    public void PauseAgility()
    {
        _activatedAgility = false;
    }
    public void RestartAgility()
    {
        _timer = 0;
    }
    public int AgilityNormalized(out float rawValue)
    {
        rawValue = _timer;
        return MapFunction(_timer, _tMin, _tMax);
    }
    private void ProcesarTimer()
    {
        float rawValue;
        _timer += Time.deltaTime;
        TimeConversion();
        _timerText.text = _hours + ":" + _minutes + ":" + _seconds;
        Agility = AgilityNormalized(out rawValue);
    }

   
    // Angular Navigation -------------------------------------------------------------------
    private void SumarAngulo()
    {
        _difAngle = Mathf.Abs(Vector3.Angle(_instrument.right, _previousInstrumentAngle));
        if (_difAngle > _angTol)
        {
            _angleNavigation += _difAngle;
            _previousInstrumentAngle = _instrument.right;
            AngularNavigation = AngularNavigationNormalized();
        }
       
        
    }
    public void ActivateAngularNavigation()
    {
        _previousInstrumentAngle = _instrument.right;
        _activatedAngularNavigation = true;       
    }
    public void PauseAngularNavigation()
    {
        _activatedAngularNavigation = false;
    }
    public int AngularNavigationNormalized()
    {
        return MapFunction(_angleNavigation, _angMin, _angMax);
    }




    // Linear Navigation -------------------------------------------------------------------------------------
    private void SumarDistancia()
    {
        _difPos = Vector3.Magnitude(_instrument.position - _previousInstrumentPos);
        if (_difPos > _distTol)
        {
            _distNavigation += _difPos;
            _previousInstrumentPos = _instrument.position;
            float rawValue;
            LinearNavigation = LinearNavigationNormalized(out rawValue);
        }
    }
    public void ActivateLinearNavigation()
    {
        _previousInstrumentPos = _instrument.position;
        _activatedLinearNavigation = true;        
    }
    public void PauseLinearNavigation()
    {
        _activatedLinearNavigation = false;
    }
    public int LinearNavigationNormalized(out float rawValue)
    {
        rawValue = _distNavigation;
        return MapFunction(_distNavigation, _distMin, _distMax);
    }





    // Steadiness -------------------------------------------------------------------------------
    const int CONT_GREEN = 0;
    const int CONT_YELLOW = 1;
    const int CONT_RED = 2;
    class SteadinessData
    {
        decimal[] conts = new decimal[] { 0, 0, 0 };
        // 0 green
        // 1 yellow
        // 2 red

        public void Reset()
        {
            conts[CONT_GREEN] = conts[CONT_YELLOW] = conts[CONT_RED] = 0;
        }

        public void Inc(int id)
        {
            conts[id] += 1;
        }

        public int getPoints()
        {
            int value = 0;
            decimal cont_red = conts[CONT_RED];
            decimal cont_green = conts[CONT_GREEN];
            decimal cont_yellow = conts[CONT_YELLOW];

            if (cont_red > cont_green)
            {
                if (cont_yellow >= cont_red)
                {
                    value = 1;
                }
                else
                {
                    value = 2;
                }
            }
            else if (cont_yellow > cont_green)
            {
                value = 1;
            }

            return value;
        }
    }

    SteadinessData sd = new SteadinessData();  
        
    public void SteadinessSetInstance(Transform _o)
    {
           
        if (!_trackingMode)
        {
            _objetivo = _o;
        }          
        else
        {
            _objetivo = _objectToTrack;
        }

        sd = new SteadinessData();
        _playSteadiness = true;
    }

    public void SteadinessCloseInstance()
    {
        _playSteadiness = false;
        CountSteadiness();
    }

    int GetCollisionLevel(float distance, float radius)
    {
        float step = radius / 3.0f;
        if (distance <= step)
            return CONT_GREEN;
        if (distance <= (step * 2))
            return CONT_YELLOW;
        return CONT_RED;
    }   

    private void ProcesarSteadiness()
    {
        if (_lookingForTarget)
        {
            _distToTarget = Vector3.Magnitude(_cameraPosSteadiness.position - _objetivo.position);
            _steadinessIndicator = Color.Lerp(Color.green, Color.red, _distToTarget / _distToTargetTol);            
            _steadinessIndicatorMaterial.color = _steadinessIndicator;

            if (_distToTarget < _distLocked)
            {   
                _distToTargetSupr = 0;                
                _lockedPosition = _cameraPosSteadiness.position;
                _lookingForTarget = false;
                _targetLocked = true;
                ActivateCentering();
                ActivateHC();
                if (DistanceTargetLocked != null)
                    DistanceTargetLocked();
            }

        }
        if (_targetLocked)
        {
            if (_trackingMode)
            {
                _lockedPosition = _objetivo.position;
            }
            _distToTarget = Vector3.Magnitude(_cameraPosSteadiness.position - _lockedPosition);
            _steadinessIndicator = Color.Lerp(Color.green, Color.red, _distToTarget / _distToTargetTol);
            _steadinessIndicatorMaterial.color = _steadinessIndicator;
            if (_distToTarget > _distToTargetTol)
            {

               // Debug.Log("TargetLockedLost...");
                _targetLocked = false;
                _lookingForTarget = true;
                PauseCentering();
                PauseHC();
                if (!CPXManager.Inst._centeringActive)
                {
                    CAudioManager.Inst.PlaySFX("Lost");
                }
                if (DistanceTargetLost != null)
                    DistanceTargetLost();
                if (_trackingMode)
                {
                    _trackingLostCount += 1;
                    Tracking = TrackingNormalized();
                }

                sd.Reset();
            }
            else
            {
                // inside sphere
                int nc = GetCollisionLevel(_distToTarget, _distToTargetTol);
                Debug.Log(string.Format("Colission level {0}", nc));
                sd.Inc(nc);
            }

            if (_distToTarget > _distToTargetSupr)
            {
                _distToTargetSupr = _distToTarget;
            }
            float rawValue;           
            Steadiness = SteadinessNormalized(out rawValue);
        }     

    }

    private void CountSteadiness()
    {

        //_steadinessSupremos[_currentStep] = _distToTargetSupr;
        
        _steadinessSupremos[_currentStep] = sd.getPoints(); 

        CountCentering (_currentStep);
        CountHorizon(_currentStep);
        _currentStep += 1;
    }


    public void ActivateSteadiness()
    {
        _activatedSteadiness = true;
    }
    public void PauseSteadiness()
    {
        _activatedSteadiness = false;
		if (_targetLocked) {
			_targetLocked = false;
			_lookingForTarget = true;
			PauseCentering();
			PauseHC();
			if (DistanceTargetLost != null)
				DistanceTargetLost();
			if (_trackingMode)
			{
				_trackingLostCount += 1;
				Tracking = TrackingNormalized();
			}
		}
    }
    public int SteadinessNormalized(out float _steadinessRaw)
    {

        _steadinessPreMap = 0;
        for (int _i=0; _i < _maxNumberOfSteps; _i++)
        {
            _steadinessPreMap += _steadinessSupremos[_i];
        }
        //_steadinessPreMap = _steadinessPreMap / _maxNumberOfSteps;

        _steadinessRaw = _steadinessPreMap;

        return MapFunction(_steadinessPreMap, _distTargetMin, _distTargetMax);
    }


    // Tracking --------------------------------------------------------------------------------
    public int TrackingNormalized()
    {
        return MapFunction((float)_trackingLostCount, 0.0f, (float) _trackingLosMax);
    }


    // Centering ----------------------------------------------------------------------------------------
    private void ProcesarCentering()
    {
		if (_lookingForTargetFront)
		{
			_angToTarget = Vector3.Angle(_instrument.right,_objetivo.right);
			_centeringIndicator = Color.Lerp(Color.green, Color.red, _angToTarget / _angToTargetTol);
			_centeringIndicatorMaterial.color = _centeringIndicator;
			if (_angToTarget < _angLocked)
			{
				_angToTargetSupr = 0;
				_lockedAngle = _instrument.right;
				_lookingForTargetFront = false;
				_targetFrontLocked = true;
				if (AngleTargetLocked != null)
					AngleTargetLocked();
			}
		}
		if (_targetFrontLocked)
		{
			_angToTarget = Vector3.Angle(_instrument.right, _lockedAngle);
			_centeringIndicator = Color.Lerp(Color.green, Color.red, _angToTarget / _angToTargetTol);
			_centeringIndicatorMaterial.color = _centeringIndicator;
			if (_angToTarget > _angToTargetTol)
			{
				Debug.Log("TargetAngleLockedLost...");
				_targetFrontLocked = false;
				_lookingForTargetFront = true;
                CAudioManager.Inst.PlaySFX("Lost");
                if (AngleTargetLost != null)
					AngleTargetLost();
			}
			if (_angToTarget > _angToTargetSupr)
			{
				_angToTargetSupr = _angToTarget;
			}
			//Centering = CenteringNormalized();
		}     
    }

	private void CountCentering(int _s)
	{
		_centeringSupremos[_s] = _angToTargetSupr;
	}

    public void ActivateCentering()
    {
		_activatedCentering = true;
    }
    public void PauseCentering()
    {
        _activatedCentering = false;
		_targetFrontLocked = false;
		_lookingForTargetFront = true;
		if (AngleTargetLost != null)
			AngleTargetLost();
    }
    public int CenteringNormalized()
    {
		_centeringPreMap = 0;
		for (int _i=0; _i < _maxNumberOfSteps; _i++)
		{
			_centeringPreMap += _centeringSupremos[_i];
		}
		_centeringPreMap = _centeringPreMap / _maxNumberOfSteps;
		return MapFunction(_centeringPreMap, _angTargetMin, _angTargetMax);
    }




    // Horizon Control --------------------------------------------------------------------------------------------

    private void ChangeHCInidcator()
    {
        _angHC = Mathf.Abs(Vector3.Angle(_instrument.up, new Vector3(_instrument.up.x, 0, _instrument.up.z)));
        _HorizonIndicator = Color.Lerp(Color.green, Color.red, _angHC / 45f);
        _HorizonIndicatorMaterial.color = _HorizonIndicator;
        if (_instrument.up.y > 0)
        {
            _horizonIndicatorToRotate.localRotation = Quaternion.Euler(_angHC * Vector3.forward);
        }
        else
        {
            _horizonIndicatorToRotate.localRotation = Quaternion.Euler(-_angHC * Vector3.forward);
        }
       
    }

    private void ProcesarHC()
    {
        ChangeHCInidcator();
        if (_angHC > _angHCsup)
        {
            _angHCsup = _angHC;
            HorizonControl = HCNormalized();
        }
    }

    private void CountHorizon(int _s)
    {
        _HorizonSupremos[_s] = _angHCsup;
        _activatedHC = false;
    }


    public void ActivateHC()
    {
        _angHCsup = 0;
        _activatedHC = true;
    }
    public void PauseHC()
    {
        _activatedHC = false;
    }
    public int HCNormalized()
    {
        _horizonPreMap = 0;
        for (int _i = 0; _i < _maxNumberOfSteps; _i++)
        {
            _horizonPreMap += _HorizonSupremos[_i];
        }
        _horizonPreMap = _horizonPreMap / _maxNumberOfSteps;
        return MapFunction(_horizonPreMap, _angHCmin, _angHCmax);
    }















    // Función de mapeo para llevar los valores al rango 0 100 -----------------------------------------------------
    public int MapFunction(float _x, float _xmin, float _xmax)
    {
        //// coseno
        //float _y = -1;
        //float _A = Mathf.PI / (2 * (_xmax - _xmin));
        //if (_x < _xmin) _y = 100;
        //else if (_x > _xmax) _y = 0;
        //else _y = 100 * Mathf.Cos(_A * (_x - _xmin));
        //return (int) _y;

        /*
        // lineal
        float _y = -1;
        if (_x < _xmin) _y = 100;
        else if (_x > _xmax) _y = 0;
        else _y = 100 - 100 * (_x - _xmin)/(_xmax-_xmin);
        if (_y < 10) _y = 10;
        return (int)_y;
        */

        
        if (_x < _xmin) return  100;
        if (_x > _xmax) return  0;
        return (int)(100 * (_x - _xmin) / (_xmax - _xmin));
    }




}
