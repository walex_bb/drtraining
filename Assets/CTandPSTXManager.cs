﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CTandPSTXManager : MonoBehaviour {


    private int _maxNumberOfSteps;
    private int _currentStep;


    // Use this for initialization
    void Start()
    {
        DelegatesSubscription(true);
        _currentStep = 0;
        _maxNumberOfSteps = CIndicadoresManager.Inst._totalSteps;
    }

    void OnDestroy()
    {
        DelegatesSubscription(false);
    }




    void DelegatesSubscription(bool _subscribing)
    {
        if (_subscribing)
        {
            ARDelegates.AROn += FirstDetectionDone;
            CSkillsMeasurement.DistanceTargetLocked += ImageCentered;
            CSkillsMeasurement.DistanceTargetLost += ImageDecentered;
            CPXAnimationsManager.EndAnimationOn += StepEnded;
            CPXAnimationsManager.EndAllAnimationOn += EndScenario;
        }
        else
        {
            ARDelegates.AROn -= FirstDetectionDone;
            CSkillsMeasurement.DistanceTargetLocked -= ImageCentered;
            CSkillsMeasurement.DistanceTargetLost -= ImageDecentered;
            CPXAnimationsManager.EndAnimationOn -= StepEnded;
            CPXAnimationsManager.EndAllAnimationOn -= EndScenario;
        }
    }


    private void FirstDetectionDone()
    {
        CIndicadoresManager.Inst.MarkNextStep();
        _currentStep += 1;
        ARDelegates.AROn -= FirstDetectionDone;
    }

    private void ImageCentered()
    {
        CPXAnimationsManager.Inst.PlayAnimation();
    }

    private void ImageDecentered()
    {
        CPXAnimationsManager.Inst.StopAnimation();
    }


    private void StepEnded()
    {

        _currentStep += 1;
        CIndicadoresManager.Inst.MarkNextStep();
    }


    private void EndScenario()
    {
        DelegatesSubscription(false);
    }
    


    
}
