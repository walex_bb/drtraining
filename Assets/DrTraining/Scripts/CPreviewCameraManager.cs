﻿/*
 CPreviewCameraManager: set the preview position and visibility on screen for the circular
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class CPreviewCameraManager : MonoBehaviour {
    bool _isTracking;
    [Tooltip("Si se marca, la preview no se oculta aunque este trackeando")]
    public bool _useFixedCamera;
    [Tooltip("Si se marca, la preview se oculta instantaneamente al comenzar el tracking aunque aun quede tiempo visible disponible")]
    public bool _instantantHideOnTrackingFound;
    [Tooltip("Sirve en Image Centering 1 para que la vista del mundo real nunca quede oculta y sea visible siempre a la derecha.")]
    public bool _show3DWorldCameraAllTheTime;
    float _visibleTime = GameConstants.BASE_VISIBLE_TIME;

    public Sprite _visibleEyeSpr, _closedEyeSpr, _cameraIconSpr;
    [Tooltip("Boton del ojo en pantalla con el cual se suman segundos de visibilidad")]
    public Image _indicatorImg;

    public Image _logoImg;

    [Range(0, 10)]
    public float _fadeSpeed;
    public float _moveSpeed = 9000.0f;

    [Range(0, 1)]
    public float _logoAlphaUmbral = 0.75f;

    [Header("0 = ON_SCREEN_QUARTER_CORNER | 1 = ON_SCREEN_FULL_VISIBLE")]
    public RectTransform[] _rectPositions;

    public RectTransform _parentPreviewContainerRectTransform;
    public RawImage _3DWorld, _realWorld;
    //private and internal vars

    // Use this for initialization
	void Start () {
        SetDelegates(true);
	}

    void OnDestroy() {
        SetDelegates(false);
    }

	// Update is called once per frame
	void Update () {
        SetState(Time.deltaTime);
        UpdateVisibleTime(Time.deltaTime);
        SetLogoImageVisibility(Time.deltaTime);
	}


    //MAIN LOOP METHODS
    void SetState(float _deltaTime) {
        if (_isTracking)
        {
             if (_useFixedCamera)
            {
                SetPreviewVisible(_deltaTime, false);
                SetButtonState(EButtonSpriteEye.EYE_OPEN);
                SetPreviewTargetMovePos(EPreviewPositions.ON_SCREEN_FULL_VISIBLE, _deltaTime);
            }
            else {
                if (_visibleTime > 0)
                {
                    SetPreviewVisible(_deltaTime, false);
                    SetButtonState(EButtonSpriteEye.EYE_OPEN);
                    SetPreviewTargetMovePos(EPreviewPositions.ON_SCREEN_FULL_VISIBLE, _deltaTime);
                }
                else {                   
                    SetPreviewInvisible(_deltaTime);
                    SetButtonState(EButtonSpriteEye.EYE_CLOSED);
                    SetPreviewTargetMovePos(EPreviewPositions.ON_SCREEN_QUARTER_CORNER, _deltaTime);
                }
            }
        }
        else {
            SetPreviewVisible(_deltaTime, true);
            SetButtonState(EButtonSpriteEye.REAL_CAM);
            SetPreviewTargetMovePos(EPreviewPositions.ON_SCREEN_FULL_VISIBLE, _deltaTime);
        }
    }

    void SetButtonState(EButtonSpriteEye _btnState) {
        switch (_btnState) { 
            case EButtonSpriteEye.EYE_OPEN:
                _indicatorImg.sprite = _visibleEyeSpr;
                break;
            case EButtonSpriteEye.EYE_CLOSED:
                _indicatorImg.sprite = _closedEyeSpr;
                break;
            case EButtonSpriteEye.REAL_CAM:
                _indicatorImg.sprite = _cameraIconSpr;
                break;
        }
    }

    void UpdateVisibleTime(float _deltaTime)
    {
        if (_useFixedCamera)
            return;

        if (_isTracking)
        {
            if (_visibleTime > 0)
                _visibleTime -= _deltaTime;
            else
                _visibleTime = 0;
        }
    }

    void SetLogoImageVisibility(float _deltaTime) {
        if (_logoImg == null)
            return;
        Color _color = _logoImg.color;
        _color.a += (_isTracking ? -1 : 1) * _deltaTime * _fadeSpeed;
        if (_color.a < 0)
            _color.a = 0;
        if (_color.a >= _logoAlphaUmbral)
            _color.a = _logoAlphaUmbral;
        _logoImg.color = _color;
    }

    //AUX FUNCTIONS

    void SetPreviewVisible(float _deltaTime, bool _realCameraNot3DWorld) {
        if (_realCameraNot3DWorld)
        {
            if (!_show3DWorldCameraAllTheTime)
                SetAlpha(ref _3DWorld, -_deltaTime);
            SetAlpha(ref _realWorld, _deltaTime);
        }
        else {
            SetAlpha(ref _3DWorld, _deltaTime);
            SetAlpha(ref _realWorld, -_deltaTime);
        }
    }

    void SetPreviewInvisible(float _deltaTime) {
        if (!_show3DWorldCameraAllTheTime)
            SetAlpha(ref _3DWorld, -_deltaTime);
        SetAlpha(ref _realWorld, -_deltaTime);
    }

    void SetAlpha(ref RawImage _rImg, float _delta) {
        Color _color = _rImg.color;
        _color.a +=  _delta * _fadeSpeed;
        if (_color.a < 0)
            _color.a = 0;
        if (_color.a > 1)
            _color.a = 1;
        _rImg.color = _color;
    }

    void SetPreviewTargetMovePos(EPreviewPositions _posType, float _delta) {
        _parentPreviewContainerRectTransform.position = Vector3.MoveTowards(_parentPreviewContainerRectTransform.position, _rectPositions[(int)_posType].position, _delta * _moveSpeed);
    }

    //DELEGATE METHODS

    void SetDelegates(bool _addNotRemove)
    {
        if (_addNotRemove)
        {
            ARDelegates.AROn += OnAROn;
            ARDelegates.AROff += OnAROff;
        }
        else
        {
            ARDelegates.AROn -= OnAROn;
            ARDelegates.AROff -= OnAROff;
        }
    }

    public void OnAROn() {
        _isTracking = true;
        if (!_instantantHideOnTrackingFound)
            ResetVisibleTime();
        else
            _visibleTime = 0;
    }

    public void OnAROff() {
        _isTracking = false;
    }

    public void ResetVisibleTime() {
        Debug.Log("Reseting Visible Time of Preview");
        if (_visibleTime <= 0)
            _visibleTime = GameConstants.BASE_VISIBLE_TIME;
    }
}
