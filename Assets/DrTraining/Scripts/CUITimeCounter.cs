﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CUITimeCounter : MonoBehaviour {

    public static CUITimeCounter Inst;
    private float _timer = 0;
    private string _hours, _minutes, _seconds;
    public Text _timerText;
    private bool _activated = false;
    private bool _isPlaying;

    public int _debugNorm;

    void Awake()
    {
        Inst = this;
    }

    public void Activate()
    {
        _activated = true;
    }

    public void Pause()
    {
        _activated = false;
    }

    public void Resume()
    {
        _activated = true;
    }

    public void Restart()
    {
        _timer = 0;
    }

    public float GetTotalTime()
    {
        return _timer;
    }


    void Update() {
        if (_activated && _isPlaying)
        {
            _timer += Time.deltaTime;
            TimeConversion();
            _timerText.text = _hours + ":" + _minutes + ":" + _seconds;
            //_debugNorm = EstimatorTimeNormalized();
        }        

    }

    public void TimeConversion() {
        float _milliseconds = _timer * 1000;
        _seconds = ((int)(_milliseconds / 1000) % 60).ToString("00");
        _minutes = ((int)((_milliseconds / (1000 * 60)) % 60)).ToString("00");
        _hours = ((int)((_milliseconds / (1000 * 60 * 60)) % 24)).ToString("00");
    }

    public string Timer
    {
        get { return _hours + ":" + _minutes + ":" + _seconds; }
    }

    public float _tMin, _tMax;

    public int EstimatorTimeNormalized()
    {
        float _rapidez = 0 ;
        //if (_timer < _tMin)
        //{
        //    _rapidez = 1;
        //}
        //else if(_timer > _tMax)
        //{
        //    _rapidez = 0;
        //}
        //else
        //{
        //    _rapidez = -(_timer- _tMin) / (_tMax - _tMin) + 1;
        //}
        float vel = 1/(_tMax - _tMin);
        float x0 = (_tMin + _tMax) / 2;
        _rapidez = Sigmoide(-vel*(_timer-x0));
        _rapidez = _rapidez / Sigmoide(-vel * (_tMin - x0));
        if (_rapidez > 1) _rapidez = 1;
        if (_rapidez < 0) _rapidez = 0;
        
        return (int)(_rapidez * 100);
    }

    public void ChangePlayingState(bool pState)
    {
        _isPlaying = pState;
    }


    private float Sigmoide(float x)
    {
        float y;
        y = 1 / (1+ Mathf.Exp(-x));

        return y;

    }


}
