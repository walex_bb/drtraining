﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CLoadSceneManager : MonoBehaviour
{

    public static CLoadSceneManager Inst;
    public string _sceneName;
    public GameObject _canvasLoadingPrefab;
    public string _FirstScene;
    public string _currentSceneToUnload;


    // Function called from outside to load new scene
    public void ChangeScene(string _sceneNameToLoad)
    {
        _sceneName = _sceneNameToLoad;
        LoadScene2();
    }

    void Awake()
    {

    }

    void OnEnable()
    {
        // SceneManager.sceneLoaded += ApagarCanvas;
        // SceneManager.sceneUnloaded += LoadScene;
    }

    void OnDisable()
    {
        // SceneManager.sceneLoaded -= ApagarCanvas;
        // SceneManager.sceneUnloaded -= LoadScene;
    }

    // Use this for initialization
    void Start()
    {
        if (Inst == null)
        {
            Inst = this;
            DontDestroyOnLoad(Inst.gameObject);
            DontDestroyOnLoad(GameObject.Find("EventSystem"));
        }
        LoadFirstScene();
        _currentSceneToUnload = SceneManager.GetActiveScene().name;
    }

    // First scene does not unload anything
    private void LoadFirstScene()
    {
        _sceneName = _FirstScene;
        StartCoroutine(AsynchronousLoad());
        /*_canvasLoading.SetActive(true);
        async = SceneManager.LoadSceneAsync(_sceneName, LoadSceneMode.Additive);  
        StartCoroutine(EsperarCarga());*/
    }

    //// Picks up the load additive end
    //private void ApagarCanvas(Scene _loadingScene, LoadSceneMode _loadmode)
    //{
    //    if (_loadingScene == SceneManager.GetSceneByName(_sceneName))
    //    {
    //        Debug.Log("Scene loaded successfully");
    //        _currentSceneToUnload = _sceneName;
    //        _canvasLoading.SetActive(false);            
    //    }
    //}



    //// Picks up the end of unload scene and loads the next scene
    //private void LoadScene(Scene _loadingScene)
    //{

    //    if (_loadingScene == SceneManager.GetSceneByName(_currentSceneToUnload))
    //    {
    //        Debug.Log("Scene Unloaded, Starting load");
    //        SceneManager.LoadScene(_sceneName, LoadSceneMode.Additive);

    //    }
    //}


    // Auxiliares dado que en el unity 3.6 no existen los delegados

    // Picks up the load additive end
    private void ApagarCanvas2()
    {
        Debug.Log("Scene loaded successfully");
        _currentSceneToUnload = _sceneName;
        StopAllCoroutines();
    }



    // Picks up the end of unload scene and loads the next scene
    private void LoadScene2()
    {
        Debug.Log("Scene Unloaded, Starting load");
        //async = SceneManager.LoadSceneAsync(_sceneName, LoadSceneMode.Additive);
        //SceneManager.LoadScene(_sceneName, LoadSceneMode.Additive);    
        //StartCoroutine(EsperarCarga());
        StartCoroutine(AsynchronousLoad());
    }

    IEnumerator AsynchronousLoad()
    {
        GameObject _canvasLoadingObj = Instantiate(_canvasLoadingPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        GameObject _puntitos = _canvasLoadingObj.GetComponentInChildren<CAnimarCarga>().gameObject;
        bool _wasLoaded = false;
        bool _waitForChange = false;
        //_okBtn.SetActive(false);
        yield return null;


        AsyncOperation ao = SceneManager.LoadSceneAsync(_sceneName);
        ao.allowSceneActivation = false;

        while (!ao.isDone)
        {
            // [0, 0.9] > [0, 1]
            float progress = Mathf.Clamp01(ao.progress / 0.90f);
            //Debug.Log("Loading progress: " + (progress * 100) + "%");

            // Loading completed
            if (ao.progress == 0.90f)
            {
                if (_puntitos != null)
                    _puntitos.SetActive(false);
                //_okBtn.SetActive(true);
                //_tipRotator.gameObject.SetActive(false);
                if (_wasLoaded || !_waitForChange)
                    ao.allowSceneActivation = true;
            }
            yield return null;
        }
    }



}
