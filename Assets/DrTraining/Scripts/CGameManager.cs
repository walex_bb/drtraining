﻿using UnityEngine;
using System.Collections;

public class CGameManager : MonoBehaviour {

    public UnityEngine.UI.Image[] _stepIndicatorVisual;
    public UnityEngine.UI.Image _firstStepDetectionVisual;
    
    public int _maxNumberOfSteps;
    private int _currentStep;

    

    // Use this for initialization
    void Start () {
        DelegatesSubscription(true);
	}

    void OnDestroy() {
        DelegatesSubscription(false);
    }
	
	// Update is called once per frame
	void Update () {
	
	}


    public void FirstDetectionDone()
    {
        CUITimeCounter.Inst.Activate();
        StartCoroutine(ChangeIndicatorAlpha(_firstStepDetectionVisual));
        ARDelegates.AROn -= FirstDetectionDone;
    }


    public void OnStepCompleted() {
        StartCoroutine(ChangeIndicatorAlpha(_stepIndicatorVisual[_currentStep]));
        _currentStep++;
        if (_currentStep == _maxNumberOfSteps)
            ShowResults();
        if (_currentStep == 1)
            CUITimeCounter.Inst.Activate(); 
    }

    void ShowResults() {
        CResultsManager.Inst.MostrarResultados();        
    }


    IEnumerator ChangeIndicatorAlpha(UnityEngine.UI.Image _image) {
        Color c = _image.color;
        for (int i = 0; i < 10; i++) {
            c.a += i / 10.0f;
            _image.color = c;
            yield return new WaitForSeconds(0.1f);
        }
    }

    void DelegatesSubscription(bool _subscribing) {
        if (_subscribing)
        {
            CDelegates.AddActivityStep += OnStepCompleted;
            ARDelegates.AROn += FirstDetectionDone;
        }
        else {
            CDelegates.AddActivityStep -= OnStepCompleted;
            ARDelegates.AROn -= FirstDetectionDone;
        }
    }
}
