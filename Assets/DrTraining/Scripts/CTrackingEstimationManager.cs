﻿using UnityEngine;
using System.Collections;

public class CTrackingEstimationManager : MonoBehaviour {

    public static CTrackingEstimationManager Inst;

    private bool _activated;
    public float _trackingEstimator;
    public float _timerOut;
    public float _tMin, _tMax;

    void Awake()
    {
        Inst = this;
    }

    public void ResumeTrackingEst()
    {
        _activated = true;
    }

    public void PauseTrackingEst()
    {
        _activated = false;
    }

    public void ResetTrackingEst()
    {
        _timerOut = 0;
    }

    public void StartTrackingEst()
    {
        ResetTrackingEst();
        ResumeTrackingEst();
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (_activated)
        {
            ProcesarMuestra();
        }

    }

    private void ProcesarMuestra()
    {
        _timerOut += Time.deltaTime;

    }


    public int EstimatorTrackingNormalized()
    {
        float _trackingEst = 0;
        //if (_timerOut < _tMin)
        //{
        //    _trackingEst = 1;
        //}
        //else if (_timerOut > _tMax)
        //{
        //    _trackingEst = 0;
        //}
        //else
        //{
        //    _trackingEst = -(_timerOut - _tMin) / (_tMax - _tMin) + 1;
        //}

        float vel = 1 / (_tMax - _tMin);
        float x0 = (_tMin + _tMax) / 2;
        _trackingEst = Sigmoide(-vel * (_trackingEst - x0));
        _trackingEst = _trackingEst / Sigmoide(-vel * (_tMin - x0));
        if (_trackingEst > 1) _trackingEst = 1;
        if (_trackingEst < 0) _trackingEst = 0;


        return (int)(_trackingEst * 100);
    }

    private float Sigmoide(float x)
    {
        float y;
        y = 1 / (1 + Mathf.Exp(-x));

        return y;

    }

}
