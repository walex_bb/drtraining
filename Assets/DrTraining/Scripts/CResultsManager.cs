﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SSkillFormatted
{
    public int Id;
    public string Score;

    public SSkillFormatted(int id, float score) {
        Id = id;
        Score = score.ToString();
    }
}

[System.Serializable]
public class SScenaryResultFormatted
{
    public int Id; // execersice id
    public string Timestamp;
    public List<SSkillFormatted> Scores;

    [NonSerialized]
    public double _seconds;
    [NonSerialized]
    public string _userEmail;
}

[System.Serializable]
public class SPersistScenaryResultFormattedList {
    public List<SScenaryResultFormatted> _resultsFormatted;
}

public class CResultsManager : MonoBehaviour {


    public GameObject _statisticsCardPanelPrefab;
    public Transform _cardPanelInstantiatedParent;
    public static CResultsManager Inst;
    public int _levelIndex;
    [SerializeField]
    float _finalPanelDelay;

    public GameObject _resultsPanel;

    public EActivityCode _escenario;

    void Awake()
    {
        Inst = this;
    }

    public void MostrarResultados()
    {
        StartCoroutine(DelayedMostrarResultados());
    }

    IEnumerator DelayedMostrarResultados()
    {
        yield return new WaitForSeconds(_finalPanelDelay);

        SScenaryResultFormatted _scenaryResultFormatted = new SScenaryResultFormatted();
        _scenaryResultFormatted.Id = _levelIndex;
        _scenaryResultFormatted.Scores = new List<SSkillFormatted>();

        CAudioManager.Inst.PlaySFX("YouWin");
        CAudioManager.Inst.PlayMusic("WinTheme");
        string _name;
        bool _callAGI = false;
        bool _callLNAV = false;
        bool _callANAV = false;
        bool _callSTE = false;
        bool _callTRA = false;
        bool _callAIM = false;
        bool _callHC = false;
        switch (_escenario)
        {
            case EActivityCode.TOUCH_AND_PROBE_STATIONARY:
                _name = "FIXED CAMERA STATIONARY TARGET";
                _callAGI = true;
                _callSTE = true;
                _callLNAV = true;
                _callANAV = true;
                break;
            case EActivityCode.TOUCH_AND_PROBE_MOVING:
                _name = "FIXED CAMERA MOVING TARGET";
                _callAGI = true;
                _callLNAV = true;
                _callTRA = true;
                break;
            case EActivityCode.IMAGE_CENTERING_1:
                _name = "MOVING CAMERA STATIONARY TARGET (1)";
                _callAGI = true;
                _callSTE = true;
                _callLNAV = true;
                _callANAV = true;
                break;
            case EActivityCode.IMAGE_CENTERING_2:
                _name = "MOVING CAMERA STATIONARY TARGET (2)";
                _callAGI = true;
                _callSTE = true;
                _callLNAV = true;
                _callANAV = true;
                break;
            case EActivityCode.TRACKING:
                _name = "MOVING CAMERA MOVING TARGET";
                _callAGI = true;
                _callLNAV = true;
                _callTRA = true;
                break;
            case EActivityCode.IMAGE_ORIENTATION:
                _name = "IMAGE ORIENTATION";
                _callAGI = true;
                _callSTE = true;
                _callLNAV = true;
                _callANAV = true;
                _callAIM = true;
                break;
            case EActivityCode.HORIZONT_CONTROL:
                _name = "HORIZON CONTROL";
                _callAGI = true;
                _callSTE = true;
                _callLNAV = true;
                _callANAV = true;
                _callAIM = true;
                _callHC = true;
                break;
            case EActivityCode.PERISCOPING:
                _name = "PERISCOPING";
                _callAGI = true;
                _callSTE = true;
                _callLNAV = true;
                _callANAV = true;
                _callAIM = true;
                _callHC = true;
                break;
            default:
                _name = "Error en nombre";
                break;

        }
        Debug.Log("Corriendo DelayedIndicatorValuesIC");
        //GameObject _cardPanel = Instantiate(_statisticsCardPanelPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        //_cardPanel.transform.SetParent(_cardPanelInstantiatedParent, false);
        yield return new WaitForEndOfFrame();
        //_cardPanel.GetComponent<CCardPanel>().SetSceneName(_name);


        int _steadiness = -1;
        int _agility = -1;
        int _linearNavigation = -1;
        int _angularNavigation = -1;
        int _tracking = -1;
        int _controlOfHorizont = -1;
        int _centering = -1;
        

        float _agilityRaw = 0;
        float _steadinessRaw = 0;
        float _linearNavigationRaw = 0;

        
        if (_callAGI)
        {
            //_agility = CUITimeCounter.Inst.EstimatorTimeNormalized();
            _agility = CSkillsMeasurement.Inst.AgilityNormalized(out _agilityRaw);
           // _cardPanel.GetComponent<CCardPanel>().SetIndicator("AGI", _agility);
        }
        if (_callSTE)
        {
            //_steadiness = SteadinessManager.Inst.EstimatorSteadinessNormalized();
            _steadiness = CSkillsMeasurement.Inst.SteadinessNormalized(out _steadinessRaw);
           // _cardPanel.GetComponent<CCardPanel>().SetIndicator("STE", _steadiness);
        }
        if (_callLNAV)
        {
            _linearNavigation = CSkillsMeasurement.Inst.LinearNavigationNormalized(out _linearNavigationRaw);
           // _cardPanel.GetComponent<CCardPanel>().SetIndicator("LNAV", _linearNavigation);
        }
        if (_callANAV)
        {
            _angularNavigation = CSkillsMeasurement.Inst.AngularNavigationNormalized();
            //_cardPanel.GetComponent<CCardPanel>().SetIndicator("ANAV", _angularNavigation);
        }
        if (_callTRA)
        {
            _tracking = CSkillsMeasurement.Inst.TrackingNormalized();
          //  _cardPanel.GetComponent<CCardPanel>().SetIndicator("TRA", _tracking);
        }
        if (_callAIM)
        {
            _centering = CSkillsMeasurement.Inst.CenteringNormalized();
           // _cardPanel.GetComponent<CCardPanel>().SetIndicator("AIM", _centering);
        } 
        if(_callHC)
        {
            _controlOfHorizont = CSkillsMeasurement.Inst.HCNormalized();
           // _cardPanel.GetComponent<CCardPanel>().SetIndicator("HC", _controlOfHorizont);
        }
        

        string _email = PlayerPrefs.GetString("email");
        if (_email != ""){

            _scenaryResultFormatted.Scores.Add(new SSkillFormatted(1, _linearNavigationRaw)); // distance
            _scenaryResultFormatted.Scores.Add(new SSkillFormatted(2, _agilityRaw)); // time in millisecs            
            _scenaryResultFormatted.Scores.Add(new SSkillFormatted(3, _steadinessRaw)); // accuracy ( new sphere colission )

            // _scenaryResultFormatted.Scores.Add(new SSkillFormatted(3, _angularNavigationRaw));
            // _scenaryResultFormatted.Scores.Add(new SSkillFormatted(5, _trackingRaw));
            //_scenaryResultFormatted.Scores.Add(new SSkillFormatted(6, _centering));
            //_scenaryResultFormatted.Scores.Add(new SSkillFormatted(7, _controlOfHorizont));

            _scenaryResultFormatted._seconds = Mathf.RoundToInt(GetComponent<CSkillsMeasurement>()._timer);
            //_scenaryResultFormatted._userEmail = _email;
            _scenaryResultFormatted.Timestamp = XMLRequestResponsesParser.ParseDate(DateTime.Now);
        }

        CLocalPersist.UpdateUserLevel(_levelIndex);

        // List<SScenaryResultFormatted> _scenaryResults = new List<SScenaryResultFormatted>();
        
        GlobalScenaryData.Inst._localScenaryResults.Add(_scenaryResultFormatted);
        GlobalScenaryData.Inst.SaveLocalScenaryResultsToDisk();

        //_scenaryResults.Add(_scenaryResultFormatted);

        if (_resultsPanel != null)
        {
            CResultsPanel panel = _resultsPanel.GetComponent<CResultsPanel>();
            panel.SetExerciceName(_name);
            panel.SetDistance(_linearNavigationRaw);
            panel.SetTime(_agilityRaw);
            panel.SetAccuracy(_steadinessRaw);
            _resultsPanel.SetActive(true);
            
        }
    }

    public void BackToAllGames()
    {        
        if (_resultsPanel != null)
        {
            CResultsPanel panel = _resultsPanel.GetComponent<CResultsPanel>();
            panel.BackToAllGames();
        }
    }

}

