﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CConfigPreviewMenu : MonoBehaviour {
    
    public GameObject _menuPanel;
    private CPreviewCameraManager _manager;
    [Space(30)]
    public Button _fixedCameraOn, _fixedCameraOff;

    [Space(30)]
    public Button _instantantOn, _instantantOff;


    void Start() {
        _manager = GetComponent<CPreviewCameraManager>();
    }

    public void OpenMenu() {
        _menuPanel.SetActive(true);
        SetFixedCameraButtonsState();
        SetInstantantHideButtonsState();
    }

    public void CloseMenu() {
        _menuPanel.SetActive(false);
    }

    public void ToggleFixedCamera() {
        _manager._useFixedCamera = !_manager._useFixedCamera;
        SetFixedCameraButtonsState(); 
    }

    public void ToggleInstantantHideOnTracking() {
        _manager._instantantHideOnTrackingFound = !_manager._instantantHideOnTrackingFound;
        SetInstantantHideButtonsState();
    }

    void SetFixedCameraButtonsState(){
        if (_manager._useFixedCamera)
        {
            _fixedCameraOn.gameObject.SetActive(true);
            _fixedCameraOff.gameObject.SetActive(false);
            if (_manager._instantantHideOnTrackingFound)
                ToggleInstantantHideOnTracking();
        }
        else
        {
            _fixedCameraOn.gameObject.SetActive(false);
            _fixedCameraOff.gameObject.SetActive(true);
        } 
    }

    void SetInstantantHideButtonsState() {
        if (_manager._instantantHideOnTrackingFound)
        {
            _instantantOn.gameObject.SetActive(true);
            _instantantOff.gameObject.SetActive(false);
        }
        else
        {
            _instantantOn.gameObject.SetActive(false);
            _instantantOff.gameObject.SetActive(true);
        }
    }
}
