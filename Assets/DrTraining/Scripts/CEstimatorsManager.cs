﻿using Vuforia;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System;

public class CEstimatorsManager : MonoBehaviour, ITrackableEventHandler
{

    public GameObject _imageTarget;
    private TrackableBehaviour mTrackableBehaviour;
    public bool _trackeando;

    // Steadiness
    public static float _media;
    public static float _potencia;
    public static float _varianza2;
    public static int _n;
    public Transform _steadinessGO;
    public static float _steadiness;
    public bool _registrar;
    //public Text _textSteadiness;

    //using for map from reals to 0-100 int scale
    static float _worstStead = 0.0f;
    static float _bestStead = 0.12f;

    public static float [] _steadinessSamples = {0, 0, 0};
    public string _animationCode;

    // Use this for initialization
    void Start()
    {
        mTrackableBehaviour = _imageTarget.GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
        ReiniciarSteadiness();
        DelegatesSubscription(true);
    }

    void OnDestroy() {
        DelegatesSubscription(false);
    }

    void DelegatesSubscription(bool _isSubscribe) {
        if (_isSubscribe) {
            CDelegates.AnimationEnd += OnAnimationEnd;
        }
        else{
            CDelegates.AnimationEnd -= OnAnimationEnd;
        }
    }

    void OnAnimationEnd(string _code) {        
        if (_code == _animationCode) { 
        
        }
    }


    public void RegistrarOn()
    {
        _registrar = true;
    }
    public void RegistrarOff()
    {
        _registrar = false;
    }

    public static void RegisterSteadiness(int _currentStep) {
        _steadinessSamples[_currentStep] = _steadiness;
    }

    public static void ReiniciarSteadiness()
    {
        _media = 0f;
        _potencia = 0f;
        _varianza2 = 0f;
        _n = 0;
        
    }

    private float ProcesarDatoSteadiness(Vector3 _pos)
    {
        float _x = _pos.magnitude;
        _n = _n + 1;
        _media = (_x + (_n - 1) * _media) / _n;
        _potencia = (_x * _x + (_n - 1) * _potencia) / _n;
        _varianza2 = _potencia - _media * _media;
        float _var = (float)Math.Sqrt(_varianza2);
        return _var;
    }

    public static int SteadinessNormalized(float _steadValue) { // not used
        float _newMin = 0;
        float _newMax = 100;

        float _oldRange = (_bestStead - _worstStead);
        float _newRange = (_newMax - _newMin);
        float _newValue = (((_steadValue - _worstStead) * _newRange) / _oldRange) + _newMin;
        if (_newValue > 100f) _newValue = 100f;
        return ((int)_newValue);
    }

    public static int GetSteadinessPromediumNormalized()
    {
        float _steadProm = 0;
        for (int i = 0; i < _steadinessSamples.Length; i++)
            _steadProm += _steadinessSamples[i];
        _steadProm /= (_steadinessSamples.Length * 1.0f);
        
        return SteadinessNormalized(_steadiness);
    }

    // Update is called once per frame
    void Update()
    {
        if (_trackeando & _registrar)
        {
            // Guardamos todo
            _steadiness = ProcesarDatoSteadiness(_steadinessGO.position);
        }
    }

    // Deteccion Vuforia
    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED)
        {
            _trackeando = true;
            _registrar = true;
        }
        else
        {
            _trackeando = false;
            _registrar = false;
            CEstimatorsManager.ReiniciarSteadiness();
        }
    }

}
