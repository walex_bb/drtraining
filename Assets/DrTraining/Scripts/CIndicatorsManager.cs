﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CIndicatorsManager : MonoBehaviour {

    const string GET_SCENARIES_STATUS = "ExercisesDataInterchange/GetExercisesStatus";
    const string GET_BEST_SCENARY_RESULTS = "ExercisesSummaries/GetBestExercisesResults";

    public Button[] _activityButton;
	public Image[] _activityLock;
	public Sprite _unlockedSprite;
	public  Sprite _finishedSprite;
	private int _CantEscenarios;

    public Sprite _lockedSprite;

    void OnEnable()
	{
        //CLocalPersist.LoadUserDataDirect(PlayerPrefs.GetString("email"));
        //StartCoroutine(UnlockUntilLastLevel());

        StartCoroutine(UnlockAndSync());   
	}

    IEnumerator UnlockAndSync() {
        string _keyString = (PlayerPrefs.GetString("email") != "" ? PlayerPrefs.GetString("email") : "GUESTDATA");
        CLocalPersist.LoadUserLevelData(_keyString);

        int _lastLevel = CLocalPersist._userLevel._lastLevel;
        if (_lastLevel <= 0 || _lastLevel >= 8)
        {
            _lastLevel = 0;
        }

        _lastLevel++;

        //int _greaterIndex = -1;
        //RenderUnlocking(_greaterIndex, _lastLevel);
        RenderUnlocking(_lastLevel);
        yield return new WaitForEndOfFrame();

        /*
        if (!CLocalAuthentication._guestMode)
        {

            WWWForm _form = new WWWForm();
            Debug.Log("TOKEN " + PlayerPrefs.GetString("token"));
            Debug.Log("email " + PlayerPrefs.GetString("email"));
            byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLGetBestScenaryResultsSend(PlayerPrefs.GetString("token")));
            Dictionary<string, string> _headers = _form.headers;
            _headers["Accept"] = "application/xml";
            _headers["Content-Type"] = "application/xml";

            GameObject _loadingAdvice = GameObject.FindWithTag("LoadingAdvice");
            _loadingAdvice.GetComponent<CLoadingAdvice>().On();
            WWW _www = new WWW(GameConstants.BASE_URL + GET_BEST_SCENARY_RESULTS, _raw, _headers);
            yield return _www;


            if (_www.error != null)
            {
                Debug.Log("" + _www.error);
                _loadingAdvice.GetComponent<CLoadingAdvice>().Off();
            }
            else
            {
                CLocalPersist._bestScoreActivities = XMLRequestResponsesParser.XMLGetBestScenaryResultsParse(_www.text);
                if (CLocalPersist._bestScoreActivities.Count != 0)
                {
                    foreach (var _key in CLocalPersist._bestScoreActivities.Keys)
                    {
                        if ((int)CLocalPersist._bestScoreActivities[_key]._code > _greaterIndex)
                            _greaterIndex = (int)CLocalPersist._bestScoreActivities[_key]._code;
                    }
                }
                _loadingAdvice.GetComponent<CLoadingAdvice>().Off();
            }
            RenderUnlocking(_greaterIndex, _lastLevel);
        }*/
    }

    //public void RenderUnlocking(int _greaterIndex, int _lastLevel) {
    public void RenderUnlocking(int _lastLevel)
    {


        /*
        if (_greaterIndex > _lastLevel)
        {
            _lastLevel = _greaterIndex;
        }
        */

        /*
        if (_lastLevel == -1)
        {
            _activityButton[0].interactable = true;
            _activityLock[0].sprite = _unlockedSprite;
        }
        else
        {*/
       
        for (int i = 1; i < _activityButton.Length; i++)
        {
            _activityButton[i].interactable = (i == _lastLevel) ? true : false;
            _activityLock[i].sprite = (i == _lastLevel) ? _unlockedSprite : _lockedSprite;
        }

        
            /*
            //Debug.Log(_lastLevel + "LastLevel");
            for (int i = 0; i <= _lastLevel + 1; i++)
            {
                if (i < _activityButton.Length)
                {
                    _activityButton[i].interactable = true;
                    if (i == _lastLevel + 1)
                    {
                        _activityLock[i].sprite = _unlockedSprite;
                    }
                    else
                    {
                        _activityLock[i].sprite = _finishedSprite;
                    }
                }
            }
            

            if (_lastLevel + 1 > _activityButton.Length)
            {
                if (CLocalPersist._bestScoreActivities.ContainsKey(EActivityCode.PERISCOPING))
                    _activityLock[_lastLevel].sprite = _finishedSprite;
                else
                    _activityLock[_lastLevel].sprite = _unlockedSprite;
            }

    
        }*/

        //CLocalPersist.UpdateUserLevel(_lastLevel);
    }

    IEnumerator UnlockUntilLastLevel() {
        WWWForm _form = new WWWForm();
        byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLGetExercisesStatusResultSend(PlayerPrefs.GetString("token")));
        Dictionary<string, string> _headers = _form.headers;
        _headers["Accept"] = "application/xml";
        _headers["Content-Type"] = "application/xml";

        WWW _www = new WWW(GameConstants.BASE_URL + GET_SCENARIES_STATUS, _raw, _headers);
        yield return _www;

        List<int> _playedGameCodes = null;

        if (_www.error != null)
            Debug.Log("" + _www.error);
        else
            _playedGameCodes = XMLRequestResponsesParser.XMLGetExercisesStatusResultParse(_www.text);


        int _lastLevel = -1;
        if (_playedGameCodes.Count != 0)
        {
            _playedGameCodes.Sort();
            _lastLevel = _playedGameCodes[_playedGameCodes.Count - 1] - 1;
        }
        _lastLevel = 8;
        //int _lastLevel = CLocalPersist._currentUser._lastLevel;
        if (_lastLevel == -1)
        {
            _activityButton[0].interactable = true;
            _activityLock[0].sprite = _unlockedSprite;
        }
        else
        {

            Debug.Log(_lastLevel + "LastLevel");
            for (int i = 0; i <= _lastLevel + 1; i++)
            {
                if (i < _activityButton.Length)
                {
                    _activityButton[i].interactable = true;
                    if (i == _lastLevel + 1)
                    {
                        _activityLock[i].sprite = _unlockedSprite;
                    }
                    else
                    {
                        _activityLock[i].sprite = _finishedSprite;
                    }
                }
            }

            if (_lastLevel + 1 > _activityButton.Length)
            {
                if (CLocalPersist._bestScoreActivities.ContainsKey(EActivityCode.PERISCOPING))
                    _activityLock[_lastLevel].sprite = _finishedSprite;
                else
                    _activityLock[_lastLevel].sprite = _unlockedSprite;
            }
        }
    }


	// Use this for initialization
	void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
