﻿using UnityEngine;
using System.Collections;

public class CBallImageCentering : MonoBehaviour {
    public string _animationCode;

    public void EndAnimation()
    {
        Debug.Log("Animation end triggered");
        CDelegates.OnAnimationEnd(_animationCode);

    }
}
