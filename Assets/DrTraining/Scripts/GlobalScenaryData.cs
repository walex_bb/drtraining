﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class MessageInfo
{
    public int Id;
    public int Type;
    public string Description;
}

[System.Serializable]
public class SkillsDataList
{
    public int Status;
    public List<MessageInfo> Messages;
    public List<SSkillData> SkillsData;  
}

[System.Serializable]
public class SSkillData {
    public int Id;
    public string Name;
    public string Description;
}

[System.Serializable]
public class SScenaryData {
    public int Id;
    public string Name;
    public string Description;
    public string TypeId; // not used
    public string TypeName;   // not used
}

[System.Serializable]
public class ScenariesDataList
{
    public int Status;
    public List<MessageInfo> Messages;
    public List<SScenaryData> ExercisesData;
}

[System.Serializable]
public class ScenaryResultRequest
{
    public ScenaryResultRequest(string token, List<SScenaryResultFormatted> results)
    {
        Token = token;
        ExerciseResult = results;
    }
    public string Token;
    public List<SScenaryResultFormatted> ExerciseResult;
}

[System.Serializable]
public class RaceDataRequest
{
    public RaceDataRequest(string token)
    {
        Token = token;        
    }
    public string Token;    
}

[System.Serializable]
public class RaceData
{   
    public int SquadId;
    public string SquadName;
    public string UserCompletedLaps;
    public string UserLastCompletedLap;
    public int UserNextExerciseId;
    public string UserNextExerciseName;
    public int LapsToNextSquad;
 
}

[System.Serializable]
public class RaceDataResponse : GenericResponse
{
    public RaceDataResponse(string token)
    {
        Token = token;
    }
    public string Token;
    public RaceData RaceData;
}

[System.Serializable]
public class SKillRanking
{
    public int SkillId;
    public string SKillName;
    public int Position;
    public float Score;
}

[System.Serializable]
public class Ranking
{
    public int position;
    public string UserName;
    public string Score;
    public bool CurrentUser;
}

[System.Serializable]
public class Skill
{
    public int SkillId;
    public string SkillName;
    public List<Ranking> Ranking;
}

[System.Serializable]
public class GlobalRanking
{
    public int SquadId;
    public string SquadName;
    public List<Skill> Skill;
}

[System.Serializable]
public class PersonalRanking
{
    public int SquadId;
    public string SquadName;
    public int NextExerciseId;
    public string NextExerciseName;
    public List<SKillRanking> SKillRanking;        
}

[System.Serializable]
public class PersonalRankingRequest
{
    public PersonalRankingRequest(string token)
    {
        Token = token;
    }
    public string Token;
}

[System.Serializable]
public class GlobalRankingRequest
{
    public GlobalRankingRequest(string token)
    {
        Token = token;
    }
    public string Token;
}

[System.Serializable]
public class Circuit
{
    public int CircuitStepId;
    public int ExerciseId;
    public string ExerciseName;
}

[System.Serializable]
public class RaceInformation
{
    public string RaceName;
    public string RaceDescription;
    public string RaceStartDate;
    public int RaceGoldSquadSize;
    public int RaceSilverSquadSize;
    public int RaceBronzeSquadSize;
    public List<Circuit> Circuit;
}

[System.Serializable]
public class RaceInformationRequest
{
    public RaceInformationRequest(string token)
    {
        Token = token;
    }
    public string Token;
}

[System.Serializable]
public class RaceInformationResponse : GenericResponse
{
    public RaceInformation RaceInformation;

}

[System.Serializable]
public class GlobalRankingResponse : GenericResponse
{
    public List<GlobalRanking> GlobalRanking;
 
}

[System.Serializable]
public class PersonalRankingResponse : GenericResponse
{
    public PersonalRanking PersonalRanking;
}

public class GlobalScenaryData : MonoBehaviour {
    const string SUBMIT_SCENARY_RESULTS = "ExercisesDataInterchange/SubmitExerciseResults";
    

    public static GlobalScenaryData Inst;
    public List<SScenaryData> _scenariesData;
    public List<SSkillData> _skillsData;
    public List<SScenaryResultFormatted> _localScenaryResults;

    public delegate void SubmitScenaryResultDone(bool success);
    public static event SubmitScenaryResultDone OnSubmitScenaryResultDone = null;

    public delegate void GetRaceDataResultDone(RaceData rd);
    public static event GetRaceDataResultDone OnGetRaceDataResultDone = null;

    public delegate void GetPersonalRankingFromServerDone(PersonalRanking pr);
    public static event GetPersonalRankingFromServerDone OnGetPersonalRankingFromServerDone = null;

    public delegate void GetGlobalRankingFromServerDone(List<GlobalRanking> gr);
    public static event GetGlobalRankingFromServerDone OnGetGlobalRankingFromServerDone = null;

    public delegate void GetRaceInformationFromServerDone(RaceInformation ri);
    public static event  GetRaceInformationFromServerDone OnGetRaceInformationFromServerDone = null;
    
    void Awake() {
        Inst = this;
        _localScenaryResults = new List<SScenaryResultFormatted>();
        DontDestroyOnLoad(Inst.gameObject);
        StartCoroutine(GetScenariesDataRequest());
        StartCoroutine(GetAbilitiesDataRequest());
    }

    const string GET_SCENARIES_DATA_URL = "ExercisesDataInterchange/GetScenariesData";
    const string GET_SKILLS_DATA_URL = "ExercisesDataInterchange/GetSkillsData";

    const string GET_RACE_DATA_URL = "Race/RaceData";
    const string GET_PERSONAL_RANKING_URL = "Race/PersonalRanking";
    const string GET_GLOBAL_RANKING_URL = "Race/GlobalRanking";
    const string GET_RACE_INFORMATION_URL = "Race/RaceInformation";

    IEnumerator GetAbilitiesDataRequest()
    {
        //WWWForm _form = new WWWForm();
        //Dictionary<string, string> _headers = _form.headers;
        //_headers["Accept"] = "application/xml";

        Dictionary<string, string> _headers = new Dictionary<string, string>();
        _headers["Content-Type"] = "application/json";

        WWW _www = new WWW(GameConstants.BASE_URL + GET_SKILLS_DATA_URL, new byte[] { 0 }, _headers);
        yield return _www;

        if (_www.error != null)
        {
            Debug.Log("" + _www.error);
        }
        else
        {
            List<SSkillData> _skillsDataFromServer = XMLRequestResponsesParser.XMLGetSkillsDataParse(_www.text);
            for (int i = 0; i < _skillsDataFromServer.Count; i++)
            {
                SSkillData _dataFromServer = _skillsDataFromServer[i];
                int _dataLocallyIndex = Inst._skillsData.FindIndex(a => a.Id == _dataFromServer.Id);
                if (_dataLocallyIndex != -1)
                {
                    Inst._skillsData[_dataLocallyIndex] = _dataFromServer;
                }
                else {
                    Inst._skillsData.Add(_skillsDataFromServer[i]);
                }
            }
            Debug.Log(_www.text);
        }
        yield return null;
    }

    IEnumerator GetScenariesDataRequest() {
        //WWWForm _form = new WWWForm();
        //byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLUserRecoveryPasswordSend(_recoveryPasswordEmail.text));
        //Dictionary<string, string> _headers = _form.headers;
        //_headers["Accept"] = "application/xml";
        //_headers["Content-Type"] = "application/xml";

        //_headers["Accept"] = "application/json";
        Dictionary<string, string> _headers =new Dictionary<string, string>();
        _headers["Content-Type"] = "application/json";

        //new byte[] { 0 })
        string url = GameConstants.BASE_URL + GET_SCENARIES_DATA_URL;
        WWW _www = new WWW(url, new byte[] {0}, _headers);
              
        yield return _www;

        if (_www.error != null)
        {
            Debug.Log("" + _www.error);
        }
        else
        {
            List<SScenaryData> _scenariesDataFromServer = XMLRequestResponsesParser.XMLGetScenariesDataParse(_www.text);
            for (int i = 0; i < _scenariesDataFromServer.Count; i++) {
                SScenaryData _dataFromServer = _scenariesDataFromServer[i];
                int _dataLocallyIndex = Inst._scenariesData.FindIndex(a => a.Id == _dataFromServer.Id);
                if (_dataLocallyIndex != -1) {
                    Inst._scenariesData[_dataLocallyIndex] = _dataFromServer;
                }
            }
                Debug.Log(_www.text);
        }
        yield return null;
    }

    //call it when send results to server or when new activity is played
    public void SaveLocalScenaryResultsToDisk() {
        SPersistScenaryResultFormattedList _resultsList = new SPersistScenaryResultFormattedList();
        _resultsList._resultsFormatted = _localScenaryResults;

        BinaryFormatter bf = new BinaryFormatter();
        string _nameKey = GetNameKey();

        FileStream file = File.Create(Application.persistentDataPath + "/DR_TRAINING_LOCAL_USER_" + _nameKey + ".gd");
        bf.Serialize(file, _resultsList);
        file.Close();

    }

    string GetNameKey(){
        return CLocalAuthentication._guestMode ? "GUESTDATA" : PlayerPrefs.GetString("email");
    }

    const string UPDATE_USER_DATA = "UserManagement/UpdateUserData";
    public delegate void SubmitUserDataResultDone(bool result);
    public static SubmitUserDataResultDone OnSubmitUserDataResultDone = null;

    public void SubmitUserDataResult(string userName)
    {
        StartCoroutine(SubmitUserDataResultAsync(userName));
    }

    public IEnumerator SubmitUserDataResultAsync(string userName)
    {

        string _firstName = userName;
        string _lastName = "na";
        string _bornDate = "1900-01-01";
        string _workPlace = "na";
        string _profession = "na";
        UpdateUserInfoRequest uer = new UpdateUserInfoRequest(PlayerPrefs.GetString("token"), new UserInfo(_firstName, _lastName, _bornDate, _workPlace, _profession));
        byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLUpdateUserDataSend(uer));
        Dictionary<string, string> _headers = new Dictionary<string, string>();
        _headers["Content-Type"] = "application/json";

        WWW _www = new WWW(GameConstants.BASE_URL + UPDATE_USER_DATA, _raw, _headers);
        yield return _www;

        if (_www.error != null)
        {
            Debug.Log("" + _www.error);
        }
        else
        {
            GenericResponse gr = JsonUtility.FromJson<GenericResponse>(_www.text);
            if (gr.Status == 0)
            {
                Debug.Log(_www.text);
                CLocalPersist._currentUser._firstName = _firstName;
            }
            else
            {
                string errDesc = "XMLUpdateUserDataSend error " + gr.Status.ToString() + '\n';
                string m = gr.Message;
                errDesc += m;
                Debug.Log(errDesc);
            }

            if (OnSubmitUserDataResultDone != null)
            {

                OnSubmitUserDataResultDone(gr.Status == 0);
                OnSubmitUserDataResultDone = null;
            }
        }
    }

    //call it when login
    public void LoadLocalScenaryResultsFromDisk() {
        string _nameKey = GetNameKey();
        string _fileName = "/DR_TRAINING_LOCAL_USER_" + _nameKey + ".gd";
        if (File.Exists(Application.persistentDataPath + _fileName))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + _fileName, FileMode.Open);
            SPersistScenaryResultFormattedList _resultsList = new SPersistScenaryResultFormattedList();
            _resultsList = (SPersistScenaryResultFormattedList)bf.Deserialize(file);
            _localScenaryResults = _resultsList._resultsFormatted;
            file.Close();
        }
    }

    public void MoveGuestLocalResultsToUserLocalResults(string _email) {
        SPersistScenaryResultFormattedList _resultsList = new SPersistScenaryResultFormattedList();
        _resultsList._resultsFormatted = _localScenaryResults;
        for (int i = 0; i < _resultsList._resultsFormatted.Count; i++)
            _resultsList._resultsFormatted[i]._userEmail = _email;

        BinaryFormatter bf = new BinaryFormatter();
        string _nameKey = _email;

        FileStream file = File.Create(Application.persistentDataPath + "/DR_TRAINING_LOCAL_USER_" + _nameKey + ".gd");
        bf.Serialize(file, _resultsList);
        file.Close();

        if (CLocalPersist._userLevel == null)
            CLocalPersist._userLevel = new SUserLevel(-1);
        CLocalPersist.SaveUserLevelData(_nameKey);

        DeleteGuestData();
    }

    public void DeleteGuestData() {
        string _guestFileName = "/DR_TRAINING_LOCAL_USER_" + "GUESTDATA" + ".gd";
        if (File.Exists(Application.persistentDataPath + _guestFileName))
        {
            File.Delete(Application.persistentDataPath + _guestFileName);
        }
    }

    public void ClearCurrentPlayerData() {
        _localScenaryResults = new List<SScenaryResultFormatted>();
        if (CLocalPersist._userLevel == null)
            CLocalPersist._userLevel = new SUserLevel(-1);
        CLocalPersist._userLevel._lastLevel = -1;
    }

    public void GetRaceDataFromServer()
    {
        StartCoroutine(GetRaceDataFromServerAsync());
    }

    public void SendActivityResultsToServer() {
        StartCoroutine(SendActivityResultsToServerAsync());
    }


    public void GetRaceInformationFromServer()
    {
        StartCoroutine(GetRaceInformationFromServerAsync());
    }

    IEnumerator GetRaceInformationFromServerAsync()
    {
        string url = GameConstants.BASE_URL + GET_RACE_INFORMATION_URL;
        byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLRaceInformationRequest(new RaceInformationRequest(PlayerPrefs.GetString("token"))));

        Dictionary<string, string> _headers = new Dictionary<string, string>();
        _headers["Content-Type"] = "application/json";
        WWW _www = new WWW(url, _raw, _headers);

        yield return _www;

        bool success = false;
        string errorDesc = "";
        RaceInformationResponse gr = null;
        if (_www.error != null)
        {
            errorDesc = _www.error;
        }
        else
        {
            gr = JsonUtility.FromJson<RaceInformationResponse>(_www.text);
            success = (gr.Status == 0);
            errorDesc = gr.Message;
        }

        Debug.Log(errorDesc);

        if (OnGetRaceInformationFromServerDone != null)
        {

            OnGetRaceInformationFromServerDone(success == true ? gr.RaceInformation : null);
            OnGetRaceInformationFromServerDone = null;
        }
    }

    public void GetGlobalRankingFromServer()
    {
        StartCoroutine(GetGlobalRankingFromServerAsync());
    }

    IEnumerator GetGlobalRankingFromServerAsync()
    {
        string url = GameConstants.BASE_URL + GET_GLOBAL_RANKING_URL;
        byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLGlobalRankingRequest(new GlobalRankingRequest(PlayerPrefs.GetString("token"))));

        Dictionary<string, string> _headers = new Dictionary<string, string>();
        _headers["Content-Type"] = "application/json";
        WWW _www = new WWW(url, _raw, _headers);

        yield return _www;

        bool success = false;
        string errorDesc = "";
        GlobalRankingResponse gr = null;
        if (_www.error != null)
        {
            errorDesc = _www.error;
        }
        else
        {
            gr = JsonUtility.FromJson<GlobalRankingResponse>(_www.text);
            success = (gr.Status == 0);
            errorDesc = gr.Message;
        }

        if (success == false)
        {

            Debug.Log(errorDesc);
        }
        else
        {
            if (OnGetGlobalRankingFromServerDone != null)
            {

                OnGetGlobalRankingFromServerDone(gr.GlobalRanking);
                OnGetGlobalRankingFromServerDone = null;
            }
        }
    }

    public void GetPersonalRankingFromServer()
    {
        StartCoroutine(GetPersonalRankingFromServerAsync());
    }

    IEnumerator GetPersonalRankingFromServerAsync()
    {
        string url = GameConstants.BASE_URL + GET_PERSONAL_RANKING_URL;
        byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLPersonalRankingRequest(new PersonalRankingRequest(PlayerPrefs.GetString("token"))));

        Dictionary<string, string> _headers = new Dictionary<string, string>();
        _headers["Content-Type"] = "application/json";
        WWW _www = new WWW(url, _raw, _headers);

        yield return _www;

        bool success = false;
        string errorDesc = "";
        PersonalRankingResponse gr = null;
        if (_www.error != null)
        {
            errorDesc = _www.error;
        }
        else
        {
            gr = JsonUtility.FromJson<PersonalRankingResponse>(_www.text);
            success = (gr.Status == 0);
            errorDesc = gr.Message;
        }

        if (success == false)
        {

            Debug.Log(errorDesc);
        }
        else
        {
            if (OnGetPersonalRankingFromServerDone != null)
            {

                OnGetPersonalRankingFromServerDone(gr.PersonalRanking);
                OnGetPersonalRankingFromServerDone = null;
            }
        }
    }

    IEnumerator GetRaceDataFromServerAsync()
    {
        string url = GameConstants.BASE_URL + GET_RACE_DATA_URL;

        byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLRaceDataRequest(new RaceDataRequest(PlayerPrefs.GetString("token"))));

        Dictionary<string, string> _headers = new Dictionary<string, string>();
        _headers["Content-Type"] = "application/json";
        WWW _www = new WWW(url, _raw, _headers);        

        yield return _www;

        bool success = false;
        string errorDesc = "";
        RaceDataResponse gr = null;
        if (_www.error != null)
        {
            errorDesc = _www.error;
        }
        else
        {
            gr = JsonUtility.FromJson<RaceDataResponse>(_www.text);
            success = (gr.Status == 0);
            errorDesc = gr.Message;
        }

        Debug.Log(errorDesc);

        if (OnGetRaceDataResultDone != null)
        {

            OnGetRaceDataResultDone(success == true ? gr.RaceData : null);
            OnGetRaceDataResultDone = null;
        }
    }

    public void Up(string userName) {

    }

    IEnumerator SendActivityResultsToServerAsync() {
        

        if (CLocalAuthentication._guestMode == false)
        {
            bool success = true;
            if (_localScenaryResults.Count > 0)
            {
                //WWWForm _form = new WWWForm();
                byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLSubmitScenaryResultsSend(new ScenaryResultRequest(PlayerPrefs.GetString("token"), _localScenaryResults)));

                //Dictionary<string, string> _headers = _form.headers;
                //_headers["Accept"] = "application/xml";
                //_headers["Content-Type"] = "application/xml";

                Dictionary<string, string> _headers = new Dictionary<string, string>();
                _headers["Content-Type"] = "application/json";

                WWW _www = new WWW(GameConstants.BASE_URL + SUBMIT_SCENARY_RESULTS, _raw, _headers);
                yield return _www;

                success = false;
                int status = -1;
                if (_www.error != null)
                {
                    Debug.Log("" + _www.error);

                }
                else
                {
                    GenericResponse gr = JsonUtility.FromJson<GenericResponse>(_www.text);
                    status = gr.Status;
                    success = (status == 0);
                }
                if (success == true)
                {

                    _localScenaryResults = new List<SScenaryResultFormatted>();
                    SaveLocalScenaryResultsToDisk();
                }
                else
                {

                    Debug.Log(string.Format("XMLSubmitScenaryResultsSend error {0}", status));
                }


                Debug.Log(_www.text);
            }
           
            if (OnSubmitScenaryResultDone != null)
            {

                OnSubmitScenaryResultDone(success);
                OnSubmitScenaryResultDone = null;
            }
        }
    }

   

    
}
