﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CIndicator : MonoBehaviour {
    public Text _indicatorTitle;
    public RectTransform _bar;
    public Image _barBG;

    public Color _titleActive, _titleInactive, _barEmptyActive, _barEmptyInactive;

    public void Configure(string name, int value) {
        _indicatorTitle.text = name;
        if (value >= 0)
            _bar.localScale = new Vector3(value, 1, 1);
        else
            _bar.localScale = Vector3.zero;
    }

    public void Activate()
    {
        _indicatorTitle.color = _titleActive;
        _barBG.color = _barEmptyActive;
    }

    public void Deactivate()
    {
        _indicatorTitle.color = _titleInactive;
        _barBG.color = _barEmptyInactive;
    }


}
