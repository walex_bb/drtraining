using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

public class CStatsManager : MonoBehaviour
{

    public CSceneLoader _sceneLoader;
    string currentType = "Gold";
    public Button _btnTabGold;
    public Button _btnTabSilver;
    public Button _btnTabBronze;

    Color color_enabled = new Color(83 / 255.0f, 94 / 255.0f, 126 / 255.0f, 255 / 255.0f);
    Color color_disabled = new Color(76 / 255.0f, 76 / 255.0f, 76 / 255.0f, 255 / 255.0f);

    public Text[] userNamesTime;
    public Text[] valuesTime;

    public Text[] userNamesDistance;
    public Text[] valuesDistance;

    public Text[] userNamesAccuracy;
    public Text[] valuesAccuracy;

    public GameObject _timeSelection;
    public GameObject _distanceSelection;
    public GameObject _accuracySelection;

    public Text _completedLapsGold;
    public Text _completedLapsSilver;
    public Text _completedLapsBronze;

    public GameObject _goldMedal;
    public GameObject _goldMedalDisabled;

    public GameObject _silverMedal;
    public GameObject _silverMedalDisabled;

    public GameObject _bronzeMedal;
    public GameObject _bronzeMedalDisabled;

    Dictionary<string, Button> buttons;
    Dictionary<string, GameObject> medalsImagesEnabled;
    Dictionary<string, GameObject> medalsImagesDisabled;

    public CLoadingAdvice _loadingAdvice;

    void onConnectionError()
    {
        _loadingAdvice._childAdvice.SetActive(false);
        _loadingAdvice._lostConectionAdvice.SetActive(true);
    }

    void onConnectionStart()
    {
        EnableTabs(false);
        _loadingAdvice._childAdvice.SetActive(true);
        _loadingAdvice._lostConectionAdvice.SetActive(false);
    }

    void onConnectionEnd()
    {       
        _loadingAdvice._childAdvice.SetActive(false);
        _loadingAdvice._lostConectionAdvice.SetActive(false);
        EnableTabs(true);
    }

    void OnSubmitScenaryResultDone(bool result) {        

        if (result == true)
        {            
            StartCoroutine(GetRaceInformationFromServerAsync());
        }
        else
        {
            onConnectionError();
        }
    }

    IEnumerator GetRaceDataFromServerAsync()
    {
        GlobalScenaryData.Inst.GetGlobalRankingFromServer();
        yield return null;
    }

    IEnumerator GetRaceInformationFromServerAsync()
    {
        GlobalScenaryData.Inst.GetRaceInformationFromServer();
        yield return null;
    }

    List<GlobalRanking> glgr = new List<GlobalRanking>();

    void OnGetRaceInformationFromServerDone(RaceInformation ri) {

        if (ri != null)
        {
            _completedLapsGold.text = ri.RaceGoldSquadSize.ToString() + " PLAYERS";
            _completedLapsSilver.text = ri.RaceSilverSquadSize.ToString() + " PLAYERS";
            _completedLapsBronze.text = ri.RaceBronzeSquadSize.ToString() + " PLAYERS";

            StartCoroutine(GetRaceDataFromServerAsync());
        }
        else
        {
            onConnectionError();
        }
    }

    void OnGetGlobalRankingFromServerDone(List<GlobalRanking> lgr)
    {
        glgr = lgr;
        if (glgr != null)
        {            
            StartCoroutine(FillDataAsync(this, glgr));
        }
        else
        {
            onConnectionError();
        }
    }

    void FillData(List<GlobalRanking> lgr)
    {
        foreach (GlobalRanking gr in lgr)
        {
            if (currentType == gr.SquadName)
            {
                FillDataSquad(gr);
                onConnectionEnd();
                return;
            }
        }
    }

    void FillDataSquad(GlobalRanking gr) {

        foreach (Skill sk in gr.Skill)
        {
            FillDataSquadSkill(sk);
        }
    }

    void FillDataSquadSkill(Skill sk) {

        Text[] users;
        Text[] values;
        GameObject selection;

        if (sk.SkillName == "Time")
        {
            users = userNamesTime;
            values = valuesTime;
            selection = _timeSelection;
        }
        else if (sk.SkillName == "Distance")
        {
            users = userNamesDistance;
            values = valuesDistance;
            selection = _distanceSelection;
        }
        else
        {
            users = userNamesAccuracy;
            values = valuesAccuracy;
            selection = _accuracySelection;
        }

        FillDataSquadSkillRanking(users, values, selection, sk.Ranking);
    }

    void FillDataSquadSkillRanking(Text[] users, Text[] values, GameObject selection, List<Ranking> lr)
    {
        foreach (Text tu in users) {

            tu.text = "";
        }

        foreach (Text tv in values)
        {

            tv.text = "";
        }

        foreach (Ranking r in lr)
        {
            int pos = r.position - 1;
            if (pos < lr.Count)
            {
                Text tbu = users[pos];
                Text tbv = values[pos];
                tbu.text = "#" + r.position + ". " + r.UserName;
                tbv.text = r.Score;
                if (r.CurrentUser == true)
                {

                    RectTransform rtdest = selection.GetComponent<RectTransform>();
                    RectTransform rtsrc = tbu.GetComponent<RectTransform>();
                    rtdest.anchoredPosition = new Vector2(rtdest.anchoredPosition.x, rtsrc.anchoredPosition.y);
                    selection.SetActive(true);
                }
            }
        }
    }

    IEnumerator FillDataAsync(CStatsManager instance, List<GlobalRanking> lgr)
    {
        instance.FillData(lgr);
        yield return null;
    }

    void EnableTabs(bool value)
    {
        _btnTabGold.interactable = value;
        _btnTabSilver.interactable = value;
        _btnTabBronze.interactable = value;
    }

    public void Start()
    {
        GlobalScenaryData.OnGetGlobalRankingFromServerDone += OnGetGlobalRankingFromServerDone;
        GlobalScenaryData.OnSubmitScenaryResultDone += OnSubmitScenaryResultDone;
        GlobalScenaryData.OnGetRaceInformationFromServerDone += OnGetRaceInformationFromServerDone;
        
        buttons = new Dictionary<string, Button>();
        buttons.Add("Gold", _btnTabGold);
        buttons.Add("Silver", _btnTabSilver);
        buttons.Add("Bronze", _btnTabBronze);

        medalsImagesEnabled = new Dictionary<string, GameObject>();
        medalsImagesEnabled.Add("Gold", _goldMedal);
        medalsImagesEnabled.Add("Silver", _silverMedal);
        medalsImagesEnabled.Add("Bronze", _bronzeMedal);

        medalsImagesDisabled = new Dictionary<string, GameObject>();
        medalsImagesDisabled.Add("Gold", _goldMedalDisabled);
        medalsImagesDisabled.Add("Silver", _silverMedalDisabled);
        medalsImagesDisabled.Add("Bronze", _bronzeMedalDisabled);        

        onConnectionStart();
        GlobalScenaryData.Inst.SendActivityResultsToServer();
    }
    
    public void onTabclick(string type) {
       
        if (currentType == type) {

            return;
        }
        _timeSelection.SetActive(false);
        _distanceSelection.SetActive(false);
        _accuracySelection.SetActive(false);
        foreach (string key in buttons.Keys) {

            if (type == key)
            {
                ColorBlock cb = buttons[key].colors;
                cb.highlightedColor = cb.pressedColor = cb.normalColor = color_enabled;
                buttons[key].colors = cb;                
                medalsImagesDisabled[key].SetActive(false);
                medalsImagesEnabled[key].SetActive(true);
            }
            else
            {
                ColorBlock cb = buttons[key].colors;
                cb.highlightedColor = cb.pressedColor = cb.normalColor = color_disabled;
                buttons[key].colors = cb;
                medalsImagesEnabled[key].SetActive(false);
                medalsImagesDisabled[key].SetActive(true);
            }
            
        }
        
        currentType = type;
        FillData(glgr);   
    }
}