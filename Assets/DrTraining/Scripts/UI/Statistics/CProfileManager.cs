using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CProfileManager : MonoBehaviour
{

    public CSceneLoader _sceneLoader;
    public InputField _firstNameInput;
    public Text _textSquad;

    public Text _textValueTime;
    public Text _textPositionTime;

    public Text _textValueDistance;
    public Text _textPositionDistance;

    public Text _textValueAccuracy;
    public Text _textPositionAccuracy;

    public Text _textLaps;

    RaceData raceData = null;
    PersonalRanking personalRanking = null;

    public CLoadingAdvice _loadingAdvice;

    void onConnectionError()
    {
        _loadingAdvice._childAdvice.SetActive(false);
        _loadingAdvice._lostConectionAdvice.SetActive(true);
    }

    void onConnectionStart()
    {
        _loadingAdvice._childAdvice.SetActive(true);
        _loadingAdvice._lostConectionAdvice.SetActive(false);
    }

    void onConnectionEnd()
    {
        _loadingAdvice._childAdvice.SetActive(false);
        _loadingAdvice._lostConectionAdvice.SetActive(false);
    }

    void OnSubmitScenaryResultDone(bool result) {        
        
        if (result == true)
        {
            StartCoroutine(GetRaceDataFromServerAsync());
        }
        else
        {
            onConnectionError();
        }
    }

    IEnumerator GetRaceDataFromServerAsync()
    {
        GlobalScenaryData.Inst.GetRaceDataFromServer();
        yield return null;
    }

    IEnumerator GetPersonalRankingFromServerAsync()
    {
        GlobalScenaryData.Inst.GetPersonalRankingFromServer();
        yield return null;
    }
    void OnGetPersonalRankingFromServerDone(PersonalRanking pr) {

        personalRanking = pr;

        if (personalRanking == null)
        {
            onConnectionError();
        }
        else
        {
            FillDataRaceData();
            onConnectionEnd();
        }
        
    }

    void OnGetRaceDataResultDone(RaceData rd) {

        raceData = rd;

        if (raceData == null)
        {
            onConnectionError();
        }
        else
        {
            StartCoroutine(GetPersonalRankingFromServerAsync());
        }
       
    }

    void OnSubmitUserDataResultDone(bool result)
    {        
    }

    void FillDataRaceData()
    {
        _textSquad.text = personalRanking.SquadName;
        _textLaps.text = string.Format("\t\t{0} LAPS COMPLETED\n{1} LAPS TO UPGRADE SQUAD", raceData.UserCompletedLaps, raceData.LapsToNextSquad);
        foreach (SKillRanking sr in personalRanking.SKillRanking) {

            string pos = sr.Position.ToString();
            string value = sr.Score.ToString();

            if (sr.SKillName == "Time")
            {
                _textPositionTime.text = pos;
                _textValueTime.text = value;
            }
            else if (sr.SKillName == "Distance")
            {
                _textPositionDistance.text = pos;
                _textValueDistance.text = value;
            }
            else
            {
                _textPositionAccuracy.text = pos;
                _textValueAccuracy.text = value;
            }
        }        
    }    

    public void Start()
    {
        _firstNameInput.text = CLocalPersist._currentUser._firstName;

        GlobalScenaryData.OnSubmitScenaryResultDone += OnSubmitScenaryResultDone;
        GlobalScenaryData.OnGetRaceDataResultDone += OnGetRaceDataResultDone;
        GlobalScenaryData.OnGetPersonalRankingFromServerDone += OnGetPersonalRankingFromServerDone;
        GlobalScenaryData.OnSubmitUserDataResultDone += OnSubmitUserDataResultDone;

        onConnectionStart();
        GlobalScenaryData.Inst.SendActivityResultsToServer();        
    }


    public void onModifyUserDataClick() {

        GlobalScenaryData.Inst.SubmitUserDataResult(_firstNameInput.text.Trim());
    }
}