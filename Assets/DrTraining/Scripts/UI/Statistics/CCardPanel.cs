﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum EActivityCode { 
    NOONE,
    TOUCH_AND_PROBE_STATIONARY,
    TOUCH_AND_PROBE_MOVING,
    IMAGE_CENTERING_1,
    IMAGE_CENTERING_2,
    TRACKING,
    IMAGE_ORIENTATION,
	HORIZONT_CONTROL,    
    PERISCOPING
}

public class CCardPanel : MonoBehaviour {
    public EActivityCode _code;
    private const string _hardcodedSceneName = "AllGamesPanel";
    public Transform _indicatorsContainer;
    public List<SIndicator> _indicatorsData;
    public GameObject _indicatorPrefab;
    private GameObject[] _cachedIndicators;
    public Text _SceneNameText;
    public bool _removeOkBtn;
    public GameObject _okBtn;
    public bool _wasActivityPlayed = true;
    public GameObject _greenTickImg;
    bool _wasCacheGenerated = false;



    public void GenerateCached() {
        if (_wasCacheGenerated)
            return;
        _greenTickImg.SetActive(_wasActivityPlayed);
        
        if (_removeOkBtn)
            _okBtn.SetActive(false);
        if (_cachedIndicators != null)
        {
            for (int i = 0; i < _cachedIndicators.Length; i++)
                Destroy(_cachedIndicators[i]);
            _cachedIndicators = null;
        }
        
        _cachedIndicators = new GameObject[_indicatorsData.Count];
        for (int i = 0; i < _indicatorsData.Count; i++)
        {

            GameObject _newIndicatorGO = Instantiate(_indicatorPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            _newIndicatorGO.transform.SetParent(_indicatorsContainer, false);
            _cachedIndicators[i] = _newIndicatorGO;

            SIndicator _data = _indicatorsData[i];
            _newIndicatorGO.GetComponent<CIndicator>().Configure(_data._name, _data._value);
            _newIndicatorGO.GetComponent<CIndicator>().Deactivate();
        }
        _wasCacheGenerated = true;
    }

    // Use this for initialization
    void Start () {
        GenerateCached(); 
    }

    void OnEnable()
    {
        //CUITimeCounter.Inst.ChangePlayingState(false);
    }

    public void Reset() {
        _wasCacheGenerated = false;
        _wasActivityPlayed = false;
        _greenTickImg.SetActive(true);
        for (int i = 0; i < _indicatorsData.Count; i++) {
            if (_indicatorsData[i]._value != -1)
            {
                _indicatorsData[i]._value = 0;
                SetIndicator(_indicatorsData[i]._code, _indicatorsData[i]._value, true);
            }
        }
    }

    public void SetSceneName(string _sceneName)
    {
        _SceneNameText.text = _sceneName;
    }

 
    public void SetIndicator(string _indicatorCode, int _steadinessNormalized, bool _onReset = false){
        _greenTickImg.SetActive(_wasActivityPlayed);
        if (!_onReset && !_wasActivityPlayed)
            return;
        if (!_onReset)
            GenerateCached();
        //Debug.Log("Seteando Indicador: " + _indicatorCode);
        CIndicator _indicator = _cachedIndicators[_indicatorsData.FindIndex(a => a._code == _indicatorCode)].GetComponent<CIndicator>();
        _indicator.Configure(_indicatorsData.Find(a => a._code == _indicatorCode)._name, _steadinessNormalized);
        _indicator.Activate();
    }

    public void OnOKButton() {
        DRDelegates.OnLoadLevelMsg(_hardcodedSceneName);
    }

    // Update is called once per frame
    void Update () {
    
    }
}

[System.Serializable]
public class SIndicator {
    public string _name;
    public string _code;
    [Range(-1, 100)]
    public int _value;
}