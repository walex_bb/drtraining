﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.DrTraining.Scripts.UI
{
    class CSimulatePanel : MonoBehaviour
    {
        public Image _simulateLocked;
        public Image _simulateUnlocked;

        void EnableSimulationImage()
        {
            bool enable = CLocalPersist.GetUserLevel() >= 0;
            if (_simulateLocked != null)
                _simulateLocked.enabled = !enable;
            if (_simulateUnlocked != null)
                _simulateUnlocked.enabled = enable;
        }

        void OnDisable()
        {            
        }

        void OnEnable()
        {
            EnableSimulationImage();
        }
    }
}
