﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class CCloseGamePopup : MonoBehaviour {
    public CSceneLoader _sceneLoader;
   
    public void ExternalOpenPopup() {

        gameObject.SetActive(true);
    }

    public void GoToHome() {

        _sceneLoader.LoadScene("Home");
    }

    public void Yes() {

        //CLocalAuthentication.EnableSimulationImage();

        string name;
        Scene sc = SceneManager.GetActiveScene();
        if (sc.name == "P0X_CalibracionScene")
        {
            name = "Home";
        }
        else
        {
            name = "AllGamesPanel";
        }
        _sceneLoader.LoadScene(name);
    }

    public void No() {

        //CLocalAuthentication.EnableSimulationImage();

        gameObject.SetActive(false);
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
