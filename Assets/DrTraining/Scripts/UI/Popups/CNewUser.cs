﻿using UnityEngine;
using System.Collections;

public class CNewUser : MonoBehaviour
{

    public void ExternalOpenPopup()
    {
        gameObject.SetActive(true);
    }

    public void Yes()
    {
        CLoadSaveData.Inst.NewGame();
        CLoadSceneManager.Inst.ChangeScene("Home");
    }

    public void No()
    {
        gameObject.SetActive(false);
    }

}
