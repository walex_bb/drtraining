﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MP;
using MP.AVI;

public class CInitialPopup : MonoBehaviour {
    public int _activityId = -1;
    public Text _name;
    public Text _description;
    private bool _isFirstActivation = true;
    public MoviePlayer _mp;
    public GameObject _playButton;
    public TextAsset _videoToPlay;

    public void ExternalOpenPopup() {
        gameObject.SetActive(true);
    }

    public void InitialPopupOK() {
        gameObject.GetComponentInChildren<CPopupScalator>().PanelOut(() => {
            if (_isFirstActivation)
            {
                _isFirstActivation = false;
                CDelegates.OnStartActivity();
            }
            gameObject.SetActive(false);
        });   
    }

    void OnEnable() {        
        _mp.source = _videoToPlay;        
        SScenaryData _data = GlobalScenaryData.Inst._scenariesData.Find(a => a.Id == _activityId);
        if (_data != null) {
            _name.text = _data.Name;
            _description.text = _data.Description;
        }
    }

    void Update() {
        if (!_mp.play) {
            _playButton.SetActive(true);
        }
    }

    public void ReplayVideo() {
        _mp.videoFrame = 0;
        _mp.play = true;
        _playButton.SetActive(false);  
    }
}
