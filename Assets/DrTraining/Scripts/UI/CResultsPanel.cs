using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class CResultsPanel : MonoBehaviour
{
    public CSceneLoader _sceneLoader;
    public Text _distance;
    public Text _time;
    public Text _accuracy;
    public Text _exerciceName;

    public void SetExerciceName(string name)
    {
        _exerciceName.text = name.Trim();
    }

    public void SetDistance(float distance)
    {
        _distance.text = distance.ToString("0.##") + " mm";
    }

    public void SetTime(float agility)
    {
        _time.text = agility.ToString("0.##") + "''";
    }

    public void SetAccuracy(float steadiness)
    {
        _accuracy.text = steadiness.ToString("0.##") + " points";
    }

    public void BackToAllGames() {

        SceneManager.UnloadScene(SceneManager.GetActiveScene().name);
        SceneManager.UnloadScene("AllGamesPanel");
        _sceneLoader.LoadScene("AllGamesPanel");
    }
}