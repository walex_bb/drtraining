﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

// AUXILIAR CLASS THAT CONTAINS ALL ANIMATIONS PLAYED BY CODE IN THE APP
public class CCodeAnimations : MonoBehaviour {
	//Displace a panel using linear lerp movement
	public static IEnumerator LerpDisplacementPanels(Action _callback, RectTransform _panelToDisplace, Vector2 _origin, Vector2 _destination, float _transitionDuration, bool _useAnchorPosition = true){
		float _accumulatedTime = 0;

		while (_accumulatedTime < _transitionDuration) {
			if (_useAnchorPosition) {
				_panelToDisplace.anchoredPosition = Vector2.Lerp (
					_origin, 
					_destination, 
					1 - (_transitionDuration - _accumulatedTime) / _transitionDuration
				);
			} else {
				_panelToDisplace.position = Vector2.Lerp (
					_origin, 
					_destination, 
					1 - (_transitionDuration - _accumulatedTime) / _transitionDuration
				);
			}
			yield return new WaitForEndOfFrame ();
			_accumulatedTime += Time.deltaTime;
		}
		if (_useAnchorPosition) {
			_panelToDisplace.anchoredPosition = _destination;
		} else {
			_panelToDisplace.position = _destination;
		}
		_callback ();
	}

	public static IEnumerator LerpScale(Action _callback, RectTransform _elementoToReescalate, Vector2 _originScale, Vector2 _targetScale, float _transitionDuration, float _delay = 0){
        yield return new WaitForSeconds(_delay);
		float _accumulatedTime = 0;
		while (_accumulatedTime < _transitionDuration) {
			_elementoToReescalate.localScale = Vector2.Lerp (_originScale, _targetScale, 1 - (_transitionDuration - _accumulatedTime) / _transitionDuration);
			yield return new WaitForEndOfFrame ();
			_accumulatedTime += Time.deltaTime;
		}
		_elementoToReescalate.localScale = _targetScale;
		_callback ();
	}

	public static IEnumerator LerpAlphaColor(Action _callback, Image _panelToDisplace, Color _origin, Color _destination, float _transitionDuration, float _delayTime = 0)
	{
		yield return new WaitForSeconds(_delayTime);
		float _accumulatedTime = 0;

		while (_accumulatedTime < _transitionDuration)
		{
			_panelToDisplace.color = Color.Lerp(
				_origin,
				_destination,
				1 - (_transitionDuration - _accumulatedTime) / _transitionDuration
			);
			yield return new WaitForEndOfFrame();
			_accumulatedTime += Time.deltaTime;
		}
		_panelToDisplace.color = _destination;
		_callback();
	}

	public static IEnumerator AugmentSize(Action _callback, RectTransform _panel, Vector2 _origin, Vector2 _destination, float _transitionDuration)
	{
		float _accumulatedTime = 0;

		while (_accumulatedTime < _transitionDuration)
		{
			_panel.sizeDelta = Vector2.Lerp(
				_origin,
				_destination,
				1 - (_transitionDuration - _accumulatedTime) / _transitionDuration
			);
			yield return new WaitForEndOfFrame();
			_accumulatedTime += Time.deltaTime;
		}
		_panel.sizeDelta = _destination;
		_callback();
	}

	public static IEnumerator ChangePreferredSize(Action _callback, LayoutElement _panel, Vector2 _origin, Vector2 _destination, float _transitionDuration)
	{
		float _accumulatedTime = 0;

		while (_accumulatedTime < _transitionDuration)
		{
			_panel.preferredWidth = Mathf.Lerp(
				_origin.x,
				_destination.x,
				1 - (_transitionDuration - _accumulatedTime) / _transitionDuration
			);
			_panel.preferredHeight = Mathf.Lerp(
				_origin.y,
				_destination.y,
				1 - (_transitionDuration - _accumulatedTime) / _transitionDuration
			);
			yield return new WaitForEndOfFrame();
			_accumulatedTime += Time.deltaTime;
		}
		_panel.preferredWidth = _destination.x;
		_panel.preferredHeight = _destination.y;
		_callback();
	}





    public static IEnumerator Change3DSize(Action _callback, Transform _elementoToReescalate, Vector3 _originScale, Vector3 _targetScale, float _transitionDuration)
    {
        float _accumulatedTime = 0;
        while (_accumulatedTime < _transitionDuration)
        {
            _elementoToReescalate.localScale = Vector3.Lerp(_originScale, _targetScale, 1 - (_transitionDuration - _accumulatedTime) / _transitionDuration);
            yield return new WaitForEndOfFrame();
            _accumulatedTime += Time.deltaTime;
        }
        _elementoToReescalate.localScale = _targetScale;
        _callback();
    }


    public static IEnumerator Change3DPosition(Action _callback, Transform _elementoToMove, Vector3 _origin, Vector3 _destiny, float _transitionDuration)
    {
        float _accumulatedTime = 0;
        while (_accumulatedTime < _transitionDuration)
        {
            _elementoToMove.localPosition = Vector3.Lerp(_origin, _destiny, 1 - (_transitionDuration - _accumulatedTime) / _transitionDuration);
            yield return new WaitForEndOfFrame();
            _accumulatedTime += Time.deltaTime;
        }
        _elementoToMove.localPosition = _destiny;
        _callback();
    }

    public static IEnumerator ChangeColor(Action _callback, Material _elementoToMove, Color _origin, Color _destiny, float _transitionDuration)
    {
        float _accumulatedTime = 0;
        while (_accumulatedTime < _transitionDuration)
        {
            _elementoToMove.color = Color.Lerp(_origin, _destiny, 1 - (_transitionDuration - _accumulatedTime) / _transitionDuration);
            yield return new WaitForEndOfFrame();
            _accumulatedTime += Time.deltaTime;
        }
        _elementoToMove.color = _destiny;
        _callback();
    }

}                         