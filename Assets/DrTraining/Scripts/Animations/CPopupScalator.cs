﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class CPopupScalator : MonoBehaviour {
    public RectTransform _panel;
    public float _transitionTimeIn = .35f;
    public float _transitionTimeOut = .20f;

    void OnEnable() { 
        StartCoroutine(CCodeAnimations.LerpScale(() => {}, _panel, new Vector2(0, 1), Vector2.one, _transitionTimeIn));
    }

    public void PanelOut(Action _callback) {
        StartCoroutine(CCodeAnimations.LerpScale(() => {
            _callback();
        }, _panel, Vector2.one, new Vector2(0, 1), _transitionTimeOut));
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
