﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CAllGamesPanelAnimations : MonoBehaviour {
    public float _timeBetweenPanelsSpiral;
    public Image [] _spiralSubPanels;
    Color _spiralSubPanelsColorOrigin;
    public Color _spiralSubPanelsColorDest;
    public CLoadingAdvice _loadingAdvice;

    void OnSubmitScenaryResultDone(bool result)
    {
        
        if (result == true) 
        {
            onConnectionEnd();
            StartCoroutine(SpiralAnimationIn());
        }
        else
        {
            onConnectionError();
        }
    }

   
    void onConnectionError()
    {
        _loadingAdvice._childAdvice.SetActive(false);
        _loadingAdvice._lostConectionAdvice.SetActive(true);
    }

    void onConnectionStart()
    {
        _loadingAdvice._childAdvice.SetActive(true);
        _loadingAdvice._lostConectionAdvice.SetActive(false);
    }

    void onConnectionEnd()
    {
        _loadingAdvice._childAdvice.SetActive(false);
        _loadingAdvice._lostConectionAdvice.SetActive(false);
    }

    // Use this for initialization
    void Start () {
        _spiralSubPanelsColorOrigin = new Color(_spiralSubPanels[0].color.r, _spiralSubPanels[0].color.g, _spiralSubPanels[0].color.b, 1);

        GlobalScenaryData.OnSubmitScenaryResultDone += OnSubmitScenaryResultDone;
        onConnectionStart();
        GlobalScenaryData.Inst.SendActivityResultsToServer();

    }

    IEnumerator SpiralAnimationIn() {
        for (int i = 0; i < _spiralSubPanels.Length; i++)
            _spiralSubPanels[i].color = _spiralSubPanelsColorOrigin;
        yield return new WaitForSeconds(1.6f);
        yield return StartCoroutine(CCodeAnimations.LerpAlphaColor(() =>{
            StartCoroutine(CCodeAnimations.LerpAlphaColor(() =>
            {
                StartCoroutine(CCodeAnimations.LerpAlphaColor(() =>
                {
                    StartCoroutine(CCodeAnimations.LerpAlphaColor(() =>
                    {
                        StartCoroutine(CCodeAnimations.LerpAlphaColor(() =>
                        {
                            StartCoroutine(CCodeAnimations.LerpAlphaColor(() =>
                            {
                                StartCoroutine(CCodeAnimations.LerpAlphaColor(() =>
                                {
                                    StartCoroutine(CCodeAnimations.LerpAlphaColor(() =>
                                    {
                                        StartCoroutine(CCodeAnimations.LerpAlphaColor(() =>
                                        {

                                        }, _spiralSubPanels[8], _spiralSubPanelsColorOrigin, _spiralSubPanelsColorDest, _timeBetweenPanelsSpiral));
                                    }, _spiralSubPanels[7], _spiralSubPanelsColorOrigin, _spiralSubPanelsColorDest, _timeBetweenPanelsSpiral));
                                }, _spiralSubPanels[6], _spiralSubPanelsColorOrigin, _spiralSubPanelsColorDest, _timeBetweenPanelsSpiral));
                            }, _spiralSubPanels[5], _spiralSubPanelsColorOrigin, _spiralSubPanelsColorDest, _timeBetweenPanelsSpiral));
                        }, _spiralSubPanels[4], _spiralSubPanelsColorOrigin, _spiralSubPanelsColorDest, _timeBetweenPanelsSpiral));
                    }, _spiralSubPanels[3], _spiralSubPanelsColorOrigin, _spiralSubPanelsColorDest, _timeBetweenPanelsSpiral));
                }, _spiralSubPanels[2], _spiralSubPanelsColorOrigin, _spiralSubPanelsColorDest, _timeBetweenPanelsSpiral));
            }, _spiralSubPanels[1], _spiralSubPanelsColorOrigin, _spiralSubPanelsColorDest, _timeBetweenPanelsSpiral));

        }, _spiralSubPanels[0], _spiralSubPanelsColorOrigin, _spiralSubPanelsColorDest, _timeBetweenPanelsSpiral));
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
