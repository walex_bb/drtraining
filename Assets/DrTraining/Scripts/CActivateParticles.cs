﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CActivateParticles : MonoBehaviour {

    [SerializeField]
    List<GameObject> _particles;

    [SerializeField]
    GameObject _particlePos;


    public void PlayParticle(int pId)
    {
        _particles[pId].SetActive(true);
        _particles[pId].transform.position = _particlePos.transform.position;
        CAudioManager.Inst.PlaySFX("Destroy");
    }
}
