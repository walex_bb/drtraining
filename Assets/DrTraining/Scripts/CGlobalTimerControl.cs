﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CGlobalTimerControl : MonoBehaviour {


    public CUITimeCounter _TimerGlobal;
    public Text _timeText;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void UpdateResult()
    {
        _timeText.text = _TimerGlobal.Timer;
    }
}
