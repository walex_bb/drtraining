﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class CSkillsStatisticsResume : MonoBehaviour {

    public RectTransform[] _barras;
    public static CSkillsStatisticsResume Inst;
    public GameObject _habilidadesPanel, _loadingPanel;
    public Text[] _skillsName;
    public Dictionary<int, float> _barValues;

    void Awake()
    {
        Inst = this;
    }

    void Start()
    {
        //_barValues = new Dictionary<int, float>();
        _habilidadesPanel.SetActive(false);
        _loadingPanel.SetActive(true);
    }

    public void TurnOnPanel(CLocalAuthentication _controller) {
        _habilidadesPanel.SetActive(true);
        _loadingPanel.SetActive(false);
        for (int i = 0; i < _barras.Length; i++)
            Reset(i);
        _controller.OverallAnimationIn(() => {
            for (int i = 0; i < _barras.Length; i++)
                Configure(i);
        });   
    }

    public void ActualizarPanel(List<int> _valores)
    {
        Inst._barValues = new Dictionary<int, float>();
        for (int _i = 0; _i < _skillsName.Length; _i++)
        {

            if (_barValues.ContainsKey(_i))
                _barValues[_i] = _valores[_i + 1];
            else
                _barValues.Add(_i, _valores[_i + 1]);

            //Configure(_barras[_i], _valores[_i+1]);
            string _skillName = GlobalScenaryData.Inst._skillsData.Find(a => a.Id == _i+1).Name;
            _skillsName[_i].text = _skillName;
        }
    }

    //barra _i, _index _i + 1
    public void Configure(int _index)
    {
        float _value = Inst._barValues[_index];
        if (_value > 0)
        {
            StartCoroutine(CCodeAnimations.LerpScale(() => { }, _barras[_index], _barras[_index].localScale, new Vector2(Mathf.Clamp(_value, 0, 100), 1), 1.0f));
        }
        else
            _barras[_index].localScale = new Vector2(0, 1);
    }

    public void Reset(int _index)
    {
        _barras[_index].localScale =new Vector2(0,1);
    }
}
