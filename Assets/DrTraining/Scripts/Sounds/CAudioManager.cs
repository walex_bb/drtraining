﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;

public class CAudioManager : MonoBehaviour
{

    public static CAudioManager Inst;
    public AudioSerial[] _musicList;
    public AudioSerial[] _sfxList;
    private List<AudioSource> _SFXsources;      // lista de TODOS los efectos del juego
    private AudioSource _musicSource;
    
    public AudioMixerGroup _musicMixer;
    public AudioMixerGroup _fxMixer;
    public AudioMixer _masterMixer;

    private bool _music, _fx;

    [SerializeField] // ¿Poner musica ambiente al empezar?
    bool _playSongOfAwake;

    void Awake()
    {
        _music = true;
        _fx = true;
        Inst = this;
        _SFXsources = new List<AudioSource>();
    }

    void Start()
    {
        if (_playSongOfAwake)
        {
            PlayMusic("Ambiente");
        }        
    }
 
    private void MakeMusicSource()          // crea un audiosource y le pone musica
    {
        GameObject newObj = new GameObject("Music Source");
        _musicSource = newObj.AddComponent<AudioSource>();
        _musicSource.GetComponent<AudioSource>().outputAudioMixerGroup = _musicMixer;
    }

    private AudioClip GetMusicClip(string hash)
    {
        for (int i = 0; i<_musicList.Length; i++)
        {
            if (_musicList [i].hash == hash)
                return _musicList [i].clip;
        }
        return null;
    }

    private AudioClip GetSFXClip(string hash)
    {
        for (int i = 0; i<_sfxList.Length; i++)
        {
            if (_sfxList [i].hash == hash)
                return _sfxList [i].clip;
        }
        return null;
    }

    public void PlayMusic(string hash, bool loops = true, Vector3 pos = default(Vector3))
    {
        if (_musicSource == null)
            MakeMusicSource();

        _musicSource.clip = GetMusicClip(hash);
        _musicSource.loop = loops;
        _musicSource.transform.position = pos;
        _musicSource.Play();
    }

    public void StopMusic()
    {
        _musicSource.Stop();
    }

    public void PlaySFX(string hash, bool loops = false, Vector3 pos = default(Vector3))
    {
        GameObject newObj = new GameObject(hash);
        AudioSource source = newObj.AddComponent<AudioSource>();
        source.GetComponent<AudioSource>().outputAudioMixerGroup = _fxMixer;
        source.clip = GetSFXClip(hash);
        source.loop = loops;
        source.transform.position = pos;
        source.Play();
        _SFXsources.Add(source);

        if (!loops && source != null)
            Destroy(source.gameObject, source.clip.length);
    }

    public void PlaySFXSimple(string hash)
    {
        PlaySFX(hash, false);
    }

    public void StopSFX(string hash)
    {
        for(int i =0; i < _SFXsources.Count; i++)
        {
            if(_SFXsources[i].name == hash)
            {
                _SFXsources[i].Stop();
                Destroy(_SFXsources[i].gameObject);
                break;
            }
        }
    }

    public void StopAllSFX()
    {
        for (int i = 0; i < _SFXsources.Count; i++)
        {
             Destroy(_SFXsources[i].gameObject);
        }
    }

    public void MusicControl() // Mute music
    {
        Inst.PlaySFX("Button2");
        if (_music == true)
        {
            _music = false;
            _masterMixer.SetFloat("musicVol", -80f);
            //_musicButton.image.sprite = _musicSprites[2];
            //SpriteState st = new SpriteState();
            //st.pressedSprite = _musicSprites[3];
            //_musicButton.spriteState = st;
        }
        else
        {
            _music = true;
            _masterMixer.SetFloat("musicVol", -20f);
            //_musicButton.image.sprite = _musicSprites[0];
            //SpriteState st = new SpriteState();
            //st.pressedSprite = _musicSprites[1];
            //_musicButton.spriteState = st;
        }
    }

    public void SfxControl() // Mute Sfx
    {
        Inst.PlaySFX("Button2");
        if (_fx == true)
        {
            _fx = false;
            _masterMixer.SetFloat("fxVol", -80f);
            //_sfxButton.image.sprite = _sfxSprites[2];
            //SpriteState st = new SpriteState();
            //st.pressedSprite = _sfxSprites[3];
            //_sfxButton.spriteState = st;
        }
        else
        {
            _fx = true;
            _masterMixer.SetFloat("fxVol", -13f);
            //_sfxButton.image.sprite = _sfxSprites[0];
            //SpriteState st = new SpriteState();
            //st.pressedSprite = _sfxSprites[1];
            //_sfxButton.spriteState = st;
        }
    }
}

[System.Serializable]
public class AudioSerial
{
    public string hash;
    public AudioClip clip;
}