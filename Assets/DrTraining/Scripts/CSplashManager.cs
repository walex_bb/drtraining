﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CSplashManager : MonoBehaviour {

    public float _waitTime = .1f;
    public string _welcome;

	// Use this for initialization
	void Start () {

        
        // force logout
        PlayerPrefs.DeleteKey("email");
        PlayerPrefs.DeleteKey("token");
        GlobalScenaryData.Inst.DeleteGuestData();
        CLocalPersist.DeleteGuestLevelData();
        GlobalScenaryData.Inst.ClearCurrentPlayerData();
        

        Invoke("LoadNextScene", _waitTime);
        
	}

    void LoadNextScene() {
        SceneManager.LoadScene(_welcome);
    }

}
