﻿using UnityEngine;
using System.Collections;

public class SteadinessManager : MonoBehaviour {

    public static SteadinessManager Inst;
    public Transform _objeto1, _objeto2;
    public float _dist;

    private bool _activated;
    public float _steadinessEstimator;
    private float _media, _potencia;
    private int _n;
    public float _sMin, _sMax;

    void Awake()
    {
        Inst = this;
    }

    public void ResumeSteadiness()
    {
        _activated = true;
    }

    public void PauseSteadiness()
    {
        _activated = false;
    }

    public void ResetSteadiness()
    {
        _steadinessEstimator = 0;
        _n = 0;
        _media = 0;
        _potencia = 0;
    }

    public void StartSteadiness()
    {
        ResetSteadiness();
        ResumeSteadiness();
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (_activated)
        {
            ProcesarMuestra();
        }      

    }

    private void ProcesarMuestra()
    {
        _n = _n + 1;
        _dist = Vector3.Distance(_objeto1.position, _objeto2.position);
        _media = (_dist + (_n - 1) * _media) / _n;
        _potencia = (_dist * _dist + (_n - 1) * _potencia) / _n;
        _steadinessEstimator = _potencia - _media * _media;

    }


    public int EstimatorSteadinessNormalized()
    {
        float _steadiness = 0;
        //if (_steadinessEstimator < _sMin)
        //{
        //    _steadiness = 1;
        //}
        //else if (_steadinessEstimator > _sMax)
        //{
        //    _steadiness = 0;
        //}
        //else
        //{
        //    _steadiness = -(_steadinessEstimator - _sMin) / (_sMax - _sMin) + 1;
        //}

        float vel = 1 / (_sMax - _sMin);
        float x0 = (_sMin + _sMax) / 2;
        _steadiness = Sigmoide(-vel * (_steadinessEstimator - x0));
        _steadiness = _steadiness / Sigmoide(-vel * (_sMin - x0));
        if (_steadiness > 1) _steadiness = 1;
        if (_steadiness < 0) _steadiness = 0;

        return (int)(_steadiness * 100);
    }


    private float Sigmoide(float x)
    {
        float y;
        y = 1 / (1 + Mathf.Exp(-x));

        return y;

    }


}
