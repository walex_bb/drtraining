﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CTelescoping : MonoBehaviour {

    public Slider _saliderTelescoping;
    public Transform _instrumentCamera;
    private bool _TelescopingActivated;
    private float _xRot, _yRot, _zRot;
    public float _grados = 30f;
    public Vector3 _angleCorrection;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (_TelescopingActivated)
        {
            _xRot = -90f + _grados;
            _yRot = -_saliderTelescoping.value * 360f;
            _zRot = 90f + _saliderTelescoping.value * 360f;
            _instrumentCamera.localEulerAngles = new Vector3(_xRot, _yRot, _zRot) - _angleCorrection;
        }
	}


    public void ActivarTelescoping()
    {
        _instrumentCamera.localEulerAngles = new Vector3(270, 0, 0) - _angleCorrection;
        _saliderTelescoping.value = 0f;
        _TelescopingActivated = true;
    }

    public void DesactivarTelescoping()
    {
        _instrumentCamera.localEulerAngles = new Vector3(-90f, 0f, 90f) - _angleCorrection;
        _TelescopingActivated = false;
    }



}
