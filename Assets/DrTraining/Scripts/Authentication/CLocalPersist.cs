﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class SActivityData { 
	public EActivityCode _code;
	public string _date;
    public int _agility, _linearNavigation, _angularNavigation, _steadiness, _tracking, _centering, _horizonControl;

	public SActivityData(EActivityCode code, string date,
		int agility, int linearNavigation, int angularNavigation, int steadiness, int tracking, int centering, int horizonControl) {
			_code = code;
			_date = date;
            _agility = agility;
            _linearNavigation = linearNavigation;
            _angularNavigation = angularNavigation;
            _steadiness = steadiness;
            _tracking = tracking;
            _centering = centering;
            _horizonControl = horizonControl;
	}

	public float CalculateScore() {
		int _result = 0;
		List<int> _fields = new List<int>();
		_fields.Add(_agility);
		_fields.Add(_linearNavigation);
		_fields.Add(_angularNavigation);
		_fields.Add(_steadiness);
		_fields.Add(_tracking);
		_fields.Add(_centering);
        _fields.Add(_horizonControl);

		int j = 0;
		while(j < _fields.Count) {
			if (_fields [j] < 0) {
				_fields.RemoveAt (j);
			}
			else
				j++;
		}

		if (_fields.Count > 0)
		{

			for (int i = 0; i < _fields.Count; i++)
			{
				_result += _fields[i];
			}

			_result /= _fields.Count;
		}
		return _result;
	}
}

[System.Serializable]
public class SUserLevel {
    public int _lastLevel = -1;
    public SUserLevel(int userLevel) {
       _lastLevel = userLevel;
    }
}

public static class CLocalPersist {
	public static SUserData _currentUser;
    public static SUserLevel _userLevel;
	public static Dictionary<EActivityCode, SActivityData> _bestScoreActivities;
    public static List<int> _bestSkillResults;

	public static void UpdateUserLevel(int _level) {
        string _key = (PlayerPrefs.GetString("email") != "" ? PlayerPrefs.GetString("email") : "GUESTDATA");
        Debug.Log(_key);
        LoadUserLevelData(_key);
		Debug.Log(_level);
       // if (_level > _userLevel._lastLevel)
		{
            _userLevel._lastLevel = _level;
            SaveUserLevelData(_key);
		}
	}

    public static int GetUserLevel()
    {
        string _email = (PlayerPrefs.GetString("email") != "" ? PlayerPrefs.GetString("email") : "GUESTDATA");
        //Load by system, no password needed
        string _path = GetActivityPath(_email);

        BinaryFormatter _bf = new BinaryFormatter();
        if (File.Exists(_path))
        {
            FileStream _file = File.Open(_path, FileMode.Open);
            _userLevel = (SUserLevel)_bf.Deserialize(_file);
            _file.Close();
        }
        else
        {
            _userLevel = new SUserLevel(-1);
            SaveUserLevelData(_email);
        }

        return _userLevel._lastLevel;
    }

    public static void LoadUserLevelData(string _email)
	{
		//Load by system, no password needed
		string _path = GetActivityPath(_email);

        BinaryFormatter _bf = new BinaryFormatter();
        if (File.Exists(_path))
        {
            FileStream _file = File.Open(_path, FileMode.Open);
            _userLevel = (SUserLevel)_bf.Deserialize(_file);
            _file.Close();
        }
        else {
            _userLevel = new SUserLevel(-1);
            SaveUserLevelData(_email);
        }
	}

    public static void DeleteGuestLevelData() {
        string _path = GetActivityPath("GUESTDATA");
        if (File.Exists(_path)) {
            File.Delete(_path);
        }
    }

    public static void SaveUserLevelData(string _email) {
        string _path = GetActivityPath(_email);

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(_path);
        bf.Serialize(file, _userLevel);
        file.Close();
    }

	public static void SaveActivityGameData(string _userEmail, 
		EActivityCode _code, string _date,
		int _steadiness, int _controlOfHorizont, int _telescoping, int _periscoping, int _tracking, int _agility
		)
	{
		string _path = GetActivityPath(_userEmail);
		FileStream _fs;
		if (File.Exists(_path))
		{
			_fs = new FileStream(_path, FileMode.Append, FileAccess.Write);
		}
		else {
			_fs = new FileStream(_path, FileMode.Create, FileAccess.Write);
		}
		StreamWriter _writer = new StreamWriter(_fs);
		_writer.WriteLine(
			(int)_code + "," +
			_date + "," +
			_steadiness + "," +
			_controlOfHorizont + "," +
			_telescoping + "," +
			_periscoping + "," +
			_tracking + "," +
			_agility);
		_writer.Flush();
		_fs.Close();
	}

	public static void DeleteGameSave(string _path) {
		File.Delete(_path);
		PlayerPrefs.DeleteKey("token");
        PlayerPrefs.DeleteKey("email");
	}

	public static void SaveUserData(SUserData _data) {
        string _key = (PlayerPrefs.GetString("email") != "" ? PlayerPrefs.GetString("email") : "GUESTDATA");
        string _path = GetUserPath(_key);
		BinaryFormatter _bf = new BinaryFormatter();
		FileStream _file = File.Create(_path);
		_bf.Serialize(_file, _data);
		_file.Close();
	}

	public static bool UserExists(string _email) {
		string _path = GetUserPath(_email);
		return File.Exists(_path);
	}


	private static string GetUserPath(string _token){
		return Application.persistentDataPath + Path.DirectorySeparatorChar  + _token+ ".sv";
	}
	private static string GetActivityPath(string _email){
		return Application.persistentDataPath + Path.DirectorySeparatorChar + "GamesData-" + _email + ".sv";
	}
}
