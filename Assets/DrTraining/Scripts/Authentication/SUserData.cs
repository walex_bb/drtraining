﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SUserData {

    public string _email;
    public string _password;
    public string _firstName;
    public string _lastName;
    public string _bornDate;
    public string _institution;
    public string _profession;
    //iid is used in case that the user changes email to avoid loss references from game score to the database entry.
    public int _iid;

    public SUserData() { }

    public SUserData(string email, string password, string firstName, string lastName, string bornDate, string institution, string profession){
        _iid = 0;
        _email = email;
        _password = password;
        _firstName = firstName;
        _lastName = lastName;
        _bornDate = bornDate;
        _institution = institution;
        _profession = profession;
    }
}
