﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class LoginRequest
{
   public string Email;
   public string Password;

    public LoginRequest(string email, string password)
    {

        Email = email;
        Password = password;
    }
}


[System.Serializable]
public class AuthResponse
{
    public int Status;
    public string Message;
    public string Token;    
}


[System.Serializable]
public class UserInfo 
  {
    public string FirstName;
    public string LastName; // not used
    public string BornDate; // not used
    public string WorkPlace; // not used
    public string Profession; // not used

    public UserInfo(string firstName, string lastName, string bornDate, string workPlace, string profession)
    {

        FirstName = firstName;
        LastName = lastName;
        BornDate = bornDate;
        WorkPlace = workPlace;
        Profession = profession;
    }
}

[System.Serializable]
public class UserInfo2
{
    public string Email;
    public string FirstName;
    public string LastName;
    public string BornDate;
    public string WorkPlace;
    public string Profession;

    public UserInfo2(string email, string firstName, string lastName, string bornDate, string workPlace, string profession)
    {
        Email = email;
        FirstName = firstName;
        LastName = lastName;
        BornDate = bornDate;
        WorkPlace = workPlace;
        Profession = profession;
    }
}

[System.Serializable]
public class RegisterAccountRequest {

    public string Email;
    public string Password;
    public UserInfo UserInfo;

    public RegisterAccountRequest(string email, string password, UserInfo userInfo) {

        Email = email;
        Password = password;
        UserInfo = userInfo;
    }
}

[System.Serializable]
public class UserDataRequest
{
    public string Token;

    public UserDataRequest(string token) {

        Token = token;
    }
}

[System.Serializable]
public class UserDataResponse
{
    public int Status;
    public string Message;
    public UserInfo2 UserInfo;
    
}

[System.Serializable]
public class UpdateUserInfoRequest
{

    public string Token;
    public UserInfo UserInfo;

    public UpdateUserInfoRequest(string token, UserInfo userInfo)
    {

        Token = token;
        UserInfo = userInfo;
    }
}

[System.Serializable]
public class GenericResponse
{
    public int Status;
    public string Message;  
}

[System.Serializable]
public class RecoverPasswordRequest
{
    public string Email;

    public RecoverPasswordRequest(string email)
    {

        Email = email;
    }
}

public class CLocalAuthentication : MonoBehaviour
{

    public static bool _guestMode = false;
    public CSceneLoader _sceneLoader;

    //public Image _simulateLocked;
    //public Image _simulateUnlocked;

    const string REQUEST_MESSAGE_STATUS_OK = "0";

	const string USER_REGISTER_URL = "UserManagement/Register";
    const string UPDATE_USER_DATA = "UserManagement/UpdateUserData";
    const string USER_LOGIN_URL = "UserManagement/Login";
    const string USER_RECOVERY_PASSWORD_URL = "UserManagement/RecoverPassword";

    const string USER_CHECK_TOKEN_URL = "";
    const string GET_USER_DATA_URL = "UserManagement/GetUserData";
    const string GET_BEST_SCENARY_RESULTS = "ExercisesSummaries/GetBestExercisesResults";
    const string GET_BEST_SKILL_RESULTS = "ExercisesSummaries/GetBestSkillsResults";

    const string CLAIM_SIMULATE_CODE = "ClaimSimulateExerciseCode";
    const string GET_SCENARIES_STATUS = "ExercisesDataInterchange/GetExercisesStatus";

    

    [Header("Guest popup and related")]
    public GameObject _lockedGuestPopup;
    public GameObject _statisticsBtn;
    public GameObject _profileBtn;
    public GameObject _lockedStatisticsBtn;
    public GameObject _lockedProfileBtn;
    public GameObject _guestSignInBtn;
    public GameObject _logoutExternalBtn;

    public GameObject _simulatePopupPanel;
    public GameObject _logoutPopupPanel;
    public GameObject _goBackGuestArrow;

    [Header("Canvas section")]
    public GameObject _gameCanvas;
    public GameObject _authenticationCanvas;

    [Header("Panels section")]
    public GameObject _homePanel;
    public GameObject _loginPanel;
    public GameObject _registerOrEditPanel;
    public GameObject _passwordResetPanel;
    public GameObject _stadisticsPanel;

    [Header("Login fields section")]
    public InputField _loginEmail;
    public InputField _loginPassword;
    public Text _loginEmailError;
    public Text _loginPasswordError;

    [Header("Password Reset references")]
    public InputField _recoveryPasswordEmail;
    public Text _passwordResetMsg;

    [Header("Register/Edit profile section")]
    public Button _openProfileBtn;
    public InputField _editEmail;
    public InputField _editFirstName;
    public InputField _editLastName;
    public InputField _editPassword;
    public InputField _editRepeatPassword;
    public InputField _editInstitution;
    public InputField _editProfesion;
    public GameObject _bornData;
    public Text _editEmailError;
    public GameObject _backButtonUpdateUser, _backButtonCreateUser;
    public Text _submitChangesText;

    public GameObject _registerOrEditLogoutBtn;
    public Text _registerOrEditTitleText;
    public Button _submitUserDataBtn;

    [Header("Error messages")]
    public string _badEmailMsg;
    public string _occupiedEmailMsg;
    public string _emptyPasswordMsg;
    public string _userCredentialsDoesntMatchMsg;
    public string _nameRequieredMsg;
    public string _passwordRequieredMsg;
    public string _passwordDoesntMatchMsg;
    public string _institutionNameRequiredMsg;

    [Header("Stadistics references from scene")]
    public List<CCardPanel> _cards;

    [Header("Enable or disable depending on connection state")]
    public Button _homeLoginBtn;
    public Button _homeRegisterBtn;
    public Button _loginSendLoginBtn;
    public Button _recoveryPasswordBtn;
    public Button _statisticsBtnBtn;
    public Button _profileUpdateBtn;
    public Button _homeProfileBtn;

    

    CLoadingAdvice _loadingAdvice;    

    void Start() {        

        _loadingAdvice = GameObject.FindWithTag("LoadingAdvice").GetComponent<CLoadingAdvice>();
        if (_guestMode)
        {
            TouchHomeLoginAsGuest();
        }
        else
        {   

            StartCoroutine(CheckIfUserWasLogged());
        }
    }

    public GameObject _blockPanel;

    void BlockPanelOn() {
        _blockPanel.SetActive(true);
    }

    void BlockPanelOff() {
        _blockPanel.SetActive(false);
    }

    void Update() {
        if (_loadingAdvice._isConnected) {
            _homeLoginBtn.interactable = true;
            _homeRegisterBtn.interactable = true;
            _statisticsBtnBtn.interactable = true;
            _profileUpdateBtn.interactable = true;
            _homeProfileBtn.interactable = true;
            _loginSendLoginBtn.interactable = true;
            _recoveryPasswordBtn.interactable = true;
        }
        else
        {
            _homeLoginBtn.interactable = false;
            _homeRegisterBtn.interactable = false;
            _statisticsBtnBtn.interactable = false;
            _profileUpdateBtn.interactable = false;
            _homeProfileBtn.interactable = false;
            _loginSendLoginBtn.interactable = false;
            _recoveryPasswordBtn.interactable = false;
        }
    }

    IEnumerator CheckIfUserWasLogged() {
		//PlayerPrefs.DeleteKey("token");
        bool _userWasLogged = false;
        if (PlayerPrefs.HasKey("token"))
        {
            if (PlayerPrefs.GetString("token") != "")
            {
                bool _wasTokenValid = true;
                yield return null;

                if (_loadingAdvice._isConnected)
                {

                    WWWForm _form = new WWWForm();
                    byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLGetUserDataSend(PlayerPrefs.GetString("token")));

                    //Dictionary<string, string> _headers = _form.headers;
                    //_headers["Content-Type"] = "application/xml";
                    //_headers["Accept"] = "application/xml";

                    Dictionary<string, string> _headers = new Dictionary<string, string>();
                    _headers["Content-Type"] = "application/json";

                    WWW _www = new WWW(GameConstants.BASE_URL + GET_USER_DATA_URL, _raw, _headers);
                    yield return _www;

                    if (_www.error != null)
                    {
                        Debug.Log("" + _www.error);
                        _wasTokenValid = false;
                    }
                    else
                    {
                        Dictionary<string, string> _userData = new Dictionary<string, string>();
                        
                        int status = XMLRequestResponsesParser.XMLGetUserDataResponse(_www.text, _userData);

                        //Dictionary<string, string> _requestResult = XMLRequestResponsesParser.XMLGetUserDataMsgParse(_www.text);
                        //if (_requestResult["Status"] == REQUEST_MESSAGE_STATUS_OK)
                        if (status == 0)
                        {
                            _wasTokenValid = true;
                        }
                        else
                        {
                            _wasTokenValid = false;
                            //Gestionar errores aqui
                        }

                    }
                }

                else {
                    _wasTokenValid = true;
                }
                //parse _www.text to check if token is valir or not, set _wasTokenValid

                if (_wasTokenValid)
                {
                    _userWasLogged = true;
                }
                else{
                    PlayerPrefs.DeleteKey("token");
                }
            }
            else
                PlayerPrefs.DeleteKey("token");
        }

        if (!_userWasLogged)
        {
            _gameCanvas.SetActive(false);
            _homePanel.SetActive(true);
            _authenticationCanvas.SetActive(true);
            PreHomeAnimationIn(() => {});
        }
        else {
            EnableDisableStatisticsButtons(true);
            _gameCanvas.SetActive(true);
            _homePanel.SetActive(false);
            _authenticationCanvas.SetActive(false);

            HomeAnimationIn(() => { });
        }
    }
    

    public void TouchHomeRegister() {
        PreHomeAnimationOut(() => {
            _homePanel.SetActive(false);
            _registerOrEditLogoutBtn.SetActive(false);

            ClearErrorMsgs();
            _submitUserDataBtn.onClick.RemoveAllListeners();

            _submitUserDataBtn.onClick.AddListener(() =>
            {
                CAudioManager.Inst.PlaySFXSimple("Button");
                SendRegisterData();
            });

            _submitChangesText.text = "REGISTER";
            _editEmail.interactable = true;

            _registerOrEditTitleText.text = "REGISTER";
            _editEmail.text = "";
            _editInstitution.text = "";
            _editFirstName.text = "";
            _editLastName.text = "";
            _editPassword.text = "";
            _editRepeatPassword.text = "";
            _editProfesion.text = "";
            //_btnBornDate.GetComponentInChildren<Text>().text = "1/1/1960";

            _registerOrEditPanel.SetActive(true);

            ProfileAnimationIn(() => {
                //enable/disable one or another button
                _backButtonUpdateUser.SetActive(false);
                _backButtonCreateUser.SetActive(true);
            });

        });
        
    }

    public void TouchHomeLogin() {
        PreHomeAnimationOut(() =>
        {
            _homePanel.SetActive(false);
            _loginPanel.SetActive(true);
            LoginAnimationIn(() => { 
            
            });
            
        });
    }

    public void CloseGuestPopupPanel()
    {
        _lockedGuestPopup.GetComponent<CPopupScalator>().PanelOut(() => {
            _lockedGuestPopup.SetActive(false);
        });
    }

    public void CloseSimulatePopupPanel() {
        _simulatePopupPanel.GetComponent<CPopupScalator>().PanelOut(() =>
        {
            _simulatePopupPanel.SetActive(false);
        });
    }

    public void OpenSimulatePopupPanel()
    {
        _simulatePopupPanel.SetActive(true);
    }

    public void OpenLogoutPopupPanel() {
        _logoutPopupPanel.SetActive(true);
    }

    public void CloseLogoutPopupPanel() {
        _logoutPopupPanel.GetComponent<CPopupScalator>().PanelOut(() =>
        {
            _logoutPopupPanel.SetActive(false);
        });
    }

    void EnableDisableStatisticsButtons(bool _enableOrDisable) {
        _statisticsBtn.SetActive(_enableOrDisable);
        _profileBtn.SetActive(_enableOrDisable);
        _logoutExternalBtn.SetActive(_enableOrDisable);

        _lockedStatisticsBtn.SetActive(!_enableOrDisable);
        _lockedProfileBtn.SetActive(!_enableOrDisable);
        _guestSignInBtn.SetActive(!_enableOrDisable);
        _goBackGuestArrow.SetActive(!_enableOrDisable);
        
    }

    public void TouchHomeLoginAsGuest() {
        PreHomeAnimationOut(() =>
        {
            _authenticationCanvas.SetActive(false);
            EnableDisableStatisticsButtons(false);
            _gameCanvas.SetActive(true);
            HomeAnimationIn(() => {
                GlobalScenaryData.Inst.LoadLocalScenaryResultsFromDisk();
                _guestMode = true;
            });  
        });
    }

    public void GoToHomeLoginRegisterAsGuest() {
        HomeAnimationOut(() =>
        {
            _gameCanvas.SetActive(false);
            _authenticationCanvas.SetActive(true);
            PreHomeAnimationIn(() =>
            {
                _loginPanel.SetActive(false);
                _registerOrEditPanel.SetActive(false);
                _homePanel.SetActive(true); 
            });
        });
    }

    public void SendLoginData() {
        ClearErrorMsgs();
        if (_loginEmail.text == "")
        {
            _loginEmailError.text = _badEmailMsg;
        }
        else {
            if (_loginPassword.text == "")
            {
                _loginEmailError.text = _emptyPasswordMsg;
            }
            else{

                // this is the valid code
                StartCoroutine(SendLoginUserDataToServer());              

            }
        }
    }

    public void RecoveryPassword() {
        StartCoroutine(RecoveryPasswordRequest());
    }

    IEnumerator RecoveryPasswordRequest() {

        //yield return null;
        //WWWForm _form = new WWWForm();
        byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLUserRecoveryPasswordSend(new RecoverPasswordRequest(_recoveryPasswordEmail.text)));
        //Dictionary<string, string> _headers = _form.headers;
        //_headers["Accept"] = "application/xml";
        //_headers["Content-Type"] = "application/xml";

        Dictionary<string, string> _headers = new Dictionary<string, string>();
        _headers["Content-Type"] = "application/json";

        WWW _www = new WWW(GameConstants.BASE_URL + USER_RECOVERY_PASSWORD_URL, _raw, _headers);
        yield return _www;

        if (_www.error != null)
        {
            Debug.Log("" + _www.error);
            _passwordResetMsg.text = "<color=red>The account doesn't exists</color>";
        }
        else
        {
            GenericResponse gr = JsonUtility.FromJson<GenericResponse>(_www.text);
            if (gr.Status != 0)
            {
                string errDesc = "XMLUserRecoveryPasswordSend error " + gr.Status.ToString() + '\n';                
                string m = gr.Message;
                errDesc += m;                
                Debug.Log(errDesc);
                _passwordResetMsg.text = "<color=red>" + m + "</color>";
            }
            else
            {
                _passwordResetMsg.text = "<color=green>Reset instructions will be sended to your email.</color>";
            }
            
        }
    }

    /*
    public static void EnableSimulationImage()
    {
        bool enable = CLocalPersist.GetUserLevel() >= 0;
        if (_simulateLocked != null)
            _simulateLocked.enabled = !enable;
        if (_simulateUnlocked != null)
            _simulateUnlocked.enabled = enable;
    }
    */

    IEnumerator SendLoginUserDataToServer() {
        BlockPanelOn();

        //WWWForm _form = new WWWForm();
        byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLUserLoginSend(new LoginRequest(_loginEmail.text, _loginPassword.text)));
        
        //Dictionary<string, string> _headers = _form.headers;
        //_headers["Accept"] = "application/xml";
        //_headers["Content-Type"] = "application/xml";

        Dictionary<string, string> _headers = new Dictionary<string, string>();
        _headers["Content-Type"] = "application/json";

        _loadingAdvice._childAdvice.SetActive(true);

        Invoke("CancelLogin", 10);
        WWW _www = new WWW(GameConstants.BASE_URL + USER_LOGIN_URL, _raw, _headers);
        yield return _www;
        CancelInvoke("CancelLogin");

        if (_www.error != null)
        {
            BlockPanelOff();
            Debug.Log("" + _www.error);
            _loadingAdvice._childAdvice.SetActive(false);
        }
        else
        {
            BlockPanelOff();
            _loadingAdvice._childAdvice.SetActive(false);
            PlayerPrefs.SetString("email", _loginEmail.text);
            string token;
            Dictionary<string, string> _requestResult = XMLRequestResponsesParser.XMLUserLoginMsgParse(_www.text, out token);
            //Debug.Log(_www.text);
            if (_requestResult["Status"] == REQUEST_MESSAGE_STATUS_OK)
            {
                _loginEmailError.text = "";
                PlayerPrefs.SetString("token", token);
                StartCoroutine(SaveLocalUserData(PlayerPrefs.GetString("token")));

                EnableDisableStatisticsButtons(true);

                LoginAnimationOut(() =>
                {

                _gameCanvas.SetActive(true);
                _authenticationCanvas.SetActive(false);
                 //EnableSimulationImage();

                 HomeAnimationIn(() => {});

                    GlobalScenaryData.Inst.LoadLocalScenaryResultsFromDisk();

                    _loginEmail.text = "";
                    _loginPassword.text = "";
                    ClearErrorMsgs();

                    if (_guestMode)
                    {

                        //GlobalScenaryData.Inst.SendActivityResultsToServer();
                        _guestMode = false;
                    }
                });
                
            }
            else
            {
                _loginEmailError.text = _requestResult["Description"];
            }
        }
    }

    IEnumerator SaveLocalUserData(string _token, bool _enableUserEditPanel = false) {
        BlockPanelOn();
        _loadingAdvice._childAdvice.SetActive(true);
        _openProfileBtn.interactable = false;
        WWWForm _form = new WWWForm();
        byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLGetUserDataSend(_token));

        //Dictionary<string, string> _headers = _form.headers;
        //_headers["Accept"] = "application/xml";
        //_headers["Content-Type"] = "application/xml";

        Dictionary<string, string> _headers = new Dictionary<string, string>();
        _headers["Content-Type"] = "application/json";

        WWW _www = new WWW(GameConstants.BASE_URL + GET_USER_DATA_URL, _raw, _headers);
        yield return _www;

        if (_www.error != null)
        {
            BlockPanelOff();
            _loadingAdvice._childAdvice.SetActive(false);
            Debug.Log("" + _www.error);
        }
        else
        {
            BlockPanelOff();
            _loadingAdvice._childAdvice.SetActive(false);
            Dictionary<string, string> _userData = new Dictionary<string, string>();

            int status = XMLRequestResponsesParser.XMLGetUserDataResponse(_www.text, _userData);
            if (status == 0)
            {
                CLocalPersist._currentUser = new SUserData(_userData["Email"], "", _userData["FirstName"], _userData["LastName"], _userData["BornDate"], _userData["WorkPlace"], _userData["Profession"]);
                SetUserEditPanelFields();
                _openProfileBtn.interactable = true;

                if (_enableUserEditPanel == true)
                    SetUserEditPanelFields();
            } else {

                Debug.Log(string.Format("Error XMLGetUserDataResponse retreiving data, status {0}", status));
            }
        }
        Debug.Log(_www.text);
    }

    public void TouchLoginDontRememberPassword() {
        _passwordResetPanel.SetActive(true);
        _loginPanel.SetActive(false);
    }

    public bool IsValidEmail(string _email) {
        System.Text.RegularExpressions.Regex mailValidator = new System.Text.RegularExpressions.Regex(@"^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$");
        return mailValidator.IsMatch(_email);
    }

    public void SendRegisterData() {
         //RegexUtilities util = new RegexUtilities();
        if (_editEmail.text == ""){
            _editEmailError.text = _badEmailMsg;
        }
        else if (!IsValidEmail(_editEmail.text))
        {
            _editEmailError.text = _badEmailMsg; 
        }
        else if (_editFirstName.text == "" )//|| _editLastName.text == "")
        {
            _editEmailError.text = _nameRequieredMsg;
        }
        else if (_editPassword.text == "")
        {
            _editEmailError.text = _passwordRequieredMsg;
        }
        else if (_editRepeatPassword.text == "")
        {
            _editEmailError.text = _passwordDoesntMatchMsg;
        }
        else if (_editPassword.text != _editRepeatPassword.text)
        {
            _editEmailError.text = _passwordDoesntMatchMsg;
        }/*
        else if (_editInstitution.text == ""){
            _editEmailError.text = _institutionNameRequiredMsg;
        }*/
        else {
			StartCoroutine(SendRegisterUserDataToServer());
        }

    }

    public GameObject _errorWithServerText;
    private void CancelRegister()
    {
        Debug.Log("Cancel register called");
        StopCoroutine(SendRegisterUserDataToServer());
        BlockPanelOff();
        _errorWithServerText.SetActive(true);
        _loadingAdvice._childAdvice.SetActive(false);
    }

    private void CancelLogin() {
        Debug.Log("Cancel register called");
        StopCoroutine(SendLoginUserDataToServer());
        BlockPanelOff();
        _errorWithServerText.SetActive(true);
        _loadingAdvice._childAdvice.SetActive(false);
    }

    private void CancelProfile() {
        StopCoroutine(OpenUserEditPanelRequest());
        BlockPanelOff();
        _loadingAdvice._childAdvice.SetActive(false);
    }

    private void CancelStatistics() {
        StopCoroutine(OpenBestSkillsPanelRequest());
        BlockPanelOff();
        _loadingAdvice._childAdvice.SetActive(false);
    }


	IEnumerator SendRegisterUserDataToServer(){

        BlockPanelOn();
        Invoke("CancelRegister", 10f);
        
		//WWWForm _form = new WWWForm ();

        string _userFirstName = _editFirstName.text.Trim();
        string _userLastName = "na";
        string _userPassword = _editPassword.text.Trim();
        string _userEmail = _editEmail.text.Trim();
        string _bornDate = "1980-05-01";
        string _workPlace = "na";
        string _profession = "na";

        byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLUserRegisterSend(new RegisterAccountRequest(_userEmail, _userPassword, new UserInfo(_userFirstName, _userLastName, _bornDate, _workPlace, _profession))));

        //Dictionary<string, string> _headers = _form.headers;
        //_headers["Accept"] = "application/xml";
        //_headers["Content-Type"] = "application/xml";

        Dictionary<string, string> _headers = new Dictionary<string, string>();
        _headers["Content-Type"] = "application/json";

        _loadingAdvice._childAdvice.SetActive(true);

        string _email = _editEmail.text;
        WWW _www = new WWW(GameConstants.BASE_URL + USER_REGISTER_URL, _raw, _headers);
		yield return _www;

        CancelInvoke("CancelRegister");

        if (_www.error != null)
        {
            BlockPanelOff();
            _loadingAdvice._childAdvice.SetActive(false);
            Debug.Log("" + _www.error); 
        }
        else
        {
            BlockPanelOff();
            _loadingAdvice._childAdvice.SetActive(false);
            Debug.Log(_www.text);

            AuthResponse res = JsonUtility.FromJson<AuthResponse>(_www.text);
            if (res.Status == 0)
            {
                GlobalScenaryData.Inst.MoveGuestLocalResultsToUserLocalResults(_email);
                //Use the login service
                _loginEmail.text = _userEmail;
                _loginPassword.text = _userPassword;

                ProfileAnimationOut(() =>
                {
                    SendLoginData();
                });
            }
            else {

                BlockPanelOff();
                _loadingAdvice._childAdvice.SetActive(false);
                string errDesc = "XMLUserRegisterSend error " + res.Status.ToString() + '\n';
                string m = res.Message;
                errDesc += m;                
                Debug.Log(errDesc);
            }
        }



	}

    public void EditBornData() {
        string tText = _bornData.GetComponent<UI.Dates.DatePicker>().Ref_InputField.text;  //_btnBornDate.GetComponentInChildren<Text>();
        //string [] _fields = tText.Split('-'); // para que se corta?
        // Poner acá cambio de fecha de nacimiento
    }

    public void TouchLogout() {
        ClearErrorMsgs();
        PlayerPrefs.DeleteKey("email");
        PlayerPrefs.DeleteKey("token");
        GlobalScenaryData.Inst.DeleteGuestData();
        CLocalPersist.DeleteGuestLevelData();
        GlobalScenaryData.Inst.ClearCurrentPlayerData();
        

        _loginPanel.SetActive(false);
        _registerOrEditPanel.SetActive(false);
        
        HomeAnimationOut(() => {
            _gameCanvas.SetActive(false);

            _homePanel.SetActive(true);
            _authenticationCanvas.SetActive(true);
            _editPassword.transform.parent.gameObject.SetActive(true);
            _editRepeatPassword.transform.parent.gameObject.SetActive(true);
            CLocalPersist._currentUser = null;
            PreHomeAnimationIn(() =>
            {

            });

            
        });
    }

    public void ClearErrorMsgs() {
        _loginEmailError.text = "";
        _editEmailError.text = "";
        _errorWithServerText.SetActive(false);
    }

    public void OpenStatisticsPanel() {
        //StartCoroutine(OpenStatisticsPanelRequest());

        _sceneLoader.LoadScene("Stats");
    }

    public void ShowLockedGuestPopup() {
        _lockedGuestPopup.SetActive(true);
    }

    // BEGIN PATRONE revisa estas funciones para agregarle data al panel de skills
    public void OpenBestSkillsPanel() {
        //StartCoroutine(OpenBestSkillsPanelRequest());
        BlockPanelOn();
        StatisticsPanelsAnimationOut(() =>
        {
            BlockPanelOff();
            _overallPanel.SetActive(true);
            CSkillsStatisticsResume.Inst.TurnOnPanel(this);
        });
        
    }

    IEnumerator OpenBestSkillsPanelRequest() {
        BlockPanelOn();
        WWWForm _form = new WWWForm();
        byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLGetBestSkillResultsSend(PlayerPrefs.GetString("token")));
        Dictionary<string, string> _headers = _form.headers;
        _headers["Accept"] = "application/xml";
        _headers["Content-Type"] = "application/xml";
        _loadingAdvice._childAdvice.SetActive(true);
        Invoke("CancelStatistics", 20);
        WWW _www = new WWW(GameConstants.BASE_URL + GET_BEST_SKILL_RESULTS, _raw, _headers);
        yield return _www;
        CancelInvoke("CancelStatistics");

        if (_www.error != null)
        {
            BlockPanelOff();
            _loadingAdvice._childAdvice.SetActive(false);
            Debug.Log("" + _www.error);
        }
        else
        {
            BlockPanelOff();
            _loadingAdvice._childAdvice.SetActive(false);
            CLocalPersist._bestSkillResults = XMLRequestResponsesParser.XMLGetBestSkillResultsParse(_www.text);
            CSkillsStatisticsResume.Inst.ActualizarPanel(CLocalPersist._bestSkillResults);
        }
    }

    // END PATRONE

    IEnumerator OpenStatisticsPanelRequest() {


        BlockPanelOn();
        
        //
        //CLocalPersist.LoadUserGameData(PlayerPrefs.GetString("token"));

        WWWForm _form = new WWWForm();
        byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLGetBestScenaryResultsSend(PlayerPrefs.GetString("token")));
        Dictionary<string, string> _headers = _form.headers;
        _headers["Accept"] = "application/xml";
        _headers["Content-Type"] = "application/xml";

        _loadingAdvice._childAdvice.SetActive(true);

        WWW _www = new WWW(GameConstants.BASE_URL + GET_BEST_SCENARY_RESULTS, _raw, _headers);
        yield return _www;

        if (_www.error != null)
        {
            BlockPanelOff();
            _loadingAdvice._childAdvice.SetActive(false);
            Debug.Log("" + _www.error);
        }
        else
        {
            BlockPanelOff();
            _loadingAdvice._childAdvice.SetActive(false);
            CLocalPersist._bestScoreActivities = XMLRequestResponsesParser.XMLGetBestScenaryResultsParse(_www.text);
            yield return StartCoroutine(OpenBestSkillsPanelRequest());

            for (int i = 0; i < _cards.Count; i++)
            {
                _cards[i].GenerateCached();
                if (CLocalPersist._bestScoreActivities.ContainsKey(_cards[i]._code))
                {

                    SActivityData _activityData = CLocalPersist._bestScoreActivities[_cards[i]._code];
                    _cards[i]._wasActivityPlayed = true;
                    List<SIndicator> _indicator = _cards[i]._indicatorsData;
                    for (int j = 0; j < _indicator.Count; j++)
                    {
                        int _value = -1;
                        switch (_indicator[j]._code)
                        {
                            case "AGI":
                                _value = _activityData._agility;
                                break;
                            case "LNAV":
                                _value = _activityData._linearNavigation;
                                break;
                            case "ANAV":
                                _value = _activityData._angularNavigation;
                                break;
                            case "STE":
                                _value = _activityData._steadiness;
                                break;
                            case "TRA":
                                _value = _activityData._tracking;
                                break;
                            case "AIM":
                                _value = _activityData._centering;
                                break;
                            case "HC":
                                _value = _activityData._horizonControl;
                                break;
                        }
                        _cards[i].SetIndicator(_indicator[j]._code, _value);
                    }
                }
                else
                {
                    //_cards[i]._wasActivityPlayed = false;
                    _cards[i].SetIndicator("AGI", -1);
                    _cards[i].SetIndicator("LNAV", -1);
                    _cards[i].SetIndicator("ANAV", -1);
                    _cards[i].SetIndicator("STE", -1);
                    _cards[i].SetIndicator("TRA", -1);
                    _cards[i].SetIndicator("AIM", 1);
                    _cards[i].SetIndicator("HC", 1);
                }
            }

            HomeAnimationOut(() =>
            {
                _stadisticsPanel.SetActive(true);
                _gameCanvas.SetActive(false);
                _registerOrEditPanel.SetActive(false);
                _loginPanel.SetActive(false);
                _authenticationCanvas.SetActive(true);

                StatisticsPanelsAnimation();
            }); 
        }
       
    }

    public void OpenUserEditPanel(){

        if (CLocalPersist._currentUser == null)
        {
            StartCoroutine(SaveLocalUserData(PlayerPrefs.GetString("token"), true));
        }

        _sceneLoader.LoadScene("Profile");

        /*
        _submitUserDataBtn.onClick.RemoveAllListeners();
        _submitUserDataBtn.onClick.AddListener(() =>
        {
            CAudioManager.Inst.PlaySFXSimple("Button");
            StartCoroutine(OpenUserEditPanelRequest());
        });

        _editEmail.interactable = false;
        _registerOrEditLogoutBtn.SetActive(true);
        _registerOrEditTitleText.text = "PROFILE";
        
        
        _backButtonUpdateUser.SetActive(true);
        _backButtonCreateUser.SetActive(false);

        HomeAnimationOut(() => {
            _gameCanvas.SetActive(false);
            _authenticationCanvas.SetActive(true);
            _homePanel.SetActive(false);
            _loginPanel.SetActive(false);
            ProfileAnimationIn(() => { });
        });
       
        if (CLocalPersist._currentUser == null)
        {
            StartCoroutine(SaveLocalUserData(PlayerPrefs.GetString("token"), true));
        }
        else {
            SetUserEditPanelFields();
        }

        */
    }
    
    IEnumerator OpenUserEditPanelRequest() {
        BlockPanelOn();
        yield return null;
        //WWWForm _form = new WWWForm();
        string _firstName = _editFirstName.text.Trim();
        string _lastName = "na";
        string _bornDate = "1980-05-01";
        string _workPlace = "na";
        string _profession = "na";
        UpdateUserInfoRequest uer = new UpdateUserInfoRequest(PlayerPrefs.GetString("token"), new UserInfo(_firstName, _lastName, _bornDate, _workPlace, _profession));
        //byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLUpdateUserDataSend(PlayerPrefs.GetString("token"), _editFirstName.text, _editLastName.text, _bornData.GetComponent<UI.Dates.DatePicker>().Ref_InputField.text, _editInstitution.text));
        byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLUpdateUserDataSend(uer));
        //Dictionary<string, string> _headers = _form.headers;
        //_headers["Accept"] = "application/xml";
        //_headers["Content-Type"] = "application/xml";

        Dictionary <string, string> _headers = new Dictionary<string, string>();
        _headers["Content-Type"] = "application/json";

        _loadingAdvice._childAdvice.SetActive(true);
        Invoke("CancelProfile", 10);
        WWW _www = new WWW(GameConstants.BASE_URL + UPDATE_USER_DATA, _raw, _headers);
        yield return _www;

        CancelInvoke("CancelProfile");
        if (_www.error != null)
        {
            BlockPanelOff();
            _loadingAdvice._childAdvice.SetActive(false);
            Debug.Log("" + _www.error);
        }
        else
        {
            GenericResponse gr = JsonUtility.FromJson<GenericResponse>(_www.text);
            if (gr.Status == 0)
            {
                BlockPanelOff();
                _loadingAdvice._childAdvice.SetActive(false);
                Debug.Log(_www.text);
                StartCoroutine(SaveLocalUserData(PlayerPrefs.GetString("token")));
            }
            else
            {
                string errDesc = "XMLUpdateUserDataSend error " + gr.Status.ToString() + '\n';
                string m = gr.Message;
                errDesc += m;                
                Debug.Log(errDesc);
            }
        }
    }

    void UpdateDatePickerVisual() {
        UI.Dates.DatePicker _datePicker = _bornData.GetComponent<UI.Dates.DatePicker>();

        string[] _dateParts = CLocalPersist._currentUser._bornDate.Split('-');
        UI.Dates.SerializableDate _sDate = _datePicker.SelectedDate;
        _sDate.Date = new DateTime(Int32.Parse(_dateParts[0]), Int32.Parse(_dateParts[1]), Int32.Parse(_dateParts[2]));
        _datePicker.SelectedDate = _sDate;
        _datePicker.UpdateDisplay();
        _datePicker.UpdateInputFieldText();
        Debug.Log(CLocalPersist._currentUser._bornDate); 
    }

    void SetUserEditPanelFields() {
        _submitChangesText.text = "UPDATE";

        UpdateDatePickerVisual();
        _editEmail.text = CLocalPersist._currentUser._email;
        _editInstitution.text = CLocalPersist._currentUser._institution;
        _editFirstName.text = CLocalPersist._currentUser._firstName;
        _editLastName.text = CLocalPersist._currentUser._lastName;
        _editProfesion.text = CLocalPersist._currentUser._profession;

        _editPassword.transform.parent.gameObject.SetActive(false);
        _editRepeatPassword.transform.parent.gameObject.SetActive(false);

        _registerOrEditPanel.SetActive(true);
        
        //_bornData.GetComponent<UI.Dates.DatePicker>().Show();
    }

    void ResetStatisticsCards(){
        for (int i = 0; i < _cards.Count; i++) {
            _cards[i].Reset();
        }
    }

    public void GoBack(string _panelName) {
        switch (_panelName) { 
            case "LOGIN":
                LoginAnimationOut(() => {
                    _loginPanel.SetActive(false);
                    _homePanel.SetActive(true);
                    PreHomeAnimationIn(() =>
                    { });
                });   
                break;
            case "REGISTER":
                ProfileAnimationOut(() =>
                {
                    _homePanel.SetActive(true);
                    _registerOrEditPanel.SetActive(false);
                    PreHomeAnimationIn(() =>
                    {

                    });
                });
                break;
            case "STADISTICS":
                StatisticsPanelsAnimationOut(() => {
                    _stadisticsPanel.SetActive(false);
                    _gameCanvas.SetActive(true);
                    _authenticationCanvas.SetActive(false);
                    ResetStatisticsCards();
                    HomeAnimationIn(() => { });
                });
                break;
            case "UPDATE_PROFILE":
                ProfileAnimationOut(() => {
                    _registerOrEditPanel.SetActive(false);
                    _gameCanvas.SetActive(true);
                    _authenticationCanvas.SetActive(false);
                    HomeAnimationIn(() => { });
                });
                break;
            case "PASSWORD_RECOVERY":
                _passwordResetPanel.SetActive(false);
                _loginPanel.SetActive(true);
                break;
            case "OVERALL":
                OverallAnimationOut(() => {
                    _overallPanel.SetActive(false);
                    _statisticsPanel.SetActive(true);
                    StatisticsPanelsAnimation(); 
                });
                break;
        }
    }
    

    public void GoToAllGamesPanel()
    {
        if (CLocalPersist.GetUserLevel() >= 0)
        {
            BlockPanelOn();
            HomeAnimationOut(() => {
                BlockPanelOff();

                _sceneLoader.LoadScene("AllGamesPanel");

            });
        }

        
    }   

    public GameObject _overallPanel, _statisticsPanel;

    [Header("Statistics cards animation")]
    public RectTransform[] _statisticsCards;
    public RectTransform[] _statisticsCardsInside;
    public RectTransform[] _statisticsCardsOutside;
    public float _statisticsCardsComeInTime;

    public void StatisticsPanelsAnimation(Action _callback = null) {
        for (int i = 0; i < _statisticsCards.Length; i++) {
            _statisticsCards[i].anchoredPosition = _statisticsCardsOutside[i].anchoredPosition;
        }
            StartCoroutine(
                CCodeAnimations.LerpDisplacementPanels(() =>
                {
                    StartCoroutine(
                        CCodeAnimations.LerpDisplacementPanels(() =>
                        {
                            StartCoroutine(
                                CCodeAnimations.LerpDisplacementPanels(() =>
                                {
                                    StartCoroutine(
                                        CCodeAnimations.LerpDisplacementPanels(() =>
                                        {
                                            StartCoroutine(
                                                CCodeAnimations.LerpDisplacementPanels(() =>
                                                {
                                                    StartCoroutine(
                                                        CCodeAnimations.LerpDisplacementPanels(() =>
                                                        {
                                                            StartCoroutine(
                                                                CCodeAnimations.LerpDisplacementPanels(() =>
                                                                {
                                                                    if (_callback != null)
                                                                        _callback();
                                                                }, _statisticsCards[6], _statisticsCardsOutside[6].anchoredPosition, _statisticsCardsInside[6].anchoredPosition, _statisticsCardsComeInTime)
                                                            );
                                                        }, _statisticsCards[5], _statisticsCardsOutside[5].anchoredPosition, _statisticsCardsInside[5].anchoredPosition, _statisticsCardsComeInTime)
                                                    );
                                                }, _statisticsCards[4], _statisticsCardsOutside[4].anchoredPosition, _statisticsCardsInside[4].anchoredPosition, _statisticsCardsComeInTime)
                                            );
                                        }, _statisticsCards[3], _statisticsCardsOutside[3].anchoredPosition, _statisticsCardsInside[3].anchoredPosition, _statisticsCardsComeInTime)
                                    );
                                }, _statisticsCards[2], _statisticsCardsOutside[2].anchoredPosition, _statisticsCardsInside[2].anchoredPosition, _statisticsCardsComeInTime)
                            );
                        }, _statisticsCards[1], _statisticsCardsOutside[1].anchoredPosition, _statisticsCardsInside[1].anchoredPosition, _statisticsCardsComeInTime)
                    );
                }, _statisticsCards[0], _statisticsCardsOutside[0].anchoredPosition, _statisticsCardsInside[0].anchoredPosition, _statisticsCardsComeInTime)
            );
    }

    public void StatisticsPanelsAnimationOut(Action _callback)
    {
        StartCoroutine(
            CCodeAnimations.LerpDisplacementPanels(() =>
            {
                StartCoroutine(
                    CCodeAnimations.LerpDisplacementPanels(() =>
                    {
                        StartCoroutine(
                            CCodeAnimations.LerpDisplacementPanels(() =>
                            {
                                StartCoroutine(
                                    CCodeAnimations.LerpDisplacementPanels(() =>
                                    {
                                        StartCoroutine(
                                            CCodeAnimations.LerpDisplacementPanels(() =>
                                            {
                                                StartCoroutine(
                                                    CCodeAnimations.LerpDisplacementPanels(() =>
                                                    {
                                                        StartCoroutine(
                                                            CCodeAnimations.LerpDisplacementPanels(() =>
                                                            {
                                                                Debug.Log("Calling callback of StatisticsPanelsAnimationOut");
                                                                _callback();
                                                            }, _statisticsCards[0],_statisticsCardsInside[0].anchoredPosition, _statisticsCardsOutside[0].anchoredPosition,  _statisticsCardsComeInTime)
                                                        );
                                                    }, _statisticsCards[1],_statisticsCardsInside[1].anchoredPosition, _statisticsCardsOutside[1].anchoredPosition,  _statisticsCardsComeInTime)
                                                );
                                            }, _statisticsCards[2],_statisticsCardsInside[2].anchoredPosition, _statisticsCardsOutside[2].anchoredPosition,  _statisticsCardsComeInTime)
                                        );
                                    }, _statisticsCards[3],_statisticsCardsInside[3].anchoredPosition, _statisticsCardsOutside[3].anchoredPosition,  _statisticsCardsComeInTime)
                                );
                            }, _statisticsCards[4],_statisticsCardsInside[4].anchoredPosition, _statisticsCardsOutside[4].anchoredPosition,  _statisticsCardsComeInTime)
                        );
                    }, _statisticsCards[5],_statisticsCardsInside[5].anchoredPosition, _statisticsCardsOutside[5].anchoredPosition,  _statisticsCardsComeInTime)
                );
            }, _statisticsCards[6], _statisticsCardsInside[6].anchoredPosition, _statisticsCardsOutside[6].anchoredPosition,  _statisticsCardsComeInTime)
        );
    }

    [Header("PreHome animation")]
    public float _preHomeAnimationTime = .6f;
    public RectTransform _logoInHome;
    public RectTransform _botoneraInHome;
    public RectTransform _logoAnchorOut, _logoAnchorIn, _botoneraAnchorOut, _botoneraAnchorIn;

    public void PreHomeAnimationIn(Action _callback) {
        StartCoroutine(
            CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _logoInHome, _logoAnchorOut.anchoredPosition, _logoAnchorIn.anchoredPosition, _preHomeAnimationTime)
            );

        StartCoroutine(
            CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _botoneraInHome, _botoneraAnchorOut.anchoredPosition, _botoneraAnchorIn.anchoredPosition, _preHomeAnimationTime)
            );
    }

    public void PreHomeAnimationOut(Action _callback)
    {
        StartCoroutine(
            CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _logoInHome, _logoAnchorIn.anchoredPosition, _logoAnchorOut.anchoredPosition, _preHomeAnimationTime)
            );

        StartCoroutine(
            CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _botoneraInHome, _botoneraAnchorIn.anchoredPosition, _botoneraAnchorOut.anchoredPosition, _preHomeAnimationTime)
            );
    }

    [Header("Home animation")]
    public float _homeAnimationTime = 1;
    public RectTransform _trainRect, _simulateRect, _bottomBarRect;
    public RectTransform _trainAnchorIn, _trainAnchorOut, _simulateAnchorIn, _simulateAnchorOut, _bottomBarAnchorIn, _bottomBarAnchorOut;

    public void HomeAnimationIn(Action _callback) {
        StartCoroutine(
            CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _trainRect, _trainAnchorOut.anchoredPosition, _trainAnchorIn.anchoredPosition, _homeAnimationTime)
            );

        StartCoroutine(
            CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _simulateRect, _simulateAnchorOut.anchoredPosition, _simulateAnchorIn.anchoredPosition, _homeAnimationTime)
            );

        StartCoroutine(
            CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _bottomBarRect, _bottomBarAnchorOut.anchoredPosition, _bottomBarAnchorIn.anchoredPosition, _homeAnimationTime)
            );
    }

    public void HomeAnimationOut(Action _callback)
    {
        StartCoroutine(
            CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _trainRect, _trainAnchorIn.anchoredPosition, _trainAnchorOut.anchoredPosition, _homeAnimationTime)
            );

        StartCoroutine(
            CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _simulateRect, _simulateAnchorIn.anchoredPosition, _simulateAnchorOut.anchoredPosition, _homeAnimationTime)
            );

        StartCoroutine(
            CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _bottomBarRect, _bottomBarAnchorIn.anchoredPosition, _bottomBarAnchorOut.anchoredPosition, _homeAnimationTime)
            );
    }

    [Header("Login animation")]
    public float _loginAnimationTime = .8f;
    public RectTransform _loginLogoRect, _loginBottomBarRect;
    public RectTransform _loginLogoAnchorIn, _loginLogoAnchorOut, _loginBottomBarAnchorIn, _loginBottomBarAnchorOut;

    public void LoginAnimationIn(Action _callback) {
        StartCoroutine(
          CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _loginLogoRect, _loginLogoAnchorOut.anchoredPosition, _loginLogoAnchorIn.anchoredPosition, _loginAnimationTime)
          );

        StartCoroutine(
            CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _loginBottomBarRect, _loginBottomBarAnchorOut.anchoredPosition, _loginBottomBarAnchorIn.anchoredPosition, _loginAnimationTime)
            );  
    }

    public void LoginAnimationOut(Action _callback)
    {
        StartCoroutine(
          CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _loginLogoRect, _loginLogoAnchorIn.anchoredPosition, _loginLogoAnchorOut.anchoredPosition, _loginAnimationTime)
          );

        StartCoroutine(
            CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _loginBottomBarRect, _loginBottomBarAnchorIn.anchoredPosition, _loginBottomBarAnchorOut.anchoredPosition, _loginAnimationTime)
            );
    }

    [Header("Profile animation")]
    public float _profileAnimationTime = .8f;
    public RectTransform _profileContainerRect;
    public RectTransform _profileAnchorIn, _profileAnchorOut;

    public void ProfileAnimationIn(Action _callback) {
        StartCoroutine(
                CCodeAnimations.LerpDisplacementPanels(() => { _callback();}, _profileContainerRect, _profileAnchorOut.anchoredPosition, _profileAnchorIn.anchoredPosition, _profileAnimationTime)
            );
    }

    public void ProfileAnimationOut(Action _callback)
    {
        StartCoroutine(
                CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _profileContainerRect, _profileAnchorIn.anchoredPosition, _profileAnchorOut.anchoredPosition, _profileAnimationTime)
            );
    }

    [Header("Overall animation")]
    public float _overallAnimationTime = 1;
    public RectTransform _overallContainerRect;
    public RectTransform _overallAnchorIn, _overallAnchorOut;

    public void OverallAnimationIn(Action _callback) {
        StartCoroutine(
                CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _overallContainerRect, _overallAnchorOut.anchoredPosition, _overallAnchorIn.anchoredPosition, _overallAnimationTime)
            );
    }

    public void OverallAnimationOut(Action _callback)
    {
        StartCoroutine(
                CCodeAnimations.LerpDisplacementPanels(() => { _callback(); }, _overallContainerRect, _overallAnchorIn.anchoredPosition, _overallAnchorOut.anchoredPosition, _overallAnimationTime)
            );
    }




    //////////////////////////////////// SIMULATE ///////////////////////
    
    //this auxiliar function is used to claim with input text and test the service
    public InputField _claimCodeInput;
    public void AuxiliarClaimCode() { 
        ClaimSimulateCode(_claimCodeInput.text);
    }

    public void ClaimSimulateCode(string _codeToClaim) {
        StartCoroutine(ClaimSimulateCodeRequest(_codeToClaim));
    }

    IEnumerator ClaimSimulateCodeRequest(string _codeToClaim)
    {
        yield return null;
        WWWForm _form = new WWWForm();
        byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLClaimSimulateCodeRequestSend(PlayerPrefs.GetString("token"), _codeToClaim));
        Dictionary<string, string> _headers = _form.headers;
        _headers["Accept"] = "application/xml";
        _headers["Content-Type"] = "application/xml";

        _loadingAdvice._childAdvice.SetActive(true);
        WWW _www = new WWW(GameConstants.BASE_URL + CLAIM_SIMULATE_CODE, _raw, _headers);
        yield return _www;

        if (_www.error != null)
        {
            _loadingAdvice._childAdvice.SetActive(false);
            Debug.Log("" + _www.error);
            
        }
        else
        {
            _loadingAdvice._childAdvice.SetActive(false);
            Dictionary<string, string> _requestResult = XMLRequestResponsesParser.XMLClaimCodeRequestMsgParse(_www.text);
            //Debug.Log(_www.text);
            if (_requestResult["Status"] == REQUEST_MESSAGE_STATUS_OK)
            {
                Debug.Log("CLAIM CODE OK!!!!!!!!!!!");
                Debug.Log(_www.text);
            }
            else
            {
                Debug.Log("CLAIM CODE ERROR");
                Debug.Log(_www.text);
            }
        }
    }

    public void CheckIfSimulateExerciseWasUnlockedForUser(int _simulateExerciseCode) {
        StartCoroutine(CheckIfSimulateExerciseWasUnlockedForUserRequest(_simulateExerciseCode));
    }

    IEnumerator CheckIfSimulateExerciseWasUnlockedForUserRequest(int _simulateExerciseCode) {
        yield return null;
        WWWForm _form = new WWWForm();
        byte[] _raw = System.Text.Encoding.UTF8.GetBytes(XMLRequestResponsesParser.XMLGetExercisesStatusResultSend(PlayerPrefs.GetString("token")));
        Dictionary<string, string> _headers = _form.headers;
        _headers["Accept"] = "application/xml";
        _headers["Content-Type"] = "application/xml";

        _loadingAdvice._childAdvice.SetActive(true);
        WWW _www = new WWW(GameConstants.BASE_URL + GET_SCENARIES_STATUS, _raw, _headers);
        yield return _www;

        if (_www.error != null)
        {
            _loadingAdvice._childAdvice.SetActive(false);
            Debug.Log("" + _www.error);

        }
        else
        {
            _loadingAdvice._childAdvice.SetActive(false);
            Dictionary<string, string> _requestResult = XMLRequestResponsesParser.XMLGetExercisesStatusRequestMsgParse(_www.text);
            //Debug.Log(_www.text);
            if (_requestResult["Status"] == REQUEST_MESSAGE_STATUS_OK)
            {
                ////////////////// PARSE RESULTS HERE
                List<int> _idsExercisesUnlocked = XMLRequestResponsesParser.XMLGetExercisesStatusResultParse(_www.text);
                if (_idsExercisesUnlocked.Find(a => a == _simulateExerciseCode) != null)
                {
                    Debug.Log("The simulate exercise is unlocked");
                }
                else {
                    Debug.Log("The simulate exercise is locked");
                }
                Debug.Log("RESUTLS RECEIVED!!!!!!!!!!!");
                Debug.Log(_www.text);
            }
            else
            {
                Debug.Log("RESULTS NOT RECEIVED, ERROR");
                Debug.Log(_www.text);
            }
        }
    }
}
