﻿using UnityEngine;
using System.Collections;

public class CPartidaLoader : MonoBehaviour {

    //public CSceneLoader _sceneLaoder;
    //public string _nextScene;

    public void NuevaPartida()
    {
        CLoadSaveData.Inst.NewGame();
        //_sceneLaoder.LoadScene(_nextScene);
    }

    public void CargarPartidaVieja()
    {
        CLoadSaveData.Inst.Load();
       // _sceneLaoder.LoadScene(_nextScene);
    }

}
