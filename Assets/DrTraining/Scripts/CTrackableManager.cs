﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


[System.Serializable]
public class ColorizeOnTrackingChange {
    public Color _onTrackingColor;
    public Color _offTrackingColor;
    public Image _img;
}
public class CTrackableManager : MonoBehaviour {
    public GameObject[] _instancesToChangeBasedOnTrackingState;
    public ColorizeOnTrackingChange[] _instancesToColorize;

	// Use this for initialization
	void Start () {
        OnAROff();
        ARDelegates.AROn += OnAROn;
        ARDelegates.AROff += OnAROff;
	}

    void Destroy() {
        ARDelegates.AROn -= OnAROn;
        ARDelegates.AROff -= OnAROff;
    }

    void OnAROn() {
        for (int i = 0; i < _instancesToChangeBasedOnTrackingState.Length; i++) {
            _instancesToChangeBasedOnTrackingState[i].SetActive(true);
        }

        for (int i = 0; i < _instancesToColorize.Length; i++)
            _instancesToColorize[i]._img.color = _instancesToColorize[i]._onTrackingColor;
    }

    void OnAROff() {
        for (int i = 0; i < _instancesToChangeBasedOnTrackingState.Length; i++)
        {
            _instancesToChangeBasedOnTrackingState[i].SetActive(false);
        }

        for (int i = 0; i < _instancesToColorize.Length; i++)
            _instancesToColorize[i]._img.color = _instancesToColorize[i]._offTrackingColor;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
