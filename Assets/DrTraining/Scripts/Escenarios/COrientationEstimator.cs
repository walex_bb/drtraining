﻿using UnityEngine;
using System.Collections;

public class COrientationEstimator : MonoBehaviour {

    // Siempre public
    public static COrientationEstimator Inst;
    public Transform _camara, _objetivo;
    public float _angMin, _angMax;
    public float _distTol;

    // Publicas solo para debuggueo
    private bool _activated;
    private float _estMin, _estMax;
    private float _angle;
    private float _mediaAbs;
    private float _estimator;
    private int _estNorm;

    // Siempre Privates
    private int _n;


    void Awake()
    {
        Inst = this;
    }

    void Start()
    {
        _estMin = _angMin * CUITimeCounter.Inst._tMin;
        _estMax = _angMax * CUITimeCounter.Inst._tMax;
        DelegatesSubscription(true);
    }

    void OnDestroy()
    {
        DelegatesSubscription(false);
    }

    void Update()
    {
        if (_activated)
        {
            ProcesarMuestra();
        }
    }

    void DelegatesSubscription(bool _subscribing)
    {
        if (_subscribing)
        {
            ARDelegates.AROn += Resume;
            ARDelegates.AROff += Pause;
        }
        else
        {
            ARDelegates.AROn -= Resume;
            ARDelegates.AROff -= Pause;
        }
    }



    public void Resume()
    {
        _activated = true;
    }

    public void Pause()
    {
        _activated = false;
    }

    public void Reset()
    {
        _estimator = 0;
        _n = 0;
        _mediaAbs = 0;
    }

    public void StartOrientation()
    {
        Reset();
        Resume();
    }

    public void ChangeObjetivo(Transform _objetivoNuevo)
    {
        _objetivo = _objetivoNuevo;
    }

    private void ProcesarMuestra()
    {
        _n = _n + 1;
        Vector3 _dist = _objetivo.position - _camara.position;
        Vector3 _normCamera = _camara.right;
        Vector3 _normObjetivo = _objetivo.right;
        _angle = Vector3.Angle(_normCamera, _normObjetivo);
        if (_dist.magnitude < _distTol && Mathf.Abs(_angle) < 60)
        {
            _mediaAbs = (Mathf.Abs(_angle) + (_n - 1) * _mediaAbs) / _n;
            _estimator = _mediaAbs * CUITimeCounter.Inst.GetTotalTime();
            //_estNorm = SmoothDescend(_estimator, _estMin, _estMax);   // Publico solo para debuggueo
        }
    }


    public int EstimatorNormalized()
    {
        return (SmoothDescend(_estimator, _estMin, _estMax));
    }


    private float Sigmoide(float x)
    {
        return 1 / (1 + Mathf.Exp(-x));
    }

    private int SmoothDescend(float x, float xmin, float xmax)
    {
        float y;
        float vel = Mathf.PI / (2 * (xmax - xmin));
        if (x < xmin)
            y = 100;
        else if (x > xmax)
            y = 0;
        else
            y = 100 * Mathf.Cos(vel * (x - xmin));
        return (int)y;
    }
}


