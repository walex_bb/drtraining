﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CXIndicadoresManager : MonoBehaviour {

	public static CXIndicadoresManager Inst;

	public Sprite _firstFilledCircle, _filledCircle;
	public Image[] _circleIndicatorSteps;
	public int _totalSteps;
	public List<string> _stepCodeTextList;
	public Text _msgText;
    public RectTransform _textBox;
    public int _currentStep;
    public enum TextAnimType { Abajo, AlCostado};
    public TextAnimType _textAnimType;




    void Awake()
	{
		Inst = this;
	}

	// Use this for initialization
	void Start () {
		_currentStep = 0;
		_msgText.text = _stepCodeTextList[_currentStep];
	}
	

	public void MarkNextStep()
	{
		//Debug.Log("marcando step" + _currentStep);
		PintarCirculo(_currentStep);
		CambiarTexto(_currentStep+1);
		_currentStep += 1;

	}


	private void PintarCirculo(int _ind)
	{
		_circleIndicatorSteps[_ind].sprite = (_currentStep == 0 ? _firstFilledCircle : _filledCircle);
	}

	private void CambiarTexto(int _ind)
	{
		if(_ind < _totalSteps)
		{
            switch (_textAnimType)
            {
                case TextAnimType.Abajo:
                    StartCoroutine(CCodeAnimations.LerpDisplacementPanels(() => {
                        _msgText.text = _stepCodeTextList[_ind];
                        StartCoroutine(CCodeAnimations.LerpDisplacementPanels(() => { }, _textBox, _textBox.anchoredPosition, _textBox.anchoredPosition - 400f * Vector2.down, 0.5f));
                    }, _textBox, _textBox.anchoredPosition, _textBox.anchoredPosition + 400f * Vector2.down, 0.5f));
                    break;
                case TextAnimType.AlCostado:
                    StartCoroutine(CCodeAnimations.LerpDisplacementPanels(() => {
                        _msgText.text = _stepCodeTextList[_ind];
                        StartCoroutine(CCodeAnimations.LerpDisplacementPanels(() => { }, _textBox, _textBox.anchoredPosition, _textBox.anchoredPosition - 400f * Vector2.right, 0.5f));
                    }, _textBox, _textBox.anchoredPosition, _textBox.anchoredPosition + 400f * Vector2.right, 0.5f));
                    break;
                default:
                    _msgText.text = _stepCodeTextList[_ind];
                    break;
            }
           
            
		}
		
	}




}
