﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CHorizonControlManager : MonoBehaviour {

    private int _maxNumberOfSteps;
    private int _currentStep;

    private bool _facing, _horizon;


    // Use this for initialization
    void Start()
    {
        DelegatesSubscription(true);
        _currentStep = 0;
        _maxNumberOfSteps = CIndicadoresManager.Inst._totalSteps;
    }

    void OnDestroy()
    {
        DelegatesSubscription(false);
    }

    // Update is called once per frame
    void Update()
    {

    }



    void DelegatesSubscription(bool _subscribing)
    {
        if (_subscribing)
        {
            ARDelegates.AROn += FirstDetectionDone;
            CHorizonChecker.HorizonOn += HorizonCorrect;
            CHorizonChecker.HorizonOff += HorizonWrong;
            CAngleChecker.AlignedOn += ImageCentered;
            CAngleChecker.AlignedOff += ImageDecentered; 
            CWorldManager.EndAnimationOn += StepEnded;
            CWorldManager.EndAllAnimationOn += EndScenario;
        }
        else
        {
            ARDelegates.AROn -= FirstDetectionDone;
            CHorizonChecker.HorizonOn -= HorizonCorrect;
            CHorizonChecker.HorizonOff -= HorizonWrong;
            CAngleChecker.AlignedOn -= ImageCentered;
            CAngleChecker.AlignedOff -= ImageDecentered;
            CWorldManager.EndAnimationOn -= StepEnded;
            CWorldManager.EndAllAnimationOn -= EndScenario;
        }
    }



    private void FirstDetectionDone()
    {
        CUITimeCounter.Inst.Activate();
        CIndicadoresManager.Inst.MarkNextStep();
        _currentStep += 1;
        ARDelegates.AROn -= FirstDetectionDone;
        
    }


    private void HorizonCorrect()
    {
        _horizon = true;
        if (_facing) ExcecuteAction();
    }

    private void HorizonWrong()
    {
        _horizon = false;
        if (_facing) StopAction();
    }

    private void ImageCentered()
    {
        _facing = true;
        if (_horizon) ExcecuteAction();

    }

    private void ImageDecentered()
    {
        _facing = false;
        if (_horizon) StopAction();
    }


    private void ExcecuteAction()
    {
        CWorldManager.Inst.PlayAnimation();
        SteadinessManager.Inst.ResumeSteadiness();
    }


    private void StopAction()
    {
        CWorldManager.Inst.StopAnimation();
        SteadinessManager.Inst.PauseSteadiness();
    }





    private void StepEnded()
    {
       
        _currentStep += 1;
        CIndicadoresManager.Inst.MarkNextStep();
    }




    private void EndScenario()
    {
        DelegatesSubscription(false);
        CUITimeCounter.Inst.Pause();
        SteadinessManager.Inst.PauseSteadiness();
        CResultsManager.Inst.MostrarResultados();
    }





}
