﻿using UnityEngine;
using System.Collections;

public class CCallEndOfAnimation : MonoBehaviour {

    public enum Tipo { Moving, Stationary };
    public Tipo _escenario;



    public void EndAnimation()
    {
        Debug.Log("Calling End of Animation");
        if (_escenario == Tipo.Moving)
        {
            CAnimationsManager.Inst.EndAnimationCall();
        }
        else if (_escenario == Tipo.Stationary)
        {
            CWorldManager.Inst.EndAnimationCall();
        }

        

    }


}
