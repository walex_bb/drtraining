﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CTrackingManager : MonoBehaviour {

    private int _maxNumberOfSteps;
    private int _currentStep;

    


    // Use this for initialization
    void Start()
    {
        DelegatesSubscription(true);
        _currentStep = 0;
        _maxNumberOfSteps = CIndicadoresManager.Inst._totalSteps;
    }

    void OnDestroy()
    {
        DelegatesSubscription(false);
    }

    // Update is called once per frame
    void Update()
    {

    }



    void DelegatesSubscription(bool _subscribing)
    {
        if (_subscribing)
        {
           // CDelegates.AddActivityStep += OnStepCompleted;
            ARDelegates.AROn += FirstDetectionDone;
            CDistanceChecker.EnLaMiraOn += ImageCentered;
            CDistanceChecker.EnLaMiraOff += ImageDecentered;
            CAnimationsManager.EndAnimationOn += StepEnded;
        }
        else
        {
           // CDelegates.AddActivityStep -= OnStepCompleted;
            ARDelegates.AROn -= FirstDetectionDone;
            CDistanceChecker.EnLaMiraOn -= ImageCentered;
            CDistanceChecker.EnLaMiraOff -= ImageDecentered;
            CAnimationsManager.EndAnimationOn -= StepEnded;
        }
    }



    private void FirstDetectionDone()
    {
        CUITimeCounter.Inst.Activate();
        CIndicadoresManager.Inst.MarkNextStep();
        _currentStep += 1;
        ARDelegates.AROn -= FirstDetectionDone;
        CTrackingEstimationManager.Inst.StartTrackingEst();
        
    }




    private void ImageCentered()
    {
        CAnimationsManager.Inst.PlayAnimation();
        SteadinessManager.Inst.ResumeSteadiness();
        CTrackingEstimationManager.Inst.PauseTrackingEst();

    }

    private void ImageDecentered()
    {
        CAnimationsManager.Inst.StopAnimation();
        SteadinessManager.Inst.PauseSteadiness();
        CTrackingEstimationManager.Inst.ResumeTrackingEst();
    }


    private void StepEnded()
    {
       
        _currentStep += 1;
        CIndicadoresManager.Inst.MarkNextStep();
        if (_currentStep == _maxNumberOfSteps)
        {
            EndScenario();
        }
    }


    private void EndScenario()
    {
        DelegatesSubscription(false);
        CUITimeCounter.Inst.Pause();
        SteadinessManager.Inst.PauseSteadiness();
        CTrackingEstimationManager.Inst.PauseTrackingEst();
        CResultsManager.Inst.MostrarResultados();
    }





}
