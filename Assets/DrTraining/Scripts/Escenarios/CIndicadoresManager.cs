﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CIndicadoresManager : MonoBehaviour {

	public static CIndicadoresManager Inst;

	public Sprite _firstFilledCircle, _filledCircle;
	public Image[] _circleIndicatorSteps;
	public int _totalSteps;
	public List<string> _stepCodeTextList;
	public Text _msgText;
	public int _currentStep;

	


	void Awake()
	{
		Inst = this;
	}

	// Use this for initialization
	void Start () {
		_currentStep = 0;
		_msgText.text = _stepCodeTextList[_currentStep];
	}
	
	// Update is called once per frame
	void Update () {
	
	}



	public void MarkNextStep()
	{
		//Debug.Log("marcando step" + _currentStep);
		PintarCirculo(_currentStep);
		CambiarTexto(_currentStep+1);
		_currentStep += 1;

	}


	private void PintarCirculo(int _ind)
	{
		_circleIndicatorSteps[_ind].sprite = (_currentStep == 0 ? _firstFilledCircle : _filledCircle);
	}

	private void CambiarTexto(int _ind)
	{
		if(_ind < _totalSteps)
		{
			_msgText.text = _stepCodeTextList[_ind];
		}
		
	}




}
