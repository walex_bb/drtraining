﻿using UnityEngine;
using System.Collections;

public class CDistanceChecker : MonoBehaviour {

    public Transform _objeto1, _objeto2;
    public float _toleranciaDistacia = 2.0f;
    public float _distanciaFija = 7.0f;
    public bool _isNear;
    private bool _isNearPrev;
    public float _dist; // Queda publica para poder calibrar la distancia fija y la tolerancia

    // Delegates para cuando cambia de estado
    public delegate void EnLaMiraDelegate();
    public static event EnLaMiraDelegate EnLaMiraOn;
    public delegate void FueraDeMiraDelegate();
    public static event FueraDeMiraDelegate EnLaMiraOff;
    
    void Update()
    {
        _dist = Vector3.Distance(_objeto1.position, _objeto2.position);
        _isNear = (_dist <= (_distanciaFija + _toleranciaDistacia)) & (_dist >= (_distanciaFija - _toleranciaDistacia));
        if(!_isNearPrev & _isNear & (EnLaMiraOn != null))
        {
            EnLaMiraOn();
        }
        else if(_isNearPrev & !_isNear & (EnLaMiraOff != null))
        {
            EnLaMiraOff();
        }
        _isNearPrev = _isNear;
    }
}
