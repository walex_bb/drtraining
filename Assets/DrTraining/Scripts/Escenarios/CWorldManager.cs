﻿using UnityEngine;
using System.Collections;
using System;

public class CWorldManager : MonoBehaviour {

    public Animator _anim;
    public GameObject _GOtoColorize;
    public float _animSpeed = 0.1f;
    public int _cantSteps;
    private int _currentStep;
    public Transform[] _StepsPositions;
    public GameObject _objectToMove;
    public enum Tipo { Moving, Stationary };
    public Tipo _escenario;


    public static CWorldManager Inst;
    public delegate void EndAnimationDelegate();
    public static event EndAnimationDelegate EndAnimationOn;
    public delegate void EndAllAnimationDelegate();
    public static event EndAllAnimationDelegate EndAllAnimationOn; 

    void Awake()
    {
        Inst = this;
    }

    void Start()
    {
        _currentStep = 0;
        if(_escenario == Tipo.Moving)
        {
            _anim.speed = 0f;
        }
    }

    public void PlayAnimation()
    {
        CAudioManager.Inst.PlaySFX("Found");
        StartCoroutine("SoundLoop");
        switch (_escenario)
        {
            case Tipo.Moving:
                _anim.speed = _animSpeed;
                _GOtoColorize.GetComponent<MeshRenderer>().material.color = Color.green;
                break;
            case Tipo.Stationary:
                _anim.SetFloat("Velocity", _animSpeed);
                _anim.SetBool("play_anim", true);
                break;
            default:
                Debug.Log("Error en seteo tipo escenario");
                break;
        }              
    }

    public void StopAnimation()
    {
        CAudioManager.Inst.PlaySFX("Lost");
        StopCoroutine("SoundLoop");
        switch (_escenario)
        {
            case Tipo.Moving:
                _anim.speed = 0;
                _GOtoColorize.GetComponent<MeshRenderer>().material.color = Color.red;
                break;
            case Tipo.Stationary:
                _anim.SetFloat("Velocity", -_animSpeed);
                _anim.SetBool("play_anim", false);
                break;
            default:
                Debug.Log("Error en seteo tipo escenario");
                break;
        }
    }

    IEnumerator SoundLoop()
    {
        WaitForSeconds _soundDelay = new WaitForSeconds(0.3f);
        yield return _soundDelay;
        _soundDelay = new WaitForSeconds(1);
        while (true)
        {
            CAudioManager.Inst.PlaySFX("Loop");
            yield return _soundDelay;
        }
    }

    public void EndAnimationCall()
    {
        CAudioManager.Inst.PlaySFX("Lost");
        StopCoroutine("SoundLoop");
        if (EndAnimationOn != null)
        {
            EndAnimationOn();
        }
        NextStep();
    }

    public void NextStep()
    {
        _objectToMove.SetActive(false);
        _currentStep += 1;
        if(_currentStep == _cantSteps)
        {
            if (EndAllAnimationOn != null)
            {
                EndAllAnimationOn();
            }
        }
        else
        {
            Debug.Log("Yendo al siguiente paso");
            _objectToMove.transform.position = _StepsPositions[_currentStep].position;
            _objectToMove.transform.rotation = _StepsPositions[_currentStep].rotation;
            _anim.SetTrigger("next");
            _objectToMove.SetActive(true);
            
            //ResetAnimation
        }
        
    }



    public IEnumerator OscilateSizeAndDisapeare(Action _callback, Transform _objeto, Vector3 _minScale, Vector3 _maxScale, float _period, float _numberOfPeriods)
    {
        int _n = 0;
        while (_n < _numberOfPeriods)
        {
            yield return StartCoroutine(CCodeAnimations.Change3DSize(() => { }, _objeto, _objeto.localScale, _maxScale, _period / 2.0f));
            yield return StartCoroutine(CCodeAnimations.Change3DSize(() => { }, _objeto, _objeto.localScale, _minScale, _period / 2.0f));
            _n += 1;
        }
        StartCoroutine(CCodeAnimations.Change3DSize(() => { }, _objeto, _objeto.localScale, Vector3.zero, _period / 2.0f));
        _callback();
        yield break;
    }





}
