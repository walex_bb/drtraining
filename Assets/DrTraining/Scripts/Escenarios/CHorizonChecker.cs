﻿using UnityEngine;
using System.Collections;

public class CHorizonChecker : MonoBehaviour {

    public Transform _objeto1, _objeto2;
    public float _toleranciaDistacia = 30f;
    public bool _isAlined;
    private bool _isAlinedPrev;
    public float _angle; // Queda publica para poder calibrar la distancia fija y la tolerancia
    private bool _active;
    public bool _directAngle;

    // Delegates para cuando cambia de estado
    public delegate void HorizonDelegate();
    public static event HorizonDelegate HorizonOn;
    public delegate void NotHorizonDelegate();
    public static event NotHorizonDelegate HorizonOff;


    void Start()
    {
        CDistanceChecker.EnLaMiraOn += Activate;
        CDistanceChecker.EnLaMiraOff += Deactivate;
    }

    void OnDestroy()
    {
        CDistanceChecker.EnLaMiraOn -= Activate;
        CDistanceChecker.EnLaMiraOff -= Deactivate;
    }


    void Update()
    {
        if (_active)
        {

            _angle = _directAngle ? Vector3.Angle(_objeto1.up, _objeto2.up) : Vector3.Angle(_objeto1.up, -_objeto2.up);
            _isAlined = (Mathf.Abs(_angle) <= _toleranciaDistacia);
            if (!_isAlinedPrev & _isAlined & (HorizonOn != null))
            {
                HorizonOn();
            }
            else if (_isAlinedPrev & !_isAlined & (HorizonOff != null))
            {
                HorizonOff();
            }
            _isAlinedPrev = _isAlined;
        }

    }


    public void Activate()
    {
        _active = true;
    }
    public void Deactivate()
    {
        if (_isAlinedPrev & (HorizonOff != null))
        {
            HorizonOff();
        }
        _active = false;
        _isAlinedPrev = false;
        _isAlined = false;

    }
}
