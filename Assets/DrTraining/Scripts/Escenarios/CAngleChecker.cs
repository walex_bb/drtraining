﻿using UnityEngine;
using System.Collections;

public class CAngleChecker : MonoBehaviour {

    public Transform _objeto1, _objeto2;
    public float _toleranciaDistacia = 30f;
    public bool _isAlined;
    private bool _isAlinedPrev;
    public float _angle; // Queda publica para poder calibrar la distancia fija y la tolerancia
    private bool _active;
    public bool _directAngle;

    // Delegates para cuando cambia de estado
    public delegate void AlignedDelegate();
    public static event AlignedDelegate AlignedOn;
    public delegate void NotAlignedDelegate();
    public static event NotAlignedDelegate AlignedOff;


    void Start()
    {
        CDistanceChecker.EnLaMiraOn += Activate;
        CDistanceChecker.EnLaMiraOff += Deactivate;
    }

    void OnDestroy()
    {
        CDistanceChecker.EnLaMiraOn -= Activate;
        CDistanceChecker.EnLaMiraOff -= Deactivate;
    }


    void Update()
    {
        if (_active)
        {
            
            _angle = _directAngle?  Vector3.Angle(_objeto1.forward, _objeto2.forward):Vector3.Angle(_objeto1.forward, -_objeto2.forward);
            _isAlined = (Mathf.Abs(_angle) <= _toleranciaDistacia);
            if (!_isAlinedPrev & _isAlined & (AlignedOn != null))
            {
                AlignedOn();
            }
            else if (_isAlinedPrev & !_isAlined & (AlignedOff != null))
            {
                AlignedOff();
            }
            _isAlinedPrev = _isAlined;
        }
       
    }


    public void Activate()
    {
        _active = true;
    }
    public void Deactivate()
    {
        if (_isAlinedPrev & (AlignedOff != null))
        {
            AlignedOff();
        }
        _active = false;
        _isAlinedPrev = false;
        _isAlined = false;
        
    }


}
