﻿using UnityEngine;
using System.Collections;

public class CAnimationsManager : MonoBehaviour {

    public Animator _anim;
    public GameObject _GOtoColorize;
    public float _animSpeed = 0.1f;
    string _actualColor = "dark";
    float _elapsedTime;
    [SerializeField]
    float _colorDurationTime;
    bool _loop = true;
    MeshRenderer _colorBola;

    public static CAnimationsManager Inst;
    public delegate void EndAnimationDelegate();
    public static event EndAnimationDelegate EndAnimationOn;

    void Awake()
    {
        Inst = this;
    }

    void Start()
    {
        _anim.speed = 0;
        _colorBola = _GOtoColorize.GetComponent<MeshRenderer>();
    }

    public void PlayAnimation()
    {
        _anim.speed = _animSpeed;
        StartCoroutine("ChangeColor");
        CAudioManager.Inst.PlaySFX("Found");
        StartCoroutine("SoundLoop");
        //_GOtoColorize.GetComponent<MeshRenderer>().material.color = Color.green;
    }

    public void StopAnimation()
    {
        _anim.speed = 0;
        StopCoroutine("ChangeColor");
        CAudioManager.Inst.PlaySFX("Lost");
        StopCoroutine("SoundLoop");
        _GOtoColorize.GetComponent<MeshRenderer>().material.color = Color.red;
    }

    IEnumerator SoundLoop()
    {
        WaitForSeconds _soundDelay = new WaitForSeconds(0.3f);
        yield return _soundDelay;
        _soundDelay = new WaitForSeconds(1);
        while (true)
        {
            CAudioManager.Inst.PlaySFX("Loop");
            yield return _soundDelay;
        }
    }

    IEnumerator ChangeColor()
    {
        while (_loop)
        {
            if (_actualColor == "dark")
            {
                while (_elapsedTime < _colorDurationTime)
                {
                    _elapsedTime += Time.deltaTime;
                    _colorBola.material.color = Color.Lerp(new Color32(0x05, 0x92, 0x1D, 0xFF), new Color32(0x1D, 0xFF, 0x00, 0xFF), _elapsedTime / _colorDurationTime); //Mathf.PingPong(Time.time, 1));
                    yield return null;
                }
                _elapsedTime = 0;
                _actualColor = "light";
            }
            else
            {
                while (_elapsedTime < _colorDurationTime)
                {
                    _elapsedTime += Time.deltaTime;
                    _colorBola.material.color = Color.Lerp(new Color32(0x1D, 0xFF, 0x00, 0xFF), new Color32(0x05, 0x92, 0x1D, 0xFF), _elapsedTime / _colorDurationTime); //Mathf.PingPong(Time.time, 1));
                    yield return null;
                }
                _elapsedTime = 0;
                _actualColor = "dark";
            }
            yield return null;
        }
        yield return null;
    }

    public void EndAnimationCall()
    {
        CAudioManager.Inst.PlaySFX("Lost");
        StopCoroutine("SoundLoop");
        if (EndAnimationOn != null)
        {
            EndAnimationOn();
        }
    }


}
