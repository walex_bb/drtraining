﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CImageOrientationManager : MonoBehaviour {

    private int _maxNumberOfSteps;
    private int _currentStep;

    


    // Use this for initialization
    void Start()
    {
        DelegatesSubscription(true);
        _currentStep = 0;
        _maxNumberOfSteps = CIndicadoresManager.Inst._totalSteps;
    }

    void OnDestroy()
    {
        DelegatesSubscription(false);
    }

    // Update is called once per frame
    void Update()
    {

    }



    void DelegatesSubscription(bool _subscribing)
    {
        if (_subscribing)
        {
            ARDelegates.AROn += FirstDetectionDone;
            CAngleChecker.AlignedOn += ImageCentered;
            CAngleChecker.AlignedOff += ImageDecentered;
            CWorldManager.EndAnimationOn += StepEnded;
            CWorldManager.EndAllAnimationOn += EndScenario;
        }
        else
        {
            ARDelegates.AROn -= FirstDetectionDone;
            CAngleChecker.AlignedOn -= ImageCentered;
            CAngleChecker.AlignedOff -= ImageDecentered;
            CWorldManager.EndAnimationOn -= StepEnded;
            CWorldManager.EndAllAnimationOn -= EndScenario;
        }
    }



    private void FirstDetectionDone()
    {
        CUITimeCounter.Inst.Activate();
        CIndicadoresManager.Inst.MarkNextStep();
        _currentStep += 1;
        ARDelegates.AROn -= FirstDetectionDone;
        CTrackingEstimationManager.Inst.StartTrackingEst();
        
    }




    private void ImageCentered()
    {
        CWorldManager.Inst.PlayAnimation();
        SteadinessManager.Inst.ResumeSteadiness();
       // COrientationEstimator.Inst.Resume();
        CTrackingEstimationManager.Inst.PauseTrackingEst();

    }

    private void ImageDecentered()
    {
        CWorldManager.Inst.StopAnimation();
        SteadinessManager.Inst.PauseSteadiness();
    //    COrientationEstimator.Inst.Pause();
        CTrackingEstimationManager.Inst.ResumeTrackingEst();
    }


    private void StepEnded()
    {
       
        _currentStep += 1;
        CIndicadoresManager.Inst.MarkNextStep();
    }




    private void EndScenario()
    {
        DelegatesSubscription(false);
        CUITimeCounter.Inst.Pause();
        SteadinessManager.Inst.PauseSteadiness();
    //    COrientationEstimator.Inst.Pause();
        CTrackingEstimationManager.Inst.PauseTrackingEst();
        CResultsManager.Inst.MostrarResultados();
    }





}
