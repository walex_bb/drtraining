﻿using UnityEngine;
using System.Collections;

public enum ETRACK_MODE { 
	NOONE,
	POSITION,
	ROTATION,
	BOTH
}



public class CTrackImageTarget : MonoBehaviour {
	public Transform _imageTarget;
	public ETRACK_MODE _trackMode;
	public float _lerpVel = 10f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (_imageTarget != null)
		{
			if (_trackMode == ETRACK_MODE.BOTH)
			{
				//transform.position = _imageTarget.transform.position;
				transform.position = Vector3.Lerp(transform.position, _imageTarget.transform.position, _lerpVel* Time.deltaTime); 
				transform.rotation = _imageTarget.transform.rotation;
			}
			else { 
				if (_trackMode == ETRACK_MODE.POSITION)
					transform.position = _imageTarget.transform.position;
				else if (_trackMode == ETRACK_MODE.ROTATION)
					transform.rotation = _imageTarget.transform.rotation;
			}
		}
	}


	private Vector3 PasabajosPos(Vector3 _targetPos)
	{
		Vector3 _instrumentPos = Vector3.zero;
		_instrumentPos = Vector3.Lerp(transform.position, _imageTarget.transform.position, Time.deltaTime);

		return _instrumentPos;
	}



}
