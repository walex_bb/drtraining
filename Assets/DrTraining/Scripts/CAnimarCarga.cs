﻿using UnityEngine;
using System.Collections;

public class CAnimarCarga : MonoBehaviour {


    //public bool _activated;
    public GameObject[] _circulosCarga;
    private WaitForSeconds _timerCarga = new WaitForSeconds(0.125f);
    //private float _timer;
    //private int _ind;


    void OnEnable()
    {
        //_activated =true;        
        
    }

    void OnDisable()
    {
        StopAllCoroutines();
        //_activated = false;
    }

    void Start () {
        //_circulosCarga[0].SetActive(true);
        for (int _j = 0; _j < _circulosCarga.Length; _j++)
        {
            _circulosCarga[_j].SetActive(false);
        }
        StartCoroutine(AnimarCarga());
        //_timer = _timerCarga;
        //_ind = 0;
        //_circulosCarga[_ind].SetActive(true);
    }
	
	void Update () {
        //if (_activated)
        //{
        //    if (_timer < 0)
        //    {
        //        _timer = _timerCarga;
        //        _circulosCarga[_ind].SetActive(false);
        //        _ind += 1;
        //        if (_ind == _circulosCarga.Length) _ind = 0;
        //        _circulosCarga[_ind].SetActive(true);
        //    }
        //}
	}



    private IEnumerator AnimarCarga()
    {
        int _i = 0;
        while (true)
        {
            _circulosCarga[_i].SetActive(false);
            yield return new WaitForEndOfFrame();
            _i++;
            _i %= _circulosCarga.Length;
            _circulosCarga[_i].SetActive(true);
            yield return new WaitForEndOfFrame();
            yield return _timerCarga;
        }
    }




}
