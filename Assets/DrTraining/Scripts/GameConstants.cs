﻿using UnityEngine;
using System.Collections;

public enum EPreviewPositions
{
    ON_SCREEN_QUARTER_CORNER,
    ON_SCREEN_FULL_VISIBLE
}

public enum EButtonSpriteEye
{
    REAL_CAM,
    EYE_OPEN,
    EYE_CLOSED
}

public class GameConstants {
    public static readonly float BASE_VISIBLE_TIME = 10.0f;
    public static readonly string LOCAL_REQUEST_CODE = "local";
    //public static readonly string BASE_URL = "https://backend.doctor-training.com:8443/rest/webservices/";
    public static readonly string BASE_URL = "https://surme-training.azurewebsites.net/rest/webservices/";
    public static readonly string BASE_URL_LOGIN_PORT = "https://backend.doctor-training.com:8443/rest/webservices/";
}
