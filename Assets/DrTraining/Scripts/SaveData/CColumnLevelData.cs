﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CColumnLevelData {
    public int _columnNumber;
    public int _actualNote;
    public int _noteDuration;

    public CColumnLevelData(int columnNumber, int actualNote, int noteDuration) {
        _columnNumber = columnNumber;
        _actualNote = actualNote;
        _noteDuration = noteDuration;
    }

    public CColumnLevelData() {
        _columnNumber = 0;
        _actualNote = -1;
        _noteDuration = 0;
    }
}
