﻿using System.Xml;
using System.Xml.Serialization;


[XmlRoot]
public class Player
{
    [XmlElement]
    public int Level { get; set; }

    [XmlElement]
    public int Health { get; set; }
}