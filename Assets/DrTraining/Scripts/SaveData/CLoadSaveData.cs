﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class CLoadSaveData:MonoBehaviour
{
    public static CLoadSaveData Inst;
    public  CData _data;
    private string _path;
    public CDataContainer dataCollection;

    // Singleton
    void Awake()
    {
        Inst = this;
        _path = createPath();
    }

    // Nueva partida. Borro Data y guardo nuevo archivo
    public void NewGame()
    {
        dataCollection = new CDataContainer();
        _data = dataCollection.Datas[0];
        Save();
    }


    // Guardado de lo que haya en _data
    public void Save()
    {
        dataCollection.Save(_path);
    }

    // Levantamos lo que haya en path
    public void Load()
    {
        dataCollection = CDataContainer.Load(_path);
        _data = dataCollection.Datas[0];
    }


    // Following method is used to retrive the relative path as device platform
    private string createPath()
    {
#if UNITY_EDITOR
        return Application.dataPath + "/XML/" + "ProgressData.xml";
#elif UNITY_ANDROID
        return Application.persistentDataPath + "/ProgressData.xml";
#elif UNITY_IPHONE
        return Application.persistentDataPath + "/ProgressData.xml";
#else
        return Application.dataPath +  "/ProgressData";
#endif
    }




}
