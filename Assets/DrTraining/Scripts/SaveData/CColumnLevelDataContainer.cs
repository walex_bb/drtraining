﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("ColumnLevelCollection")]
public class CColumnLevelDataContainer {

    [XmlArray("Columns")]
    [XmlArrayItem("Column")]
    public List<CColumnLevelData> _columns = new List<CColumnLevelData>();





}
