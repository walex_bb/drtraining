﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;


public class CData
{
    [XmlAttribute("name")]
    public string Name;
    public bool _done;

    public CData()
    {
        Name = "La nada mismo";
        _done = false;
    }

}
