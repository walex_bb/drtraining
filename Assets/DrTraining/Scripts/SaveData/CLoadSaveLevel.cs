﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class CLoadSaveLevel:MonoBehaviour
{
    public InputField _levelName;
    // este metodo se llama cuando tocas el boton de Load, te dejo la estructura CColumnLevelDataContainer cargada y la tenes que pasar a tu array
    // y generar las columnas por codigo. Podes hacerlo aca mismo si te resulta mas facil. Max
    public void LoadLevelData() {
        CColumnLevelDataContainer _levelData = LoadFromText(_levelName.text);

        //para pasarlo a un array tenes un metodo en List que se llama ToArray()

        //Para debuggear
        if (_levelData != null)
        {
            if (_levelData._columns == null || _levelData._columns.Count == 0)
            {
                Debug.Log("Aun no hay columnas en este nivel");
                return;
            }
            for (int i = 0; i < _levelData._columns.Count; i++)
                Debug.Log("Col Nro " + i + " Note " + _levelData._columns[i]._actualNote + " Row number " + _levelData._columns[i]._columnNumber + " Duration " + _levelData._columns[i]._noteDuration);
        
        }
        else
            Debug.Log("El archivo no existe");
    }

    // este otro metodo se llama cuando tocas el boton de Save, te va a guardar el archivo en la carpeta Resources/Levels/ del proyecto.
    // lo hacemos asi para que los xml queden en el build puesto que a Ceibal solo le vamos a mandar un archivo y no varios. Max
    public void SaveLevelData() {
        //Lo que tendrias que hacer aca es a _dataContainer rellenarlo con los datos de tus columnas. Podes hacerte una referencia luego a las mismas en este script e irlas cargando.
        CColumnLevelDataContainer _dataContainer = new CColumnLevelDataContainer();
        _dataContainer._columns = new List<CColumnLevelData>();
        _dataContainer._columns.Add(new CColumnLevelData(1, 0, 1)); // estas son columnas de ejemplo, los valores que puse son re truchos, despues pone valores correctos vos Seba.
        _dataContainer._columns.Add(new CColumnLevelData(2, 1, 3));
        Save(_levelName.text, _dataContainer);
    }

    public static void Save(string path, CColumnLevelDataContainer _data)
    {
        var serializer = new XmlSerializer(typeof(CColumnLevelDataContainer));
        using (var stream = new FileStream("Assets/Resources/Levels/" + path + ".xml", FileMode.Create))
        {
            serializer.Serialize(stream, _data);
        }
    }

    public static CColumnLevelDataContainer Load(string _path)
    {
        var serializer = new XmlSerializer(typeof(CColumnLevelDataContainer));
        using (var stream = new FileStream("Levels/" + _path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as CColumnLevelDataContainer;
        }
    }

    public static CColumnLevelDataContainer LoadFromText(string _fileName)
    {

        var serializer = new XmlSerializer(typeof(CColumnLevelDataContainer));
        TextAsset textAsset = (TextAsset)Resources.Load("Levels/" + _fileName);

        CColumnLevelDataContainer _columnLevel = serializer.Deserialize(new StringReader(textAsset.text)) as CColumnLevelDataContainer;
        return _columnLevel;
    }
}
