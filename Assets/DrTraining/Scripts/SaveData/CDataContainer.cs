﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;


[XmlRoot("DataCollection")]
public class CDataContainer
{
    [XmlArray("Datas"), XmlArrayItem("Data")]
    public CData[] Datas;

    public CDataContainer()
    {
        Datas = new CData[1];
    }


    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(CDataContainer));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }

    public static CDataContainer Load(string path)
    {
        var serializer = new XmlSerializer(typeof(CDataContainer));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as CDataContainer;
        }
    }

    //Loads the xml directly from the given string. Useful in combination with www.text.
    public static CDataContainer LoadFromText(string text)
    {
        var serializer = new XmlSerializer(typeof(CDataContainer));
        return serializer.Deserialize(new StringReader(text)) as CDataContainer;
    }
}