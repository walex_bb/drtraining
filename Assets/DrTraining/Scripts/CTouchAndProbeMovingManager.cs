﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CTouchAndProbeMovingManager : MonoBehaviour
{
    public int _totalSteps;
    public int _currentStep = -1;
    public int[] _numericCodesForSteps;
    public List<SStepCodeText> _stepCodeTextList;
    public Text _msgText;
    [Header("Circle indicator in right down corner of the screen")]
    public Image[] _circleIndicatorSteps;
    public Sprite _filledCircle;
    [Tooltip("Special one with green color")]
    public Sprite _firstFilledCircle;
    [SerializeField]
    CToucher _toucher;


    // Use this for initialization
    void Start()
    {
        ARDelegates.AROn += OnAROn;
        ARDelegates.AROff += OnAROff;
        DRDelegates.TouchObject += MoveForward;        
        SteadinessManager.Inst.ResetSteadiness();
        GoToNextStep();
    }

    void OnDestroy()
    {
        ARDelegates.AROn -= OnAROn;
        ARDelegates.AROff -= OnAROff;
        DRDelegates.TouchObject -= MoveForward;
    }

    void OnAROn()
    {
        if (_currentStep == 0)
            GoToNextStep();
    }

    void OnAROff()
    {

    }


    void MoveForward()
    {
        if (_currentStep == 0)
            GoToNextStep();
    }

    // Se usa en el moving target por ahora, ojo
    public void GoToStep(int _stepNumber)
    {
        _currentStep = _stepNumber;
        DoStep();
    }

    public void GoToNextStep()
    {
        _currentStep += 1;
        // Activar Timer luego de la detección del marcador
        if (_currentStep == 1)
        {
            CUITimeCounter.Inst.Activate();
        }

        if (_currentStep > 0)
        {
            _circleIndicatorSteps[_currentStep - 1].sprite = (_currentStep - 1 == 0 ? _firstFilledCircle : _filledCircle);
        }
        if (_currentStep == _totalSteps)
        {
            StartCoroutine(ShowCongratulation());
        }
        else
        {
            DoStep();
        }
    }

    void DoStep()
    {
        _msgText.text = _stepCodeTextList.Find(a => a._numericCode == _numericCodesForSteps[_currentStep])._textToWrite;
    }

    IEnumerator ShowCongratulation()
    {
        CAudioManager.Inst.PlaySFX("Lost");
        _toucher.StopCoroutine("SoundLoop");
        yield return new WaitForSeconds(.5f);
        CResultsManager.Inst.MostrarResultados();
    }
  

}
