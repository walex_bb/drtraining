﻿using UnityEngine;
using System.Collections;

public class ARDelegates : MonoBehaviour {

    public delegate void ARTrackingDelegates();
    public static event ARTrackingDelegates AROn;
    public static event ARTrackingDelegates AROff;

    public static void OnAROn(){
        if (AROn != null)
            AROn();
    }

    public static void OnAROff(){
        if (AROff != null)
            AROff();
    }

    public static event ARTrackingDelegates PreNotificationAROn;
    public static event ARTrackingDelegates PreNotificationAROff;

    public static void OnPreNotificationAROn()
    {
        if (PreNotificationAROn != null)
            PreNotificationAROn();
    }

    public static void OnPreNotificationAROff()
    {
        if (PreNotificationAROff != null)
            PreNotificationAROff();
    }
}
