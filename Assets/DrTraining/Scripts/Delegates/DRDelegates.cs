﻿using UnityEngine;
using System.Collections;

public class DRDelegates : MonoBehaviour {

	public delegate void NoParametersDelegates();
    public static event NoParametersDelegates TouchObject;

    public static void OnTouchObject(){
	    if (TouchObject != null)
		    TouchObject();
    }

    public delegate void SceneDelegate(string _levelName);
    public static event SceneDelegate LoadLevelMsg;

    public static void OnLoadLevelMsg(string _levelName){
        if (LoadLevelMsg != null)
        {
            LoadLevelMsg(_levelName);
        }
    }
}
