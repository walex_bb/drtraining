﻿using UnityEngine;
using System.Collections;

public class CCheckSphereContained : MonoBehaviour {

	public Transform _objeto1, _objeto2;
	public float _toleranciaDistacia = 2.0f;
	public float _distanciaFija = 7.0f;
	public bool _isSmallSphereContainedInBigOne;
	public float _dist;

	public SphereCollider _bigSphere;
	public Transform _smallSpherePointReference;

	void Update () {
		_dist = Vector3.Distance(_objeto1.position, _objeto2.position);
		if (!_isSmallSphereContainedInBigOne)
		{
			if ((_dist <= (_distanciaFija + _toleranciaDistacia)) & (_dist >= (_distanciaFija - _toleranciaDistacia)))
			{
				_isSmallSphereContainedInBigOne = true;
				PlaySounds();
			}
		}
		else
		{
			if (!(_dist <= (_distanciaFija + _toleranciaDistacia)) & !(_dist <= (_distanciaFija - _toleranciaDistacia)))
			{
				_isSmallSphereContainedInBigOne = false;
				PlaySounds();
			}

		}
		_isSmallSphereContainedInBigOne = (_dist <= (_distanciaFija+_toleranciaDistacia)) & (_dist >= (_distanciaFija - _toleranciaDistacia));
	}

	public void PlaySounds()
	{
		if (_isSmallSphereContainedInBigOne)
		{
			CAudioManager.Inst.PlaySFX("Found");
			StartCoroutine("SoundLoop");
		}
		else
		{
			CAudioManager.Inst.PlaySFX("Lost");
			StopCoroutine("SoundLoop");
		}
	}

	IEnumerator SoundLoop()
	{
		WaitForSeconds _soundDelay = new WaitForSeconds(0.3f);
		yield return _soundDelay;
		_soundDelay = new WaitForSeconds(1);
		while (true)
		{
			CAudioManager.Inst.PlaySFX("Loop");
			yield return _soundDelay;
		}
	}
}
