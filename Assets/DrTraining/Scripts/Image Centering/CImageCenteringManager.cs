﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CImageCenteringManager : MonoBehaviour {
    private CCheckSphereContained _checkSphereContained;
    [Range(1, 10)]
    public float _maxCenteringTime = 5;
    public float _reescalatingSpeed = 1.0f;
    public Sprite[] _spritePerStep;
    public SpriteRenderer _visualIndicator;
    private Vector3 _oldLocalScale;
    
    // [] is used to find the positions and List is used to delete the used positions and avoid place repetition on respawn
    private GameObject[] _spawnPointObjects;
    private List<Transform> _spawnPoints;

    private Animator _catchedAnimator;
    public string _animationCode;
    [Range(0, 1)]
    public float _fordwardAnimatorUpperLimit = 0.99f;
    [Range(0, 1)]
    public float _backwardAnimatorDownwerLimit = 0.1f;

    [HideInInspector]
    public bool _animBolaOn;

    // Use this for initialization
    void Start () {
        _checkSphereContained = GetComponent<CCheckSphereContained>();
        ConfigureSpawnPoints();
        _oldLocalScale = _checkSphereContained._bigSphere.gameObject.transform.localScale;
        DelegatesSubscription(true);
        _catchedAnimator = _checkSphereContained._bigSphere.GetComponent<Animator>();
        SteadinessManager.Inst.ResetSteadiness();
    }

    void ConfigureSpawnPoints() {
        _spawnPointObjects = GameObject.FindGameObjectsWithTag("SpawnPoint");
        _spawnPoints = new List<Transform>();

        //Shuffle the array
        for (int i = 0; i < _spawnPointObjects.Length; i++) {
            int _index = Random.Range(0, _spawnPointObjects.Length);
            GameObject _tempGO = _spawnPointObjects[_index];
            _spawnPointObjects[_index] = _spawnPointObjects[i];
            _spawnPointObjects[i] = _tempGO;
        }

        //From array to List
        for (int i = 0; i < _spawnPointObjects.Length; i++)
            _spawnPoints.Add(_spawnPointObjects[i].transform);

    }

    void OnDestroy() {
        DelegatesSubscription(false);
    }

    void DelegatesSubscription(bool _isSubscribe) {
        if (_isSubscribe) {
            CDelegates.AnimationStart += OnAnimationStart;
            CDelegates.AnimationEnd += OnAnimationEnd;
        }
        else{
            CDelegates.AnimationStart -= OnAnimationStart;
            CDelegates.AnimationEnd -= OnAnimationEnd;
        }
    }

    void OnAnimationStart(string _code) {
        if (_code == _animationCode) { 
        
        }
    }

    void OnAnimationEnd(string _code) {
        if (_code == _animationCode)
        {
            _animBolaOn = false;
            SteadinessManager.Inst.PauseSteadiness();
            _checkSphereContained._bigSphere.gameObject.transform.parent.transform.position = _spawnPoints[_spawnPoints.Count - 1].position;
            _spawnPoints.RemoveAt(_spawnPoints.Count - 1);
            _checkSphereContained._bigSphere.transform.localScale = _oldLocalScale;
            _visualIndicator.sprite = _spritePerStep[0];

            //Notify the system to trigger the UI and logic changes
            CDelegates.OnAddActivityStep();
        }
    }

	void Update () {
        float _animatorTime = GetAnimationTime();

        if (_animBolaOn)
        {
            if (_checkSphereContained._isSmallSphereContainedInBigOne) {
                //ejecutar la animacion normalmente, velocidad = 1
                _catchedAnimator.SetFloat("Velocity", 1);
                SteadinessManager.Inst.ResumeSteadiness(); 
                //si tiempo de animacion es 1 entonces animBolaOn = false
                if (_animatorTime >= _fordwardAnimatorUpperLimit) {
                    _animBolaOn = false;
                    _catchedAnimator.SetBool("play_anim", false);
                }
            }
            else {
                //ejecutar la animacion normalmente, velocidad = -1.
                _catchedAnimator.SetFloat("Velocity", -1);
                SteadinessManager.Inst.PauseSteadiness();
                //si tiempo de animacion es 0 entonces animBolaOn = false
                if (_animatorTime <= _backwardAnimatorDownwerLimit)
                {
                    _animBolaOn = false;
                    _catchedAnimator.SetBool("play_anim", false);
                }
            }
        }
        else {
            if (_checkSphereContained._isSmallSphereContainedInBigOne)
            {
                _animBolaOn = true;
                SteadinessManager.Inst.ResumeSteadiness();
                _catchedAnimator.SetFloat("Velocity", 1);
                _catchedAnimator.SetBool("play_anim", true);
            }
            else
            {
                _animBolaOn = false;
                _catchedAnimator.SetFloat("Velocity", 1);
                _catchedAnimator.SetBool("play_anim", false);
            }
        }
    }

    //Auxiliar functions
    private float GetAnimationTime() {
        AnimatorStateInfo currentState = _catchedAnimator.GetCurrentAnimatorStateInfo(0);
        return currentState.normalizedTime;
    }

}
