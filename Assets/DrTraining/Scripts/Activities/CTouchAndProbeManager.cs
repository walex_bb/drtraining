﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CTouchAndProbeManager : MonoBehaviour {
	public int _totalSteps;
	private int _currentStep = -1;
	public int [] _numericCodesForSteps;
	public List<SStepCodeText> _stepCodeTextList;

	public GameObject _congratulationPanel;
	public Animator _3dWorld;
	public GameObject [] _interactiveGameObjects;

	public Text _msgText;
    public RectTransform _textBox;

    [Tooltip("If true then the image indicator is composed by one single image, else it uses individual circle images")]
	public bool _useSingleImageIndicator = true;
	public Image _stepIndicatorImage;
	public Sprite[] _stepPerStepSpr;

	[Header("Circle indicator in right down corner of the screen")]
	public Image[] _circleIndicatorSteps;
	public Sprite _filledCircle;
	[Tooltip("Special one with green color")]
	public Sprite _firstFilledCircle;



	// Use this for initialization
	void Start () {

		ARDelegates.AROn += OnAROn;
		ARDelegates.AROff += OnAROff;
		DRDelegates.TouchObject += MoveForward;
		for (int i = 0; i < _interactiveGameObjects.Length; i++)
			_interactiveGameObjects[i].SetActive(false);
		GoToNextStep();
		SteadinessManager.Inst.ResetSteadiness();
	}

	void OnDestroy() {
		ARDelegates.AROn -= OnAROn;
		ARDelegates.AROff -= OnAROff;
		DRDelegates.TouchObject -= MoveForward;
	}

	void OnAROn() {
		if (_currentStep == 0)
			GoToNextStep();
	}

	 void OnAROff() {
		//_currentStep = 0;
		//DoStep();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void MoveForward() {
		if (_currentStep == 0)
			GoToNextStep();
	}

	// Se usa en el moving target por ahora, ojo
	public void GoToStep(int _stepNumber)
	{
		_currentStep = _stepNumber;
		if (_stepIndicatorImage != null)
			if (_currentStep > 0)
				_stepIndicatorImage.sprite = _stepPerStepSpr[_currentStep - 1];
		DoStep();
	}

	public void GoToNextStep() {
		_currentStep += 1;
		// Activar Timer luego de la detección del marcador
		if (_currentStep == 1)
		{
			CUITimeCounter.Inst.Activate();
		} 
			  
		if (_currentStep > 0)
		{
			if (_useSingleImageIndicator)
			{
				if (_stepIndicatorImage != null)
					_stepIndicatorImage.sprite = _stepPerStepSpr[_currentStep - 1];
			}                
			else
				_circleIndicatorSteps[_currentStep - 1].sprite = (_currentStep - 1 == 0 ? _firstFilledCircle : _filledCircle);
		}
		if (_currentStep == _totalSteps)
		{
			StartCoroutine(ShowCongratulation());

		}
		else
		{
			DoStep();
		}
	}

	void DoStep() {
		StartCoroutine(StepWith3dWorldAnimation());
        StartCoroutine(CCodeAnimations.LerpDisplacementPanels(() => {
            _msgText.text = _stepCodeTextList.Find(a => a._numericCode == _numericCodesForSteps[_currentStep])._textToWrite;
            StartCoroutine(CCodeAnimations.LerpDisplacementPanels(() => { }, _textBox, _textBox.anchoredPosition, _textBox.anchoredPosition - 400f * Vector2.down, 0.5f));
        }, _textBox, _textBox.anchoredPosition, _textBox.anchoredPosition + 400f * Vector2.down, 0.5f));
        
	}

	IEnumerator ShowCongratulation() {
	   // _globalTimeManager.UpdateResult();
		yield return new WaitForSeconds(.5f);
		CResultsManager.Inst.MostrarResultados();
		//_congratulationPanel.SetActive(true);
	}

	IEnumerator StepWith3dWorldAnimation()
	{
		if (_currentStep > 0)
		{
			if(_3dWorld != null)
			{
				_3dWorld.SetTrigger("avanzar");
				if (_currentStep == 1)
				{
					yield return new WaitForSeconds(2f);
				}
				else
				{
					yield return new WaitForSeconds(3f);
				}
			}           
			if(_currentStep - 1< _interactiveGameObjects.Length)
			{
				_interactiveGameObjects[_currentStep - 1].SetActive(true);
				SteadinessManager.Inst._objeto2 = _interactiveGameObjects[_currentStep - 1].transform;
			}
			
		}
		yield break;
	}
	
}
