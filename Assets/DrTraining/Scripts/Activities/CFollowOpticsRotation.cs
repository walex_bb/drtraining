﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CFollowOpticsRotation : MonoBehaviour {

    public Slider _saliderTelescoping;
    public float _angleCorrection;


    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x,transform.localEulerAngles.y, -_saliderTelescoping.value * 360f + _angleCorrection);
    }
}
