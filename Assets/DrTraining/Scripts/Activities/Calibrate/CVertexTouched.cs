﻿using UnityEngine;
using System.Collections;

public class CVertexTouched : MonoBehaviour {
    public CCalibrationManager _manager;
    public bool _wasTouched = false;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other) {
        if (_wasTouched)
            return;
        if (other.gameObject.CompareTag("CalibrateInstrument")) {
            _wasTouched = true;
            _manager.GoToNextStep();
        }
    }
}
