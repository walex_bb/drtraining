﻿using UnityEngine;
using System.Collections;

public class CTouchAndProbeObjectToTouchMoving : MonoBehaviour {

    public CTouchAndProbeMovingManager _manager;
    public CTrackingManager _managerTrasking;
    public enum TipoManager { TandPMT, Tracking };
    public TipoManager _tipoManager;
    public Animator _anim;
    public float _animSpeed = 0.1f;
    MeshRenderer _colorBola;
    string _actualColor = "dark";
    float _elapsedTime;
    [SerializeField]
    float _colorDurationTime;
    bool _loop = true;
    

    void Start() { 
        //_anim = GetComponent<Animator>();
        _anim.speed = 0;
        _colorBola = this.gameObject.GetComponent<MeshRenderer>();
    }

    public void PlayAnimation() {
        _anim.speed = _animSpeed;
        //_colorBola.material.color = Color.green;
        StartCoroutine("ChangeColor");
        SteadinessManager.Inst.ResumeSteadiness();
        CTrackingEstimationManager.Inst.PauseTrackingEst();
        //_manager.GoToStep(2);
    }

    public void StopAnimation()
    {
        _anim.speed = 0;
        StopCoroutine("ChangeColor");
        _colorBola.material.color = Color.red;
        SteadinessManager.Inst.PauseSteadiness();
        CTrackingEstimationManager.Inst.ResumeTrackingEst();
        //_manager.GoToStep(1);
    }

    IEnumerator ChangeColor()
    {
        while (_loop)
        {
            if (_actualColor == "dark")
            {
                while (_elapsedTime < _colorDurationTime)
                {
                    _elapsedTime += Time.deltaTime;
                    _colorBola.material.color = Color.Lerp(new Color32(0x05, 0x92, 0x1D, 0xFF), new Color32(0x1D, 0xFF, 0x00, 0xFF), _elapsedTime / _colorDurationTime); //Mathf.PingPong(Time.time, 1));
                    yield return null;
                }
                _elapsedTime = 0;
                _actualColor = "light";
            }
            else
            {
                while (_elapsedTime < _colorDurationTime)
                {
                    _elapsedTime += Time.deltaTime;
                    _colorBola.material.color = Color.Lerp(new Color32(0x1D, 0xFF, 0x00, 0xFF), new Color32(0x05, 0x92, 0x1D, 0xFF), _elapsedTime / _colorDurationTime); //Mathf.PingPong(Time.time, 1));
                    yield return null;
                }
                _elapsedTime = 0;
                _actualColor = "dark";
            }
            yield return null;
        }           
        yield return null;
    }

    public void EndAnimation() {
        SteadinessManager.Inst.PauseSteadiness();
        CTrackingEstimationManager.Inst.PauseTrackingEst();
        if(_tipoManager == TipoManager.TandPMT)
        {
            _manager.GoToNextStep();
        }
       
        gameObject.SendMessage("MarkAsTouched", SendMessageOptions.DontRequireReceiver);
        //_manager.GoToStep(2);
        
    }
}
