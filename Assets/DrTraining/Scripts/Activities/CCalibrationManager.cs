﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SStepCodeText{
	public int _numericCode;
	public string _textToWrite;
}

public enum ECalibrateActions {
	ACTIVATE_VERTEX,
	TURN_ON_VERTEX,
	SCALE_CILINDER,
	FACE_FADE_IN,
	ACTIVATE_GAME_OBJECT
}

[System.Serializable]
public class SActionsObjectsPerStep {
	public ECalibrateActions []_actionsToApplyOnObjects;
	public GameObject[] _objects;
}


public class CCalibrationManager : MonoBehaviour {
	public int _levelIndex;
	public int _totalSteps;
	private int _currentStep = -1;
	public int [] _numericCodesForSteps;
	public List<SStepCodeText> _stepCodeTextList;
	[SerializeField]
	float _finalPanelDelay;

	public GameObject _congratulationPanel;

	//public GameObject[] _interactiveGameObjects;

	public Text _msgText;
    public RectTransform _textBox;

	[Tooltip("If true then the image indicator is composed by one single image, else it uses individual circle images")]
	public bool _useSingleImageIndicator = true;
	public Image _stepIndicatorImage;
	public Sprite[] _stepPerStepSpr;

	public SActionsObjectsPerStep[] _actionsOnEveryStep;

	[Header("Circle indicator in right down corner of the screen")]
	public Image[] _circleIndicatorSteps;
	public Sprite _filledCircle;
	[Tooltip("Special one with green color")]
	public Sprite _firstFilledCircle;

	[Header("Elements to activate when popup OK is touched")]
	public bool _useNextList = false;
	public GameObject[] _activateListWhenGameStartConfirm;
	public Animator[] _animatorsToTurnSpeedZero;

    public Button _initialPopupOkButton;
    public bool _isMarkerPreDetected = false;

	void OnStartActivity() {
		//SetElementsListState(true);
	}

	void SetElementsListState(bool _isActive) {
		if (_activateListWhenGameStartConfirm != null)
			for (int i = 0; i < _activateListWhenGameStartConfirm.Length; i++)
				_activateListWhenGameStartConfirm[i].SetActive(_isActive);
	}

	// Use this for initialization
	void Start () {
		for (int i = 0; i < _animatorsToTurnSpeedZero.Length; i++)
			_animatorsToTurnSpeedZero[i].speed = 0;
        _initialPopupOkButton.onClick.AddListener(TouchOk);
        ARDelegates.PreNotificationAROn += OnPreNotificationAROn;
        ARDelegates.PreNotificationAROff += OnPreNotificationAROff;
		//SetElementsListState(false);


	}

    void OnPreNotificationAROn() {
        _isMarkerPreDetected = true;  
    }

    void OnPreNotificationAROff()
    {
        _isMarkerPreDetected = false;
    }

    public void TouchOk() {
        _initialPopupOkButton.onClick.RemoveListener(TouchOk);
        ARDelegates.AROn += OnAROn;
        DRDelegates.TouchObject += GoToNextStep;
        CDelegates.StartActivity += OnStartActivity;
        //for (int i = 0; i < _interactiveGameObjects.Length; i++)
        //    _interactiveGameObjects[i].SetActive(false);
        GoToNextStep();
        if (_isMarkerPreDetected)
            OnAROn();
    }

	void OnDestroy() {
		ARDelegates.AROn -= OnAROn;
        ARDelegates.PreNotificationAROn -= OnPreNotificationAROn;
        ARDelegates.PreNotificationAROff -= OnPreNotificationAROff;
		DRDelegates.TouchObject -= GoToNextStep;
		CDelegates.StartActivity -= OnStartActivity;
	}

	void OnAROn() {
		if (_currentStep == 0)
			GoToNextStep();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void GoToNextStep() {
		
		_currentStep += 1;
		
		if (_currentStep > 0)
		{
			if(_useSingleImageIndicator)
				_stepIndicatorImage.sprite = _stepPerStepSpr[_currentStep - 1];
			else
				_circleIndicatorSteps[_currentStep - 1].sprite = (_currentStep - 1 == 0? _firstFilledCircle: _filledCircle);
		}
		if (_currentStep == _totalSteps)
		{
			CAudioManager.Inst.PlaySFX("Destroy");
			StartCoroutine(ShowCongratulation());			
		}
		else
		{
			if (_currentStep > 1)
			{
				CAudioManager.Inst.PlaySFX("Found");
			}			
			DoStep();
		}
	}

	void DoStep() {
		if (_currentStep > 0)
		{
			//if (!_useNextList)
			//    //_interactiveGameObjects[_currentStep - 1].SetActive(true);
			//else
			//{
				SActionsObjectsPerStep _ao = _actionsOnEveryStep[_currentStep - 1];
				for (int i = 0; i < _ao._objects.Length; i++)
				{
					switch (_ao._actionsToApplyOnObjects[i])
					{
						case ECalibrateActions.ACTIVATE_VERTEX:
							_ao._objects[i].GetComponent<Animator>().speed = 1;
							break;
						case ECalibrateActions.TURN_ON_VERTEX:
							_ao._objects[i].GetComponent<Animator>().SetTrigger("change_color");
							break;
						case ECalibrateActions.SCALE_CILINDER:
							_ao._objects[i].GetComponent<Animator>().speed = 1;
							break;
						case ECalibrateActions.FACE_FADE_IN:
							_ao._objects[i].GetComponent<Animator>().speed = 1;
							break;
						case ECalibrateActions.ACTIVATE_GAME_OBJECT:
							_ao._objects[i].SetActive(true);
							break;
					}
				}
			//}

		}
        StartCoroutine(CCodeAnimations.LerpDisplacementPanels(() => {
            _msgText.text = _stepCodeTextList.Find(a => a._numericCode == _numericCodesForSteps[_currentStep])._textToWrite;
            StartCoroutine(CCodeAnimations.LerpDisplacementPanels(() => {}, _textBox, _textBox.anchoredPosition, _textBox.anchoredPosition - 400f * Vector2.down, 0.5f));
        }, _textBox, _textBox.anchoredPosition, _textBox.anchoredPosition + 400f * Vector2.down, 0.5f));
		
	}

	IEnumerator ShowCongratulation() {
        // CLoadSaveData.Inst._data.Name = "ahaaa";
        // CLoadSaveData.Inst.Save();

        if (CLocalPersist.GetUserLevel() < 0)
        {
            CLocalPersist.UpdateUserLevel(0);
        }
		yield return new WaitForSeconds(_finalPanelDelay);
		_congratulationPanel.SetActive(true);
		CAudioManager.Inst.PlaySFX("YouWin");
		CAudioManager.Inst.PlayMusic("WinTheme");
	}	




}
