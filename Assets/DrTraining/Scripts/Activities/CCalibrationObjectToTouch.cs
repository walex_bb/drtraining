﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CTouched))]
public class CCalibrationObjectToTouch : MonoBehaviour {
    private Material _mat;
    public Color _touchedColor;
    public Vector3 _rotationEdge;
    public int _angularSpeed;
    [HideInInspector]
    public bool _wasTouched = false;
    // Use this for initialization
    void Start()
    {
        _mat = GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_wasTouched)
        {
            transform.Rotate(_rotationEdge, _angularSpeed * Time.deltaTime);
        }
    }

    public void OnTouch()
    {
        _mat.SetColor("_Color", _touchedColor);
        _wasTouched = true;
        gameObject.SendMessage("MarkAsTouched", SendMessageOptions.DontRequireReceiver);
    }
}
