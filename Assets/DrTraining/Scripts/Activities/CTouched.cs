﻿using UnityEngine;
using System.Collections;

public class CTouched : MonoBehaviour {
    public string _eventToTriggerOnTouched;
    public string _eventToTriggerOnTouchedEnd;
    [HideInInspector]
    public bool _wasTouched = false;

    public void OnTouchEV(){
        
        Debug.Log("Touching the ball");
        gameObject.SendMessage(_eventToTriggerOnTouched, SendMessageOptions.DontRequireReceiver);
    }

    public void OnTouchExitEV()
    {
        Debug.Log("Not touching the ball anymore");
        gameObject.SendMessage(_eventToTriggerOnTouchedEnd, SendMessageOptions.DontRequireReceiver);
    }

    public void MarkAsTouched() {
        _wasTouched = true;
    }
}
