﻿using UnityEngine;
using System.Collections;

public class CTouchAndProbeObjectToTouch : MonoBehaviour {
    private bool _wasTouched = false;
    public CTouchAndProbeManager _manager;
    [SerializeField]
    CToucher _toucher;

    public void PlayAnimation() {
        if (!_wasTouched) {
            Animator _anim = GetComponent<Animator>();
            _anim.SetFloat("Velocity", 0.2f);
            _anim.SetBool("play_anim", true);
            SteadinessManager.Inst.ResumeSteadiness();
        }
    }

    public void StopAnimation()
    {
        if (!_wasTouched)
        {
            Animator _anim = GetComponent<Animator>();
            _anim.SetFloat("Velocity", -1.0f);
            _anim.SetBool("play_anim", false);
            SteadinessManager.Inst.PauseSteadiness();
        }
    }

    public void EndAnimation() {
        CAudioManager.Inst.PlaySFX("Lost");
        _toucher.StopCoroutine("SoundLoop");
        _manager.GoToNextStep();
        _wasTouched = true;
        gameObject.SendMessage("MarkAsTouched", SendMessageOptions.DontRequireReceiver);
        SteadinessManager.Inst.PauseSteadiness();
    }
}
