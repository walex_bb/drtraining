﻿using UnityEngine;
using System.Collections;

public class CToucher : MonoBehaviour {
    void OnTriggerEnter(Collider other) {
        CTouched _touched = other.gameObject.GetComponent<CTouched>();
        if (_touched != null && !_touched._wasTouched)
        {
            Debug.Log("IS TOUCHED");
            CAudioManager.Inst.PlaySFX("Found");
            StartCoroutine("SoundLoop");
            _touched.OnTouchEV();
            DRDelegates.OnTouchObject();
        }
    }

    void OnTriggerExit(Collider other) {
        CTouched _touched = other.gameObject.GetComponent<CTouched>();
        if (_touched != null && !_touched._wasTouched)
        {
            Debug.Log("IS NOT TOUCHED");
            CAudioManager.Inst.PlaySFX("Lost");
            StopCoroutine("SoundLoop");
            _touched.OnTouchExitEV();
            //DRDelegates.OnTouchObject();
        }
    }

    IEnumerator SoundLoop()
    {
        WaitForSeconds _soundDelay = new WaitForSeconds(0.3f);
        yield return _soundDelay;
        _soundDelay = new WaitForSeconds(1);
        while (true)
        {
            CAudioManager.Inst.PlaySFX("Loop");
            yield return _soundDelay;
        }
    }
}
