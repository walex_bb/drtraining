﻿using UnityEngine;
using System.Collections;

public class CMaskRotation : MonoBehaviour {
    public Transform _transformRef;
    private RectTransform _rect;

    public float _zrot;
    // Use this for initialization
    void Start () {
        _rect = GetComponent<RectTransform>();
	}

	
	// Update is called once per frame
	void Update () {
        if (_transformRef.gameObject.activeSelf) {
            float _xDist = _transformRef.right.x;
            float _yDist = _transformRef.right.y;
            float _zDist = _transformRef.right.z;

            _zrot = Mathf.Rad2Deg * Mathf.Atan2(
                _yDist,
                Mathf.Sqrt(_zDist * _zDist + _xDist * _xDist)
                );           
            if(_transformRef.forward.y < 0)
            {
                _zrot = -_zrot - 180;
            }
            _zrot = _zrot + 90;

            _rect.rotation = Quaternion.Euler(_rect.rotation.x, _rect.rotation.y, _zrot  );

        }
	}
}
