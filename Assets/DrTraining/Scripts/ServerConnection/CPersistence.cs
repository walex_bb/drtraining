﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;



[System.Serializable]
public class SUser {
    public int _id;
    public string _name;
    public string _email;
    public string _password;
    public string _date;
    public string _profession;
    public string _institution;

    public SUser(string _urlRequest, string email, string password, string name = "Anonimous", string profession = "", string institution = "") {
        _id = -1; //request id from server, if id == -1 locally then data cant be sended to server.
        _email = email;
        _password = password;
        //_date = date;
        _name = name;
        _profession = profession;
        _institution = institution;
    }

    public void UpdateUserInfo(string _urlRequest) {
        //generate xml and
        //send a request to update the data on the server
        //if id == -1 then request an id. if cant retrieve id then
    }

    public SUser(string _urlRequest) { 
        //request user info from server
    }
}

[System.Serializable]
public class SScenary {
    public int _id; // OBJECT ID on DB
    public int _userId; // which user is related to this result?
    
    public byte _agility; // Game fields
    public byte _steadiness;
    public byte _tracking;
    public byte _controlOfHorizont;
    public byte _periscoping;
    public byte _telescoping;

    public byte _overallPromedy; // derived from previous fields

    public string _date;
    public int _versionNumber;

    public SScenary(string _urlRequest, int userId,  byte agility, byte steadiness, byte tracking, byte controlOfHorizont, byte periscoping, byte telescoping) {
        //first, check if database is reacheable
        _id = -1; // request id from database
        _userId = userId;
        _agility = agility;
        _steadiness = steadiness;
        _tracking = tracking;
        _controlOfHorizont = controlOfHorizont;
        _periscoping = periscoping;
        _telescoping = telescoping;

        List<int> _sumValues = new List<int>();
        AddValue(ref _sumValues, _agility);
        AddValue(ref _sumValues, _steadiness);
        AddValue(ref _sumValues, _tracking);
        AddValue(ref _sumValues, _controlOfHorizont);
        AddValue(ref _sumValues, _periscoping);
        AddValue(ref _sumValues, _telescoping);

        _overallPromedy = GetPromedy(_sumValues);

        //_date = date;
        _versionNumber = 0; // request version number from server
    }

    public SScenary(string _urlRequest, int _id) { 
        //get a scenary from database
    }

    void AddValue(ref List<int> _list, byte _value) {
        if (_value > 100)
            _list.Add(_value);
    }

    byte GetPromedy(List<int> _list) {
        int _result = 0;
        if (_list.Count != 0)
        {
            for (int i = 0; i < _list.Count; i++)
            {
                _result += _list[i];
            }
        }
        else
            return 0;
        return (byte)(_result/_list.Count);
    }
}

[System.Serializable]
public class SSystemData {
    public int _currentSystemVersion;
    public string _lastVersionUpdated;
}

public class CPersistence : MonoBehaviour {

    private static CPersistence _instance;
    public static CPersistence GetInstante() {
        if (_instance == null) { 
        
        }
        return _instance;
    }
    private CPersistence() { }


    public SUser _user;
    public SScenary _bestScenary;
    public List<SScenary> _scenaryList;

    public void GetAbsoluteBestScenary(string _urlRequest) {
        if (_urlRequest == GameConstants.LOCAL_REQUEST_CODE)// || !CheckIfConnectionAvailable(_urlRequest))
        {
            //Dont send any request, use local data
            //GetBestScenaryPerCategory(_urlRequest);
            for (int i = 0; i < _scenaryList.Count; i++)
            {
                if (_bestScenary == null)
                    _bestScenary = _scenaryList[i];
                else
                    if (_bestScenary._overallPromedy < _scenaryList[i]._overallPromedy)
                        _bestScenary = _scenaryList[i];
            }
        }
        else {
            //request best scenary from server
        }
    }

    public bool GetUserData(string _urlRequest, string _email, string _password) {
        bool _result = false;
        /*if (CheckIfConnectionAvailable(_urlRequest)) { 
            _user = WWWRequester.SendRequest(_urlRequest, );
            if (){
            
            }
            _result = true;
        }*/
        
        return _result;
    }

    public void UpdateUserData(string _urlRequest) { 
    
    }

    public IEnumerator CheckIfConnectionAvailable(string _urlRequest) {
        yield return null;//StartCoroutine(WWWRequester.SendRequest(_urlRequest));
        
    }


}


/// Singleton que tenga el mejor escenario conseguido, tambien una entrada para cada escenario y los datos del jugador.
