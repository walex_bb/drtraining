﻿using UnityEngine;
using System.Collections;

public class WWWRequester {
    public static bool _result = false;
    public IEnumerator SendRequest(string _urlRequest) {
        WWW www = new WWW(_urlRequest);
        
        yield return www;

        _result = www.error == "";
        if (!_result)
            Debug.Log("Request error " + www.error);
    }
}
