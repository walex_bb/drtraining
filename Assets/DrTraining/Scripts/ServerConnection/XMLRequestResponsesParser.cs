﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public class XMLRequestResponsesParser : MonoBehaviour {

    public static List<SSkillData> XMLGetSkillsDataParse(string _xml) {
       
        SkillsDataList sk = JsonUtility.FromJson<SkillsDataList>(_xml);
        if (sk.Status != 0)
        {

            Debug.Log("" + "XMLGetScenariesDataParse error " + sk.Status.ToString());
            return new List<SSkillData>();
        }
        return sk.SkillsData;
    }

    
    public static List<SScenaryData> XMLGetScenariesDataParse(string _xml) {

        ScenariesDataList sd = JsonUtility.FromJson<ScenariesDataList>(_xml);
        if (sd.Status != 0) {

            Debug.Log("" + "XMLGetScenariesDataParse error " + sd.Status.ToString());
            return new List<SScenaryData>();
        }
        return sd.ExercisesData;

    }

    public static string XMLGetExercisesStatusResultSend(string _token) {
        StringWriterWithEncoding _sw = new StringWriterWithEncoding();
        XmlWriter _xml = XmlWriter.Create(_sw);

        _xml.WriteStartDocument();
        _xml.WriteStartElement("GetExercisesStatus");
        _xml.WriteElementString("Token", _token);
        _xml.WriteEndElement();
        _xml.WriteEndDocument();
        _xml.Flush();
        _sw.Close();

        Debug.Log(_sw.ToString());

        return _sw.ToString();   
    }

    public static string XMLGetBestScenaryResultsSend(string _token) {
        StringWriterWithEncoding _sw = new StringWriterWithEncoding();
        XmlWriter _xml = XmlWriter.Create(_sw);

        _xml.WriteStartDocument();
        _xml.WriteStartElement("GetBestExercisesResults");
        _xml.WriteElementString("Token", _token);
        _xml.WriteEndElement();
        _xml.WriteEndDocument();
        _xml.Flush();
        _sw.Close();

        Debug.Log(_sw.ToString());

        return _sw.ToString();  
    }

    public static string XMLGetBestSkillResultsSend(string _token) {
        StringWriterWithEncoding _sw = new StringWriterWithEncoding();
        XmlWriter _xml = XmlWriter.Create(_sw);

        _xml.WriteStartDocument();
        _xml.WriteStartElement("GetBestSkillsResults");
        _xml.WriteElementString("Token", _token);
        _xml.WriteEndElement();
        _xml.WriteEndDocument();
        _xml.Flush();
        _sw.Close();

        Debug.Log(_sw.ToString());

        return _sw.ToString(); 
    }

    public static List<int> XMLGetBestSkillResultsParse(string _xml) {
        List<int> _skillResults = new List<int>();
 
        XmlDocument _xmlDoc = new XmlDocument();
        StringReader _stringReader = new StringReader(_xml);
        _xmlDoc.LoadXml(_stringReader.ReadToEnd());

        XmlNodeList _nodes = _xmlDoc.GetElementsByTagName("SkillScore");
        int _agility = -1; // tiempo
        int _linearNavigation = -1; // distancia
        int _angularNavigation = -1;
        int _steadiness = -1; // cambia al modo de colision con la esfera
        int _tracking = -1;
        int _centering = -1;
        int _horizonControl = -1;

        foreach (XmlNode _exerciseResult in _nodes)
        {

            XmlNode _idNode = _exerciseResult.ChildNodes[0];
            Debug.Log("Skill ID: " + _idNode.InnerText);

            XmlNode _scoreNode = _exerciseResult.ChildNodes[1];
            Debug.Log("Score: " + _scoreNode.InnerText);

            int _scoreResult = Int32.Parse(_scoreNode.InnerText);
            switch (_idNode.InnerText)
            {
                case "1":
                    _agility = _scoreResult;
                    break;
                case "2":
                    _linearNavigation = _scoreResult;
                    break;
                case "3":
                    _angularNavigation = _scoreResult;
                    break;
                case "4":
                    _steadiness = _scoreResult;
                    break;
                case "5":
                    _tracking = _scoreResult;
                    break;
                case "6":
                    _centering = _scoreResult;
                    break;
                case "7":
                    _horizonControl = _scoreResult;
                    break;
            }
        }
        _skillResults.Add(0); // the index [0] is not used. Check the documentation for skill codes on drive document
        _skillResults.Add(_agility);
        _skillResults.Add(_linearNavigation);
        _skillResults.Add(_angularNavigation);
        _skillResults.Add(_steadiness);
        _skillResults.Add(_tracking);
        _skillResults.Add(_centering);
        _skillResults.Add(_horizonControl);

        return _skillResults;
    }

    public static string ParseDate(DateTime _date)
    {
        /*
        string _year = "";
        string _month = "";
        string _day = "";
        string _hours = "";
        string _minutes = "";
        string _seconds = "";

        _year = _date.Year.ToString();
        if (_date.Month < 10)
            _month = "0" + _date.Month;
        else
            _month = "" + _date.Month;


        if (_date.Day < 10)
            _day = "0" + _date.Day;
        else
            _day = "" + _date.Day;

        _hours = _date.Hour;
        _minutes = _date.Minute;
        _seconds = _date.Second;
        string _result = _year + "-" + _month + "-" + _day + "-" + _hours + "-" + _minutes + "-" + _seconds;
        */

        //string _result = string.Format("{0:D2}-{1:D2}-{2:D2}-{3:D2}-{4:D2}-{5:D2}", _date.Year, _date.Month, _date.Day, _date.Hour, _date.Minute, _date.Second);
        string _result = _date.ToString("yyyy-MM-dd HH:mm:ss");
        Debug.Log(_result);
        return _result;
    }

    

    public static string XMLRaceInformationRequest(RaceInformationRequest request)
    {
        string json = JsonUtility.ToJson(request);
        return json;
    }

    public static string XMLGlobalRankingRequest(GlobalRankingRequest request)
    {
        string json = JsonUtility.ToJson(request);
        return json;
    }

    public static string XMLPersonalRankingRequest(PersonalRankingRequest request)
    {
        string json = JsonUtility.ToJson(request);
        return json;
    }

    public static string XMLRaceDataRequest(RaceDataRequest request) {

        string json = JsonUtility.ToJson(request);
        return json;
    }

    public static string XMLSubmitScenaryResultsSend(ScenaryResultRequest results) {
        
        /*
        StringWriterWithEncoding _sw = new StringWriterWithEncoding();
        XmlWriter _xml = XmlWriter.Create(_sw);

        _xml.WriteStartDocument();
        _xml.WriteStartElement("SubmitExerciseResults");
        _xml.WriteElementString("Token", _token);
        _xml.WriteStartElement("Results");
        for (int i = 0; i < _scenaryResults.Count; i++) {
            _xml.WriteStartElement("ExerciseResult");
            _xml.WriteElementString("Id", _scenaryResults[i].Id.ToString());

            _xml.WriteElementString("Timestamp", _scenaryResults[i].Timestamp);

            _xml.WriteElementString("TotalSeconds", _scenaryResults[i]._seconds.ToString());
            _xml.WriteStartElement("Scores");
            for (int j = 0; j < _scenaryResults[i].Scores.Count; j++)
            {
                _xml.WriteStartElement("SkillScore");

                _xml.WriteElementString("Id", _scenaryResults[i].Scores[j].Id.ToString());
                _xml.WriteElementString("Score", _scenaryResults[i].Scores[j].Score.ToString());
                Debug.Log("Reporting Skill " + _scenaryResults[i].Scores[j].Id);
                _xml.WriteEndElement(); // SkillScore
            }
            _xml.WriteEndElement(); // Scores
            _xml.WriteEndElement(); // ExerciseResult
        }
        _xml.WriteEndElement(); // Results
        _xml.WriteEndElement(); // SubmitExerciseResults
        _xml.WriteEndDocument();
        _xml.Flush();
        _sw.Close();
        
        Debug.Log(_sw.ToString());
        
        return _sw.ToString();    
        */
        
        string json = JsonUtility.ToJson(results);
        return json;
    }

    public static Dictionary<EActivityCode, SActivityData> XMLGetBestScenaryResultsParse(string _xml) {
        Dictionary<EActivityCode, SActivityData> _bestScenaries = new Dictionary<EActivityCode, SActivityData>();

        XmlDocument _xmlDoc = new XmlDocument(); 
        StringReader _stringReader = new StringReader(_xml); 
        //_stringReader.Read(); // skip BOM
        //Debug.Log(_stringReader.ReadToEnd());
        _xmlDoc.LoadXml(_stringReader.ReadToEnd());

        XmlNodeList _nodes = _xmlDoc.GetElementsByTagName("ExerciseResult");
        foreach (XmlNode _exerciseResult in _nodes) {
            
            XmlNode _idNode = _exerciseResult.ChildNodes[0];
            Debug.Log("Exercise ID:" + _idNode.InnerText);

            XmlNode _timeStampNode = _exerciseResult.ChildNodes[1];
            Debug.Log(_timeStampNode.InnerText);


            XmlNode _scoresNode = _exerciseResult.ChildNodes[5];
            int _agility = 0;
            int _linearNavigation = 0;
            int _angularNavigation = 0;
            int _steadiness = 0;
            int _tracking = 0;
            int _centering = 0;
            int _horizonControl = 0;

            int _skillIdNumber = 1;
            if (_scoresNode.Name == "Scores")
            {
                XmlNodeList _skillNodes = _scoresNode.ChildNodes;
                foreach (XmlNode _skillScore in _skillNodes) {
                    XmlNode _skillId = _skillScore.ChildNodes[0];
                    
                    if (_skillId.Name == "Id")
                    {
                        Debug.Log("Skill ID:" + _skillId.InnerText);
                        _skillIdNumber = Int32.Parse(_skillId.InnerText);
                    }

                    XmlNode _skillValue = _skillScore.ChildNodes[1];
                    if (_skillValue.Name == "Score") {
                        Debug.Log("Score:" + _skillValue.InnerText);
                        int _scoreResult = Int32.Parse(_skillValue.InnerText);
                        switch (_skillIdNumber) { 
                            case 1:
                                _agility = _scoreResult;
                                break;
                            case 2:
                                _linearNavigation = _scoreResult;
                                break;
                            case 3:
                                _angularNavigation = _scoreResult;
                                break;
                            case 4:
                                _steadiness = _scoreResult;
                                break;
                            case 5:
                                _tracking = _scoreResult;
                                break;
                            case 6:
                                _centering = _scoreResult;
                                break;
                            case 7:
                                _horizonControl = _scoreResult;
                                break;
                        }
                    }
                }
            }
            SActivityData _activityData = new SActivityData((EActivityCode)Int32.Parse(_idNode.InnerText), _timeStampNode.InnerText, _agility, _linearNavigation, _angularNavigation, _steadiness, _tracking, _centering, _horizonControl);
            if (!_bestScenaries.ContainsKey(_activityData._code))
                _bestScenaries.Add(_activityData._code, _activityData);
        }

        return _bestScenaries;
    }

    public static string XMLUserRegisterSend(RegisterAccountRequest accountInfo) {

        string json = JsonUtility.ToJson(accountInfo);
        return json;
    }

    public static string XMLUpdateUserDataSend(UpdateUserInfoRequest ueReq)
    {
        string json = JsonUtility.ToJson(ueReq);
        return json;

    }

    public static string XMLUserLoginSend(LoginRequest loginReq) {
      
        string json = JsonUtility.ToJson(loginReq);
        return json;
    }

    public static Dictionary<string, string> XMLUserLoginMsgParse(string _responseText, out string token) {

        AuthResponse lr = JsonUtility.FromJson<AuthResponse>(_responseText);
        Dictionary<string, string> _resultParsed = new Dictionary<string, string>();

        string status_str = lr.Status.ToString();
        Debug.Log("STATUS:" + status_str);
        _resultParsed["Status"] = status_str;

        if (status_str != "0")
        {
            token = "";
            _resultParsed["Id"] = "0";
            _resultParsed["Type"] = "0";
            _resultParsed["Description"] = lr.Message;
        }
        else {

            _resultParsed["Description"] = "Login successfully";
            token = lr.Token;
        }

        
        return _resultParsed;
    } 

    public static string XMLUserRecoveryPasswordSend(RecoverPasswordRequest rpr) {
        
        string json = JsonUtility.ToJson(rpr);
        return json;
    }
   

    public static string XMLGetUserDataSend(string _token) {        

        UserDataRequest udr = new UserDataRequest(_token);
        return JsonUtility.ToJson(udr);
    }

    public static int XMLGetUserDataResponse(string _xml, Dictionary<string, string> _responseData) {
        Debug.Log(_xml);

        UserDataResponse udr = JsonUtility.FromJson<UserDataResponse>(_xml);
        if (udr.Status == 0) {

            _responseData["FirstName"] = udr.UserInfo.FirstName;
            _responseData["LastName"] = udr.UserInfo.LastName;
            _responseData["Email"] = udr.UserInfo.Email;
            _responseData["WorkPlace"] = udr.UserInfo.WorkPlace;
            _responseData["BornDate"] = udr.UserInfo.BornDate;
            _responseData["Profession"] = udr.UserInfo.Profession;
        } else {

            string errors = "";
            string m = udr.Message;
            errors += m;            
            Debug.Log("XMLGetUserDataResponse error " + udr.Status.ToString() + '\n' + errors);
        }        
        
        return udr.Status;
    }

    public static List<int> XMLGetExercisesStatusResultParse(string _xml) {
        //Debug.Log(_xml);
        List<int> _gameCodes = new List<int>();

        XmlReader _xmlReader = XmlReader.Create(new StringReader(_xml));
        while (_xmlReader.Read())
        {
            if (_xmlReader.IsStartElement("ExerciseStatus")/* _xmlReader.Name == "ExerciseStatus"*/)
            {
                _xmlReader.Read();
                _xmlReader.Read();
                int _id = Int32.Parse(_xmlReader.Value);
                _xmlReader.Read();

               _xmlReader.Read();
                _xmlReader.Read();
                int _status = Int32.Parse(_xmlReader.Value);

                if (_status == 1)
                    _gameCodes.Add(_id);
            }
        }

        return _gameCodes;
    }

    public static string XMLClaimSimulateCodeRequestSend(string _token, string _codeToClaim)
    {
        StringWriterWithEncoding _sw = new StringWriterWithEncoding();
        XmlWriter _xml = XmlWriter.Create(_sw);

        _xml.WriteStartDocument();
        _xml.WriteStartElement("ClaimSimulateExerciseCode");
        _xml.WriteElementString("Token", _token);
        _xml.WriteElementString("Code", _codeToClaim);
        _xml.WriteEndElement();
        _xml.WriteEndDocument();
        _xml.Flush();
        _sw.Close();

        Debug.Log(_sw.ToString());

        return _sw.ToString();
    }

    public static Dictionary<string, string> XMLClaimCodeRequestMsgParse(string _responseText)
    {
        Dictionary<string, string> _resultParsed = new Dictionary<string, string>();

        XmlDocument _xmlDoc = new XmlDocument();
        StringReader _stringReader = new StringReader(_responseText);
        _xmlDoc.LoadXml(_stringReader.ReadToEnd());

        XmlNodeList _nodes = _xmlDoc.GetElementsByTagName("Status");

        foreach (XmlNode _result in _nodes)
        {

            XmlNode _statusNode = _result.ChildNodes[0];
            Debug.Log("STATUS:" + _statusNode.InnerText);
            _resultParsed["Status"] = _statusNode.InnerText;
            if (_statusNode.InnerText == "0")
            {
                _resultParsed["Description"] = "CODE CLAIMED!";
            }
            else
            {

                XmlNodeList _messageNodes = _xmlDoc.GetElementsByTagName("Message");
                foreach (XmlNode _messageNode in _messageNodes)
                {
                    _resultParsed["Id"] = _messageNode.ChildNodes[0].InnerText;
                    _resultParsed["Type"] = _messageNode.ChildNodes[1].InnerText;
                    _resultParsed["Description"] = _messageNode.ChildNodes[2].InnerText;
                }
            }
        }

        return _resultParsed;
    }

    public static Dictionary<string, string> XMLGetExercisesStatusRequestMsgParse(string _responseText)
    {
        Dictionary<string, string> _resultParsed = new Dictionary<string, string>();

        XmlDocument _xmlDoc = new XmlDocument();
        StringReader _stringReader = new StringReader(_responseText);
        _xmlDoc.LoadXml(_stringReader.ReadToEnd());

        XmlNodeList _nodes = _xmlDoc.GetElementsByTagName("Status");

        foreach (XmlNode _result in _nodes)
        {

            XmlNode _statusNode = _result.ChildNodes[0];
            Debug.Log("STATUS:" + _statusNode.InnerText);
            _resultParsed["Status"] = _statusNode.InnerText;
            if (_statusNode.InnerText == "0")
            {
                _resultParsed["Description"] = "RESULTS RECEIVED";
            }
            else
            {

                XmlNodeList _messageNodes = _xmlDoc.GetElementsByTagName("Message");
                foreach (XmlNode _messageNode in _messageNodes)
                {
                    _resultParsed["Id"] = _messageNode.ChildNodes[0].InnerText;
                    _resultParsed["Type"] = _messageNode.ChildNodes[1].InnerText;
                    _resultParsed["Description"] = _messageNode.ChildNodes[2].InnerText;
                }
            }
        }

        return _resultParsed;
    }

}

public class StringWriterWithEncoding : StringWriter
{
    private readonly Encoding _encoding = Encoding.UTF8;

    public StringWriterWithEncoding()
    {
    }

    public StringWriterWithEncoding(IFormatProvider formatProvider)
        : base(formatProvider)
    {
    }

    public StringWriterWithEncoding(StringBuilder sb)
        : base(sb)
    {
    }

    public StringWriterWithEncoding(StringBuilder sb, IFormatProvider formatProvider)
        : base(sb, formatProvider)
    {
    }


    public StringWriterWithEncoding(Encoding encoding)
    {
        _encoding = encoding;
    }

    public StringWriterWithEncoding(IFormatProvider formatProvider, Encoding encoding)
        : base(formatProvider)
    {
        _encoding = encoding;
    }

    public StringWriterWithEncoding(StringBuilder sb, Encoding encoding)
        : base(sb)
    {
        _encoding = encoding;
    }

    public StringWriterWithEncoding(StringBuilder sb, IFormatProvider formatProvider, Encoding encoding)
        : base(sb, formatProvider)
    {
        _encoding = encoding;
    }

    public override Encoding Encoding
    {
        get { return (null == _encoding) ? base.Encoding : _encoding; }
    }
}
