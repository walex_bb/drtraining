﻿using UnityEngine;
using System.Collections;

public class CLoadingAdvice : MonoBehaviour {
    public float _checkForServerConectionInterval = 1.0f;
    public GameObject _childAdvice;
    public GameObject _lostConectionAdvice;
    public bool _isConnected;
    public bool _doNotInitialize = false;
	// Use this for initialization
	void Start () {

        if (_doNotInitialize == false)
        {
            _childAdvice.SetActive(false);
            _lostConectionAdvice.SetActive(false);
        }
        DontDestroyOnLoad(this.gameObject);
        StartCoroutine(CheckForServerConection());
	}

    IEnumerator CheckForServerConection() {
        WaitForSeconds _waitTime = new WaitForSeconds(_checkForServerConectionInterval);
        while (true) {
            _isConnected = !NotInternetConnected();
            _lostConectionAdvice.SetActive(!_isConnected);
            yield return _waitTime;
        }
    }

    public bool NotInternetConnected() {
        return Application.internetReachability == NetworkReachability.NotReachable;
    }

    public void On() {
        _childAdvice.SetActive(true);
    }

    public void Off() {
        _childAdvice.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
