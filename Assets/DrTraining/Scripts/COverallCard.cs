﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class COverallCard : MonoBehaviour
{
    public Transform _indicatorsContainer;
    public List<SIndicator> _indicatorsData;
    public GameObject _indicatorPrefab;
    private GameObject[] _cachedIndicators;


    // Use this for initialization
    void Start()
    {
        _cachedIndicators = new GameObject[_indicatorsData.Count];
        for (int i = 0; i < _indicatorsData.Count; i++)
        {

            GameObject _newIndicatorGO = Instantiate(_indicatorPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            _newIndicatorGO.transform.SetParent(_indicatorsContainer, false);
            _cachedIndicators[i] = _newIndicatorGO;

            SIndicator _data = _indicatorsData[i];
            _newIndicatorGO.GetComponent<CIndicator>().Configure(_data._name, _data._value);
            _newIndicatorGO.GetComponent<CIndicator>().Deactivate();
        }
    }


    public void SetIndicator(string _indicatorCode, int _steadinessNormalized)
    {
        Debug.Log("Seteando Indicador: " + _indicatorCode);
        CIndicator _indicator = _cachedIndicators[_indicatorsData.FindIndex(a => a._code == _indicatorCode)].GetComponent<CIndicator>();
        _indicator.Configure(_indicatorsData.Find(a => a._code == _indicatorCode)._name, _steadinessNormalized);
        _indicator.Activate();
    }


    // Update is called once per frame
    void Update()
    {

    }
}

