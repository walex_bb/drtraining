﻿using UnityEngine;
using System.Collections;

public class CDelegates {

	public delegate void NotifyAnimationsDelegate(string _code);
    public static event NotifyAnimationsDelegate AnimationStart;
    public static event NotifyAnimationsDelegate AnimationEnd;

    public static void OnAnimationStart(string _code)
    {
        if (AnimationStart != null)
            AnimationStart(_code);
    }

    public static void OnAnimationEnd(string _code)
    {
        if (AnimationEnd != null)
            AnimationEnd(_code);
    }

    public delegate void DelegateNoParameter();
    public static event DelegateNoParameter AddActivityStep;

    public static void OnAddActivityStep()
    {
        if (AddActivityStep != null)
        {
            AddActivityStep();
        }
    }

    public static event DelegateNoParameter StartActivity;

    public static void OnStartActivity()
    {
        if (StartActivity != null)
        {
            StartActivity();
        }
    }
}
