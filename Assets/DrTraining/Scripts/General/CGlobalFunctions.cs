﻿using UnityEngine;
using System.Collections;
using System;

public class CGlobalFunctions : MonoBehaviour {

    public static string GetDateString() {
        DateTime _today = DateTime.Now;
        return _today.Day + "/" + _today.Month + "/" + _today.Year;
    }
}
