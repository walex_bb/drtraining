﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class CSceneLoader : MonoBehaviour {
    public float _showLoaderDuration = 0;
    public Image [] _loadingPanelElements;
    Color[] _loadingPanelElementsInitialColor;
    Color[] _loadingPanelElementsTransparentColor;
    Vector2 _loadingPanelOutsidePos;

    void Start() {
        
        DRDelegates.LoadLevelMsg += LoadScene;
        if (_showLoaderDuration > 0) {
            _loadingPanelElementsInitialColor = new Color[_loadingPanelElements.Length];
            _loadingPanelElementsTransparentColor = new Color[_loadingPanelElements.Length];
            for (int i = 0; i < _loadingPanelElements.Length; i++)
            {
                _loadingPanelElementsInitialColor[i] = _loadingPanelElements[i].color;
                _loadingPanelElementsTransparentColor[i] = new Color(_loadingPanelElements[i].color.r, _loadingPanelElements[i].color.g, _loadingPanelElements[i].color.b, 0);
                //_loadingPanelElements[i].color = _loadingPanelElementsTransparentColor[i];
            }

            for (int i = 0; i < _loadingPanelElements.Length; i++)
                StartCoroutine(
                    CCodeAnimations.LerpAlphaColor(() =>
                    {
                    }, _loadingPanelElements[i], _loadingPanelElementsInitialColor[i], _loadingPanelElementsTransparentColor[i], _showLoaderDuration)
                );
        }
    }

    void OnDisable(){
        DRDelegates.LoadLevelMsg -= LoadScene;
    }

    public void LoadScene(string _sceneName) {
        //.LoadScene(_sceneName); 
        if (_showLoaderDuration > 0) {
            for (int i = 0; i < _loadingPanelElements.Length - 1; i++)
                StartCoroutine(
                    CCodeAnimations.LerpAlphaColor(() =>
                    {
                    }, _loadingPanelElements[i], _loadingPanelElementsTransparentColor[i], _loadingPanelElementsInitialColor[i], _showLoaderDuration)
                );

            StartCoroutine(
                    CCodeAnimations.LerpAlphaColor(() =>
                    {
                        CLoadSceneManager.Inst.ChangeScene(_sceneName);
                    }, _loadingPanelElements[_loadingPanelElements.Length - 1], _loadingPanelElementsTransparentColor[_loadingPanelElements.Length - 1], _loadingPanelElementsInitialColor[_loadingPanelElements.Length - 1], _showLoaderDuration)
                );
        }
        else
            CLoadSceneManager.Inst.ChangeScene(_sceneName);
    }

    IEnumerator LoadNewSceneDelayRenderer(string _sceneName) {
        yield return new WaitForEndOfFrame();
        CLoadSceneManager.Inst.ChangeScene(_sceneName);
    }
}
