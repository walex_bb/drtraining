/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

namespace Vuforia
{
    /// <summary>
    /// A custom handler that implements the ITrackableEventHandler interface.
    /// </summary>
    public class DefaultTrackableEventHandler : MonoBehaviour,
                                                ITrackableEventHandler
    {
      //  public  MaskableGraphic [] _displaysToHideOnTrackingFound;
        public bool _isFound = false;
        //public float _fadeSpeed = 1.0f;
        #region PRIVATE_MEMBER_VARIABLES
 
        private TrackableBehaviour mTrackableBehaviour;
    
        #endregion // PRIVATE_MEMBER_VARIABLES



        #region UNTIY_MONOBEHAVIOUR_METHODS
    
        void Start()
        {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
        }

        #endregion // UNTIY_MONOBEHAVIOUR_METHODS



        #region PUBLIC_METHODS

        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        #endregion // PUBLIC_METHODS



        #region PRIVATE_METHODS


        private void OnTrackingFound()
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Enable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = true;
            }

            // Enable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = true;
            }
            _isFound = true;
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
            ARDelegates.OnPreNotificationAROn();
            ARDelegates.OnAROn();
        }


        private void OnTrackingLost()
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = false;
            }

            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
            _isFound = false;
            ARDelegates.OnPreNotificationAROff();
            ARDelegates.OnAROff();
        }

        //void Update() {
        //    if (_isFound)
        //    {
        //        for (int i = 0; i < _displaysToHideOnTrackingFound.Length; i++)
        //        {
        //            Color c = _displaysToHideOnTrackingFound[i].color;
        //            c.a -= Time.deltaTime * _fadeSpeed;
        //            if (c.a <= 0)
        //                c.a = 0;
        //            _displaysToHideOnTrackingFound[i].color = c;
        //        }
        //    }
        //    else {
        //        for (int i = 0; i < _displaysToHideOnTrackingFound.Length; i++)
        //        {
        //            Color c = _displaysToHideOnTrackingFound[i].color;
        //            c.a += Time.deltaTime * _fadeSpeed;
        //            if (c.a >= 1)
        //                c.a = 1;
        //            _displaysToHideOnTrackingFound[i].color = c;
        //        }
        //    }
        //}

        #endregion // PRIVATE_METHODS
    }
}
