﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class CPXManager : MonoBehaviour {

	public bool _centeringActive;
    public bool _isMarkerPreDetected = false;
    public Button _initialPopupOkButton;

    public static CPXManager Inst;
    
    void Awake()
    {
        Inst = this;
    }


    void Start()
    {
        ARDelegates.PreNotificationAROn += OnPreNotificationAROn;
        ARDelegates.PreNotificationAROff += OnPreNotificationAROff;
        _initialPopupOkButton.onClick.AddListener(OnOkButton);
    }

    void OnOkButton() {
        
        _initialPopupOkButton.onClick.RemoveListener(OnOkButton);
        DelegatesSubscription(true);
        if (_isMarkerPreDetected)
            FirstDetectionDone();
        
    }

    void OnDestroy()
    {
        ARDelegates.PreNotificationAROn -= OnPreNotificationAROn;
        ARDelegates.PreNotificationAROff -= OnPreNotificationAROff;
        DelegatesSubscription(false);
    }

    void OnPreNotificationAROn()
    {
        _isMarkerPreDetected = true;
    }

    void OnPreNotificationAROff()
    {
        _isMarkerPreDetected = false;
    }

    void DelegatesSubscription(bool _subscribing)
    {
        if (_subscribing)
        {
            ARDelegates.AROn += FirstDetectionDone;
			if (!_centeringActive) {
				CSkillsMeasurement.DistanceTargetLocked += ImageOK;
				CSkillsMeasurement.DistanceTargetLost += ImageNotOK;
			} else {
				CSkillsMeasurement.AngleTargetLocked += ImageOK;
				CSkillsMeasurement.AngleTargetLost += ImageNotOK;
			}           
            CPXAnimationsManager.EndAnimationOn += NextStep;
            CPXAnimationsManager.EndAllAnimationOn += EndScenario;
        }
        else
        {
            ARDelegates.AROn -= FirstDetectionDone;
			if (!_centeringActive) {
				CSkillsMeasurement.DistanceTargetLocked -= ImageOK;
				CSkillsMeasurement.DistanceTargetLost -= ImageNotOK;
			} else {
				CSkillsMeasurement.AngleTargetLocked -= ImageOK;
				CSkillsMeasurement.AngleTargetLost -= ImageNotOK;
			} 
            CPXAnimationsManager.EndAnimationOn -= NextStep;
            CPXAnimationsManager.EndAllAnimationOn -= EndScenario;
        }
    }    

    private void FirstDetectionDone()
    {
        Debug.Log("First Detection...");
        CXIndicadoresManager.Inst.MarkNextStep();
        ARDelegates.AROn -= FirstDetectionDone;
        CPXAnimationsManager.Inst.NextStep();
    }

    private void ImageOK()
    {
        CPXAnimationsManager.Inst.PlayAnimation();
    }

    private void ImageNotOK()
    {
        CPXAnimationsManager.Inst.StopAnimation();
    }

    private void NextStep()
    {
        CXIndicadoresManager.Inst.MarkNextStep();
    }


    private void EndScenario()
    {
        DelegatesSubscription(false);
        CResultsManager.Inst.MostrarResultados();
    }












}
