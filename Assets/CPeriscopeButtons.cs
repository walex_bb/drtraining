﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CPeriscopeButtons : MonoBehaviour {

    public Color _on, _off;

    public void Prender()
    {
        this.GetComponent<Image>().color = _on;
    }

    public void Apagar()
    {
        this.GetComponent<Image>().color = _off;
    }

}
