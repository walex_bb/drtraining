﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class STParameter {
    public string _typeName = "";
    public string _parameterName = "";
}

public class shat_DelegateCodeGenerator : EditorWindow {

    private string _generatedDelegateText;
    private static List<STParameter> _parameterList = new List<STParameter>();
    private static List<string> _derivedDelegateNames = new List<string>();
    private string _delegateReturnType = "void";
    private string _delegateBaseName = "DelegateBaseName";

    [MenuItem("SirHat/Delegate Code Generator")]
    static void Init() {
        shat_DelegateCodeGenerator _window = (shat_DelegateCodeGenerator)EditorWindow.GetWindow(typeof(shat_DelegateCodeGenerator));
        _window.Show();
        //_parameterList = new List<STParameter>();
    }

    void OnGUI()
    {
        bool _missingDelegateParameters = false;
        bool _missingEventName = false;
        _generatedDelegateText = "public delegate " + _delegateReturnType + " " + _delegateBaseName + "(" ;

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label("DELEGATE PROPERTIES", EditorStyles.boldLabel);
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.Space(10);
        EditorGUILayout.HelpBox("Uso: agregar los parametros/eventos de tu delegado, copiar usando CTRL + c o el boton de Copy y pegarlo en el .cs donde tengas tus delegados", MessageType.Info);
        GUILayout.Space(10);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label(new GUIContent("Type", "The type of the delegate (void, int, float, etc)"), GUILayout.Width(50));
        _delegateReturnType = GUILayout.TextField(_delegateReturnType, GUILayout.Width(50));
        GUILayout.Label(new GUIContent("Name", "The name of the delegate"), GUILayout.Width(70));
        _delegateBaseName = GUILayout.TextField(_delegateBaseName, GUILayout.Width(200));
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Add parameter"))
        {
            _parameterList.Add(new STParameter());
        }

        if (_parameterList.Count > 0)
        {
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label(new GUIContent("Param", "The parameter type (void, int, float, etc)"), GUILayout.Width(50));
            GUILayout.Label(new GUIContent("Name", "The variable name"), GUILayout.Width(200));
            GUILayout.Label("", GUILayout.Width(30));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            for (int i = 0; i < _parameterList.Count; i++)
            {
                _generatedDelegateText += _parameterList[i]._typeName + " " + _parameterList[i]._parameterName + (i == (_parameterList.Count - 1) ? "" : ", ");
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                _parameterList[i]._typeName = GUILayout.TextField(_parameterList[i]._typeName, GUILayout.Width(50));
                _parameterList[i]._parameterName = GUILayout.TextField(_parameterList[i]._parameterName, GUILayout.Width(200));
                if (_parameterList[i]._typeName == "" || _parameterList[i]._parameterName == "")
                    _missingDelegateParameters = true;
                if (GUILayout.Button("-", GUILayout.Width(30)))
                {
                    _parameterList.RemoveAt(i);
                }
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
        }
        _generatedDelegateText += ");";
        _generatedDelegateText += "\n";

        if (GUILayout.Button("Add Event"))
        {
            _derivedDelegateNames.Add("CustomEvent");
        }

        if (_derivedDelegateNames.Count > 0)
        {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label(new GUIContent("Event name", "The event name"), GUILayout.Width(150));
            GUILayout.Label("", GUILayout.Width(30));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            for (int i = 0; i < _derivedDelegateNames.Count; i++)
            {
                if (_derivedDelegateNames[i] == "")
                    _missingEventName = true;
                _generatedDelegateText += "public static event " + _delegateBaseName + " " + _derivedDelegateNames[i] + ";";
                _generatedDelegateText += "\n";

                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                _derivedDelegateNames[i] = GUILayout.TextField(_derivedDelegateNames[i], GUILayout.Width(150));
                if (GUILayout.Button("-", GUILayout.Width(30)))
                {
                    _derivedDelegateNames.RemoveAt(i);
                }
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
            }

            for (int i = 0; i < _derivedDelegateNames.Count; i++)
            {
                _generatedDelegateText += "\n";
                _generatedDelegateText += "public static " + _delegateReturnType + " On" + _derivedDelegateNames[i] + "(";
                for (int j = 0; j < _parameterList.Count; j++)
                {
                    _generatedDelegateText += _parameterList[j]._typeName + " " + _parameterList[j]._parameterName + (j == (_parameterList.Count - 1) ? "" : ", ");
                }

                _generatedDelegateText += "){\n";
                _generatedDelegateText += "\tif (" + _derivedDelegateNames[i] + " != null){\n";
                _generatedDelegateText += "\t\tOn" + _derivedDelegateNames[i] + "(";
                for (int j = 0; j < _parameterList.Count; j++)
                {
                    _generatedDelegateText += _parameterList[j]._parameterName + (j == (_parameterList.Count - 1) ? "" : ", ");
                }
                _generatedDelegateText += ")" + ";" + "\n";
                _generatedDelegateText += "}\n";
                //_generatedDelegateText += "\n";
            }

        }

        if (_missingDelegateParameters || _missingEventName || _delegateReturnType == "" || _delegateBaseName == "")
        {
            EditorGUI.BeginDisabledGroup(true);
        }
        if (_missingDelegateParameters)
        {
            EditorGUILayout.HelpBox("No se admiten parametros sin tipo ni nombre, elimine los parametros vacios para generar el delegate", MessageType.Error);
            
        }

        if (_missingEventName)
        {
            EditorGUILayout.HelpBox("No se admiten eventos de delegado sin nombre", MessageType.Error);
            
        }

        if (_delegateReturnType == "" || _delegateBaseName == "") {
            EditorGUILayout.HelpBox("Se debe escribir el tipo que retorna el delegado y su nombre obligatoriamente", MessageType.Error);
        }

        EditorGUILayout.TextArea(_generatedDelegateText);

        if(GUILayout.Button("Copy to clipboard")){
            GUIUtility.systemCopyBuffer = _generatedDelegateText;
            //System.Windows.Clipboard.SetText(_generatedDelegateText);
        }
        if (_missingDelegateParameters || _missingEventName || _delegateReturnType == "" || _delegateBaseName == "")
            EditorGUI.EndDisabledGroup();
    }
}
