﻿using UnityEngine;
using System.Collections;

public class C3DWorldAdjust : MonoBehaviour {

    public RectTransform _panel;
    public Transform _world;
    public Transform _worldReference;
    public Camera _refCamera;
    public bool _useReference;
    public float _movingStep, _scaleStep, _camPosStep, _fovStep;

	// Use this for initialization
	void Start () { 
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void OpenPanel()
    {
        StartCoroutine(CCodeAnimations.LerpDisplacementPanels(() => { }, _panel, _panel.anchoredPosition, Vector2.zero, 0.5f));
        if (_useReference)
        {
            _worldReference.gameObject.SetActive(true);
        }
    }

    public void ClosePanel()
    {
        StartCoroutine(CCodeAnimations.LerpDisplacementPanels(() => { }, _panel, _panel.anchoredPosition, _panel.anchoredPosition + Vector2.right* _panel.rect.width, 0.5f));
        if (_useReference)
        {
            _worldReference.gameObject.SetActive(false);
        }
    }

    public void MoveUp()
    {
        _world.position += Vector3.up * _movingStep;
    }

    public void MoveDown()
    {
        _world.position += Vector3.down * _movingStep;
    }

    public void MoveLeft()
    {
        _world.position += Vector3.left * _movingStep;
    }

    public void MoveRight()
    {
        _world.position += Vector3.right * _movingStep;
    }

    public void MoveFar()
    {
        _world.position += Vector3.forward * _movingStep;
    }

    public void MoveNear()
    {
        _world.position += Vector3.back * _movingStep;
    }

    public void ScaleUp()
    {
        _world.localScale += Vector3.one * _scaleStep;
    }

    public void ScaleDown()
    {
        _world.localScale -= Vector3.one * _scaleStep;
    }

    public void CameraIn()
    {
        _refCamera.transform.localPosition = _refCamera.transform.localPosition + _camPosStep * _refCamera.transform.forward;
    }

    public void CameraOut()
    {
        _refCamera.transform.localPosition = _refCamera.transform.localPosition - _camPosStep * _refCamera.transform.forward;
    }

    public void CameraWide()
    {
        _refCamera.fieldOfView += _fovStep;
    }

    public void CameraStretch()
    {
        _refCamera.fieldOfView -= _fovStep;
    }

}
