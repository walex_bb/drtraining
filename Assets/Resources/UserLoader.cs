﻿using UnityEngine;
using System.Collections;

public class UserLoader : MonoBehaviour {

    public const string path = "users";

	// Use this for initialization
	void Start () 
    {
        // Prueba ed guardado en xml
        UserContainer ic = new UserContainer();
        ic.items.Add(new User());
        User _barto = new User("El Barto", 29, "Springfield");
        Escenario _touchAndProbeST = new Escenario();
        _touchAndProbeST.escenarioName = "Touch And Probe Stationary Target";
        _touchAndProbeST.agility = 50;
        _barto.escenarios.Add(_touchAndProbeST);
        ic.items.Add(_barto);
        UserContainer.Save(path, ic);

        // Prueba de levantar xml
        ic = UserContainer.Load(path);
        foreach (User item in ic.items)
        {
            print(item.name);
        }

    }


}
