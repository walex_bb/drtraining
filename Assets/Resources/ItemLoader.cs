﻿using UnityEngine;
using System.Collections;

public class ItemLoader : MonoBehaviour {

    public const string path = "items";

	// Use this for initialization
	void Start () 
    {
        // Prueba de levantar xml
        ItemContainer ic = ItemContainer.Load(path);
        foreach (Item item in ic.items)
        {
            print(item.name);
        }

        // Prueba ed guardado en xml

        ItemContainer.Save("items2", ic);

	}
	
	
}
