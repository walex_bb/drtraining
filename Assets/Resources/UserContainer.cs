﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("UserCollection")]
public class UserContainer {

    [XmlArray("Users")]
    [XmlArrayItem("User")]
    public List<User> items = new List<User>();

    public static UserContainer Load(string path)
    {
        TextAsset _xml = Resources.Load<TextAsset>(path);
        XmlSerializer serializer = new XmlSerializer(typeof(UserContainer));
        StringReader reader = new StringReader(_xml.text);
        UserContainer items = serializer.Deserialize(reader) as UserContainer;
        reader.Close();

        return items;
    }


    public static void Save(string path, UserContainer _data)
    {
        var serializer = new XmlSerializer(typeof(UserContainer));
        using (var stream = new FileStream("Assets/Resources/" + path + ".xml", FileMode.Create))
        {
            serializer.Serialize(stream, _data);
        }
    }


}
