﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("ItemCollection")]
public class ItemContainer {

    [XmlArray("Items")]
    [XmlArrayItem("Item")]
    public List<Item> items = new List<Item>();

    public static ItemContainer Load(string path)
    {
        TextAsset _xml = Resources.Load<TextAsset>(path);
        XmlSerializer serializer = new XmlSerializer(typeof(ItemContainer));
        StringReader reader = new StringReader(_xml.text);
        ItemContainer items = serializer.Deserialize(reader) as ItemContainer;
        reader.Close();

        return items;
    }


    public static void Save(string path, ItemContainer _data)
    {
        var serializer = new XmlSerializer(typeof(ItemContainer));
        using (var stream = new FileStream("Assets/Resources/" + path + ".xml", FileMode.Create))
        {
            serializer.Serialize(stream, _data);
        }
    }


}
