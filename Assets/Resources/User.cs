﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

public class User {

    [XmlAttribute("name")]
    public string name;

    [XmlElement("Age")]
    public int age;

    [XmlElement("Afiliation")]
    public string afiliation;

    [XmlArray("Escenarios")]
    [XmlArrayItem("Escenario")]
    public List<Escenario> escenarios = new List<Escenario>();

    public User()
    {
        name = "John Doe";
        age = 33;
        afiliation = "None";
    }

    public User(string _name, int _age, string _afiliation)
    {
        name = _name;
        age = _age;
        afiliation = _afiliation;
    }

   



}


public class Escenario
{
    [XmlAttribute("EscenarioName")]
    public string escenarioName;
    [XmlElement("Agility")]
    public float agility;


}






